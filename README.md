# ct02-keyboard-stuff

I have a Siemens Nixdorf CT-02 since I was 1 year old :) Now I can code and somewhat understand electronics so I can make it into the best keyboard for my likings.

Here are some pictures of the keyboard: https://photos.app.goo.gl/VnyPgqQrbDbW5NGN8

### Directories

 - config-app: Configurator app made with JavaFX (and a bunch of pain)
 - config-cli: Command line interface for interacting with the firmware + the native library for the config app
 - new_board: the schematics and PCB design that I created for replacing the original MCU
 - newfw: the current firmware of the keyboard (for the add-in board)
 - oldfw_328p: the first usable firmware with bit-bang P/S2 (this is where it begin)
