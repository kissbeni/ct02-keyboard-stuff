
#ifndef LOCAL_KEYS
#define LOCAL_KEYS

#define LKEYCODE(dmux, indx) ((dmux & 7) | (indx << 3))

// MODIFIERS
#define LKEY_LCTRL        0x67
#define LKEY_LALT         0x4F
#define LKEY_LSHIFT       0x1D
#define LKEY_RALT         0x57
#define LKEY_RCTRL        0x2F
#define LKEY_RSHIFT       0x56

#define LKEY_NUM_LCK      0x22
#define LKEY_CAPS_LCK     0x1C
#define LKEY_SCRL_LCK     0x59

// NUM PAD
#define LKEY_N_DIV        0x02
#define LKEY_N_MUL        0x7A
#define LKEY_N_SUB        0x7B
#define LKEY_N_ADD        0x7D
#define LKEY_N_ENTER      0x7F
#define LKEY_N_DECIMAL    0x07
#define LKEY_N_0          0x5F
#define LKEY_N_1          0x25
#define LKEY_N_2          0x26
#define LKEY_N_3          0x06
#define LKEY_N_4          0x24
#define LKEY_N_5          0x04
#define LKEY_N_6          0x05
#define LKEY_N_7          0x23
#define LKEY_N_8          0x03
#define LKEY_N_9          0x7C

#define LKEY_ARROW_UP     0x76
#define LKEY_ARROW_DOWN   0x77
#define LKEY_ARROW_LEFT   0x0F
#define LKEY_ARROW_RIGHT  0x5E

#define LKEY_PGUP         0x5A
#define LKEY_PGDOWN       0x5B
#define LKEY_HOME         0x73
#define LKEY_END          0x74
#define LKEY_DEL          0x0C
#define LKEY_INS          0x72

// FUNCTION KEYS
#define LKEY_ESC          0x39

#define LKEY_F1           0x40
#define LKEY_F2           0x38
#define LKEY_F3           0x18
#define LKEY_F4           0x60
#define LKEY_F5           0x48
#define LKEY_F6           0x30
#define LKEY_F7           0x68
#define LKEY_F8           0x50
#define LKEY_F9           0x28
#define LKEY_F10          0x08
#define LKEY_F11          0x71
#define LKEY_F12          0x70

#define LKEY_F13          0x43
#define LKEY_F14          0x41
#define LKEY_F15          0x44
#define LKEY_F16          0x42
#define LKEY_F17          0x45
#define LKEY_F18          0x3D
#define LKEY_F19          0x46
#define LKEY_F20          0x3E
#define LKEY_F21          0x47
#define LKEY_F22          0x3F
#define LKEY_F23          0x1f
#define LKEY_F24          0x1E

#define LKEY_OPT1         0x21
#define LKEY_OPT2         0x00
#define LKEY_OPT3         0x01
#define LKEY_OPT4         0x78
#define LKEY_OPT5         0x79

#define LKEY_PRINTSCR     0x58
#define LKEY_BREAK        0x20

// LETTERS AND STUFF
#define LKEY_ENTER        0x0B
#define LKEY_BACKSPACE    0x0A
#define LKEY_SPACE        0x17
#define LKEY_TAB          0x3C

#define LKEY_0            0x29
#define LKEY_1            0x3A
#define LKEY_2            0x1A
#define LKEY_3            0x19
#define LKEY_4            0x61
#define LKEY_5            0x49
#define LKEY_6            0x31
#define LKEY_7            0x11
#define LKEY_8            0x69
#define LKEY_9            0x51

#define LKEY_A            0x65
#define LKEY_B            0x34
#define LKEY_C            0x36
#define LKEY_D            0x4C
#define LKEY_E            0x63
#define LKEY_F            0x4B
#define LKEY_G            0x33
#define LKEY_H            0x13
#define LKEY_I            0x6A
#define LKEY_J            0x6B
#define LKEY_K            0x6C
#define LKEY_L            0x6D
#define LKEY_M            0x15
#define LKEY_N            0x14
#define LKEY_O            0x52
#define LKEY_P            0x53
#define LKEY_Q            0x1B
#define LKEY_R            0x62
#define LKEY_S            0x4D
#define LKEY_T            0x4A
#define LKEY_U            0x12
#define LKEY_V            0x35
#define LKEY_W            0x64
#define LKEY_X            0x37
#define LKEY_Y            0x32
#define LKEY_Z            0x4E

#define LKEY_OEM1         0x54
#define LKEY_OEM2         0x2A
#define LKEY_OEM3         0x3B
#define LKEY_OEM4         0x2B
#define LKEY_OEM5         0x2D
#define LKEY_OEM6         0x2C
#define LKEY_OEM7         0x55
#define LKEY_OEM102       0x66
#define LKEY_OEMPLUS      0x09
#define LKEY_OEMCOMMA     0x16
#define LKEY_OEMPERIOD    0x6E
#define LKEY_OEMMINUS     0x6F

#endif
