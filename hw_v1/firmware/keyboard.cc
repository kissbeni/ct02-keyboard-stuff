#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/io.h>
#include <string.h>

#include "defs.h"

#include "ps2.h"
#include "local_key_codes.h"

#define KEY_DATA_SIZE (8 * sizeof(u16t))

// key translation table
ps2_keydef key_to_proto[255];

// this way we can store 128 key states in 16 bytes of memory
u16t pressedKeys[8];
u16t pressedKeys2[8];
u16t pressedKeysLast[8];

u16t scan;

void initKeys()
{
    for (u8t i = 0; i < 255; i++)
        key_to_proto[i] = { 0xFF, 0xFF };

    key_to_proto[LKEY_NUM_LCK       ]   = PS2_KC_NUM;
    key_to_proto[LKEY_CAPS_LCK      ]   = PS2_KC_CAPS;
    key_to_proto[LKEY_SCRL_LCK      ]   = PS2_KC_SCROLL;

    key_to_proto[LKEY_N_DIV         ]   = PS2_KC_KP_DIV;
    key_to_proto[LKEY_N_MUL         ]   = PS2_KC_KP_TIMES;
    key_to_proto[LKEY_N_SUB         ]   = PS2_KC_KP_MINUS;
    key_to_proto[LKEY_N_ADD         ]   = PS2_KC_KP_PLUS;
    key_to_proto[LKEY_N_ENTER       ]   = PS2_KC_KP_ENTER;
    key_to_proto[LKEY_N_DECIMAL     ]   = PS2_KC_KP_DOT;
    key_to_proto[LKEY_N_0           ]   = PS2_KC_KP0;
    key_to_proto[LKEY_N_1           ]   = PS2_KC_KP1;
    key_to_proto[LKEY_N_2           ]   = PS2_KC_KP2;
    key_to_proto[LKEY_N_3           ]   = PS2_KC_KP3;
    key_to_proto[LKEY_N_4           ]   = PS2_KC_KP4;
    key_to_proto[LKEY_N_5           ]   = PS2_KC_KP5;
    key_to_proto[LKEY_N_6           ]   = PS2_KC_KP6;
    key_to_proto[LKEY_N_7           ]   = PS2_KC_KP7;
    key_to_proto[LKEY_N_8           ]   = PS2_KC_KP8;
    key_to_proto[LKEY_N_9           ]   = PS2_KC_KP9;

    key_to_proto[LKEY_ARROW_UP      ]   = PS2_KC_UP_ARROW;
    key_to_proto[LKEY_ARROW_DOWN    ]   = PS2_KC_DN_ARROW;
    key_to_proto[LKEY_ARROW_LEFT    ]   = PS2_KC_L_ARROW;
    key_to_proto[LKEY_ARROW_RIGHT   ]   = PS2_KC_R_ARROW;

    key_to_proto[LKEY_PGUP          ]   = PS2_KC_PGUP;
    key_to_proto[LKEY_PGDOWN        ]   = PS2_KC_PGDN;
    key_to_proto[LKEY_HOME          ]   = PS2_KC_HOME;
    key_to_proto[LKEY_END           ]   = PS2_KC_END;
    key_to_proto[LKEY_DEL           ]   = PS2_KC_DELETE;
    key_to_proto[LKEY_INS           ]   = PS2_KC_INSERT;

    key_to_proto[LKEY_ESC           ]   = PS2_KC_ESC;

    key_to_proto[LKEY_F1            ]   = PS2_KC_F1;
    key_to_proto[LKEY_F2            ]   = PS2_KC_F2;
    key_to_proto[LKEY_F3            ]   = PS2_KC_F3;
    key_to_proto[LKEY_F4            ]   = PS2_KC_F4;
    key_to_proto[LKEY_F5            ]   = PS2_KC_F5;
    key_to_proto[LKEY_F6            ]   = PS2_KC_F6;
    key_to_proto[LKEY_F7            ]   = PS2_KC_F7;
    key_to_proto[LKEY_F8            ]   = PS2_KC_F8;
    key_to_proto[LKEY_F9            ]   = PS2_KC_F9;
    key_to_proto[LKEY_F10           ]   = PS2_KC_F10;
    key_to_proto[LKEY_F11           ]   = PS2_KC_F11;
    key_to_proto[LKEY_F12           ]   = PS2_KC_F12;
    key_to_proto[LKEY_F13           ]   = PS2_KC_F13;
    key_to_proto[LKEY_F14           ]   = PS2_KC_F14;
    key_to_proto[LKEY_F15           ]   = PS2_KC_F15;
    key_to_proto[LKEY_F16           ]   = PS2_KC_F16;
    key_to_proto[LKEY_F17           ]   = PS2_KC_F17;
    key_to_proto[LKEY_F18           ]   = PS2_KC_F18;
    key_to_proto[LKEY_F19           ]   = PS2_KC_F19;
    key_to_proto[LKEY_F20           ]   = PS2_KC_F20;
    key_to_proto[LKEY_F21           ]   = PS2_KC_F21;
    key_to_proto[LKEY_F22           ]   = PS2_KC_F22;
    key_to_proto[LKEY_F23           ]   = PS2_KC_F23;
    key_to_proto[LKEY_F24           ]   = PS2_KC_F24;

    key_to_proto[LKEY_OPT1          ]   = PS2_KC_CALC;
    key_to_proto[LKEY_OPT2          ]   = PS2_KC_PLAY;
    key_to_proto[LKEY_OPT3          ]   = PS2_KC_MUTE;
    key_to_proto[LKEY_OPT4          ]   = PS2_KC_VOL_DN;
    key_to_proto[LKEY_OPT5          ]   = PS2_KC_VOL_UP;

    key_to_proto[LKEY_PRINTSCR      ]   = PS2_KC_PRTSCR;
    key_to_proto[LKEY_BREAK         ]   = PS2_KC_BREAK;

    key_to_proto[LKEY_ENTER         ]   = PS2_KC_ENTER;
    key_to_proto[LKEY_BACKSPACE     ]   = PS2_KC_BS;
    key_to_proto[LKEY_SPACE         ]   = PS2_KC_SPACE;
    key_to_proto[LKEY_TAB           ]   = PS2_KC_TAB;

    key_to_proto[LKEY_0             ]   = PS2_KC_0;
    key_to_proto[LKEY_1             ]   = PS2_KC_1;
    key_to_proto[LKEY_2             ]   = PS2_KC_2;
    key_to_proto[LKEY_3             ]   = PS2_KC_3;
    key_to_proto[LKEY_4             ]   = PS2_KC_4;
    key_to_proto[LKEY_5             ]   = PS2_KC_5;
    key_to_proto[LKEY_6             ]   = PS2_KC_6;
    key_to_proto[LKEY_7             ]   = PS2_KC_7;
    key_to_proto[LKEY_8             ]   = PS2_KC_8;
    key_to_proto[LKEY_9             ]   = PS2_KC_9;

    key_to_proto[LKEY_A             ]   = PS2_KC_A;
    key_to_proto[LKEY_B             ]   = PS2_KC_B;
    key_to_proto[LKEY_C             ]   = PS2_KC_C;
    key_to_proto[LKEY_D             ]   = PS2_KC_D;
    key_to_proto[LKEY_E             ]   = PS2_KC_E;
    key_to_proto[LKEY_F             ]   = PS2_KC_F;
    key_to_proto[LKEY_G             ]   = PS2_KC_G;
    key_to_proto[LKEY_H             ]   = PS2_KC_H;
    key_to_proto[LKEY_I             ]   = PS2_KC_I;
    key_to_proto[LKEY_J             ]   = PS2_KC_J;
    key_to_proto[LKEY_K             ]   = PS2_KC_K;
    key_to_proto[LKEY_L             ]   = PS2_KC_L;
    key_to_proto[LKEY_M             ]   = PS2_KC_M;
    key_to_proto[LKEY_N             ]   = PS2_KC_N;
    key_to_proto[LKEY_O             ]   = PS2_KC_O;
    key_to_proto[LKEY_P             ]   = PS2_KC_P;
    key_to_proto[LKEY_Q             ]   = PS2_KC_Q;
    key_to_proto[LKEY_R             ]   = PS2_KC_R;
    key_to_proto[LKEY_S             ]   = PS2_KC_S;
    key_to_proto[LKEY_T             ]   = PS2_KC_T;
    key_to_proto[LKEY_U             ]   = PS2_KC_U;
    key_to_proto[LKEY_V             ]   = PS2_KC_V;
    key_to_proto[LKEY_W             ]   = PS2_KC_W;
    key_to_proto[LKEY_X             ]   = PS2_KC_X;
    key_to_proto[LKEY_Y             ]   = PS2_KC_Y;
    key_to_proto[LKEY_Z             ]   = PS2_KC_Z;

    key_to_proto[LKEY_OEM1          ]   = PS2_KC_SEMI;
    key_to_proto[LKEY_OEM2          ]   = PS2_KC_MINUS;
    key_to_proto[LKEY_OEM3          ]   = PS2_KC_SINGLE;
    key_to_proto[LKEY_OEM4          ]   = PS2_KC_OPEN_SQ;
    key_to_proto[LKEY_OEM5          ]   = PS2_KC_BACK;
    key_to_proto[LKEY_OEM6          ]   = PS2_KC_CLOSE_SQ;
    key_to_proto[LKEY_OEM7          ]   = PS2_KC_APOS;
    key_to_proto[LKEY_OEM102        ]   = PS2_KC_EUROPE2;
    key_to_proto[LKEY_OEMPLUS       ]   = PS2_KC_EQUAL;
    key_to_proto[LKEY_OEMCOMMA      ]   = PS2_KC_COMMA;
    key_to_proto[LKEY_OEMPERIOD     ]   = PS2_KC_DOT;
    key_to_proto[LKEY_OEMMINUS      ]   = PS2_KC_DIV;

    key_to_proto[LKEY_LCTRL         ]   = PS2_KC_L_CTRL;
    key_to_proto[LKEY_LALT          ]   = PS2_KC_L_ALT;
    key_to_proto[LKEY_LSHIFT        ]   = PS2_KC_L_SHIFT;
    key_to_proto[LKEY_RALT          ]   = PS2_KC_R_ALT;
    key_to_proto[LKEY_RCTRL         ]   = PS2_KC_R_CTRL;
    key_to_proto[LKEY_RSHIFT        ]   = PS2_KC_R_SHIFT;
}

void demuxSelect(u8t val)
{
    // Output a 3 bit number to [12(LSB) 11 10(MSB)]
    PORTB = (PORTB & ~(0x4 | 0x8 | 0x10)) | ((val & 1) << 4) | ((val & 2) << 2) | (val & 4);
}

bool needToSendKeys()
{
    for (u8t i = 0; i < 8; i++)
        if (pressedKeys[i] != pressedKeysLast[i])
            return true;

    return false;
}

bool isBitSet(u16t mask, u8t indx)
{
    return ((mask >> indx) & 1);
}

u8t bitdiff(u16t mask1, u16t mask2, u8t indx)
{
    u16t mask = 1 << indx;

    if ((mask1 & mask) == (mask2 & mask))
        return BITDIFF_EQUAL;

    if (mask1 & mask)
        return BITDIFF_UNSET;

    return BITDIFF_SET;
}

void sendKeys()
{
    u8t i, j;
    u16t keymask, keymaskLast;

    for (i = 0; i < 8; i++)
    {
        keymask = pressedKeys[i];
        keymaskLast = pressedKeysLast[i];

        if (keymask == 0 || keymask == keymaskLast)
            continue;

        for (j = 0; j < 16; j++)
            if (bitdiff(keymaskLast, keymask, j) == BITDIFF_SET)
            {
                sendKeyDown(key_to_proto[LKEYCODE(i, j)]);
                _delay_micros_without_breaking_ps2(100);
            }
    }

    for (i = 0; i < 8; i++)
    {
        keymask = pressedKeys[i];
        keymaskLast = pressedKeysLast[i];

        if (keymaskLast == 0 || keymask == keymaskLast)
            continue;

        for (j = 0; j < 16; j++)
            if (bitdiff(keymaskLast, keymask, j) == BITDIFF_UNSET)
            {
                sendKeyUp(key_to_proto[LKEYCODE(i, j)]);
                _delay_micros_without_breaking_ps2(100);
            }
    }
}

void scan_keys(u16t res[])
{
    u8t mux_counter, c, d;
    for (mux_counter = 0; mux_counter < 8; mux_counter++)
    {
        demuxSelect(mux_counter);

        _delay_micros_without_breaking_ps2(20);
        SET_LOW_D(PIN_VD_SEL);
        _delay_micros_without_breaking_ps2(20);
        c = PINC, d = PIND;
        scan = (c | ((d << 1) & 0x40) | ((d << 4) & 0x80)); // Construct the first byte from [A0(LSB) A1 A2 A3 A4 A5 D5 D3(MSB)]

        SET_HIGH_D(PIN_VD_SEL);
        _delay_micros_without_breaking_ps2(20);
        c = PINC, d = PIND;
        scan |= ((u16t)(c | ((d << 1) & 0x40) | ((d << 4) & 0x80))) << 8;  // Construct the second byte from [A0(LSB) A1 A2 A3 A4 A5 D5 D3(MSB)]
        res[mux_counter] = scan;
    }
}

void loop()
{
    memcpy(pressedKeysLast, pressedKeys, KEY_DATA_SIZE);  // Save the current state

    scan_keys(pressedKeys);

    for (u8t i = 0; i < 15; i++)
        _delay_micros_without_breaking_ps2(10);

    scan_keys(pressedKeys2);

    for (u8t i = 0; i < 8; i++)
        pressedKeys[i] &= pressedKeys2[i];

    if (needToSendKeys())
        sendKeys();

    readCommands();
}

void setupPins()
{
    // Set up pins

    DDRB = 0;
    DDRB |= 0x01; // Digital pin 8      (PIN_RESERVED)
    DDRB |= 0x02; // Digital pin 9      (PIN_LED_SERIAL_ENABLE)
    DDRB |= 0x04; // Digital pin 10     (PIN_MUX_C)
    DDRB |= 0x08; // Digital pin 11     (PIN_MUX_B)
    DDRB |= 0x10; // Digital pin 12     (PIN_MUX_A)
    DDRB |= 0x20; // Digital pin 13     (PIN_LED_SERIAL_DATA)
    DDRB |= 0x40; // XTAL1              (PIN_CRYSTAL_1)
    DDRB |= 0x80; // XTAL2              (PIN_CRYSTAL_2)

    DDRC = 0;
    // 0x01: analog pin 0 (input)       (PIN_VD_1)
    // 0x02: analog pin 1 (input)       (PIN_VD_2)
    // 0x04: analog pin 2 (input)       (PIN_VD_3)
    // 0x08: analog pin 3 (input)       (PIN_VD_4)
    // 0x10: analog pin 4 (input)       (PIN_VD_5)
    // 0x20: analog pin 5 (input)       (PIN_VD_6)

    DDRD = 0;
    DDRD |= 0x01; // Digital pin 0      (PIN_TX)
    // 0x02: digital pin 1 (input)      (PIN_RX)
    DDRD |= 0x04; // Digital pin 2      (PIN_CLK)
    // 0x08: digital pin 3 (input)      (PIN_VD_8)
    DDRD |= 0x10; // Digital pin 4      (PIN_DATA)
    // 0x20: digital pin 5 (input)      (PIN_VD_7)
    DDRD |= 0x40; // Digital pin 6      (PIN_LED_SERIAL_CLK)
    DDRD |= 0x80; // Digital pin 7      (PIN_VD_SEL)

    // Set all pins to low
    PORTB = 0;
    PORTC = 0;
    PORTD = 0;

    // Set VD_SEL to HIGH
    PORTD |= 0x20; // Digital pin 7     (PIN_VD_SEL)
}

int main()
{
    setupPins();
    ps2Init();

    initKeys();

    // Start PS2 "handshake"
    // ps2Write(0xAA);

    while (1) loop();
    return 0;
}
