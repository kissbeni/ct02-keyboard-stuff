
#ifndef DEFS
#define DEFS

//// Pins ////
#define PIN_CLK					0x04	// Digital pin 2	(register D)
#define PIN_DATA				0x10	// Digital pin 4	(register D)

#define PIN_MUX_A				0x10	// Digital pin 12	(register B)
#define PIN_MUX_B				0x08	// Digital pin 11	(register B)
#define PIN_MUX_C				0x04	// Digital pin 10	(register B)

#define PIN_VD_1				0x01	// Analog pin 0		(register C)
#define PIN_VD_2				0x02	// Analog pin 1		(register C)
#define PIN_VD_3				0x04	// Analog pin 2		(register C)
#define PIN_VD_4				0x08	// Analog pin 3		(register C)
#define PIN_VD_5				0x10	// Analog pin 4		(register C)
#define PIN_VD_6				0x20	// Analog pin 5		(register C)
#define PIN_VD_7				0x20	// Digital pin 5	(register D)
#define PIN_VD_8				0x08	// Digital pin 3	(register D)

#define PIN_VD_SEL				0x80	// Digital pin 7	(register D)

#define PIN_LED_SERIAL_CLK		0x40	// Digital pin 6	(register D)
#define PIN_LED_SERIAL_DATA		0x20	// Digital pin 13	(register B)
#define PIN_LED_SERIAL_ENABLE	0x02	// Digital pin 9	(register B)

#define HIGH 1
#define LOW  0

//// Types ////

// 8 bit (1 byte) unsigned integer
typedef unsigned char u8t;
static_assert(sizeof(u8t) == 1, "invalid u8t size");

// 16 bit (2 byte) unsigned integer
typedef unsigned short u16t;
static_assert(sizeof(u16t) == 2, "invalid u16t size");

// 32 bit (4 byte) unsigned integer
typedef unsigned long u32t;
static_assert(sizeof(u32t) == 4, "invalid u32t size");

typedef struct {
	/*
	1: Keycode only
	2: Extended keycode
	3: Ignore prefixed keycode (for printscreen)
	4: BREAK keycode
	5: Keycode with emulated shift
	*/
	u8t type;
	u8t code;
} ps2_keydef;

//// IO functions ////

#define DIGITAL_READ_B(mask) ((PINB & mask) == mask)
#define DIGITAL_READ_C(mask) ((PINC & mask) == mask)
#define DIGITAL_READ_D(mask) ((PIND & mask) == mask)

#define SET_HIGH_B(mask) PORTB |= (mask)
#define SET_LOW_B(mask) PORTB &= ~(mask)

#define SET_HIGH_C(mask) PORTC |= (mask)
#define SET_LOW_C(mask) PORTC &= ~(mask)

#define SET_HIGH_D(mask) PORTD |= (mask)
#define SET_LOW_D(mask) PORTD &= ~(mask)

//// BITDIFF ////

#define BITDIFF_EQUAL 0
#define BITDIFF_SET 1
#define BITDIFF_UNSET 2

#endif
