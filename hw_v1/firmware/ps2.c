
#include "ps2.h"
#include <util/delay.h>
#include <avr/io.h>

#define PS2_SET_HIGH_D(mask) DDRD &= ~mask; PORTD |= mask
#define PS2_SET_LOW_D(mask) DDRD |= mask; PORTD &= ~mask

void ps2Init()
{
	PS2_SET_HIGH_D(PIN_CLK);
	PS2_SET_HIGH_D(PIN_DATA);

	// Clear the shift register for status LEDs
    setStatusLedBits(0xFF);
    _delay_ms(25);
    setStatusLedBits(0);

    ps2Write(PS2_KC_RESET);
}

void ack() {
	ps2Write(PS2_KC_ACK);
}

static bool shift_pressed = false;

void sendKeyUp(const ps2_keydef key)
{
	if (key.type == 0xFF)
		return;

	if (key.code == PS2_KC_L_SHIFT_RAW || key.code == PS2_KC_R_SHIFT_RAW)
		shift_pressed = false;

	switch (key.type)
	{
		case 1: // Normal keypress
			ps2Write(PS2_KC_SCANCODE);
			ps2Write(key.code);
			break;
		case 2: // Extended keypress
			ps2Write(PS2_KC_EXTEND);
			ps2Write(PS2_KC_SCANCODE);
			ps2Write(key.code);
			break;
		case 3: // Extended ignored keypress
			ps2Write(PS2_KC_EXTEND);
			ps2Write(PS2_KC_SCANCODE);
			ps2Write(PS2_KC_IGNORE);

			ps2Write(PS2_KC_EXTEND);
			ps2Write(PS2_KC_SCANCODE);
			ps2Write(key.code);
			break;
		case 4: // WTF? keypress
			if (key.code == PS2_KC_BREAK_RAW) // Special handling for KC_BREAK
			{
				// No action
			}

			break;
		case 5:  // Emulated shift keypress
				 // No action
			break;
	}
}

void sendKeyDown(const ps2_keydef key)
{
	if (key.type == 0xFF)
		return;

	if (key.code == PS2_KC_L_SHIFT_RAW || key.code == PS2_KC_R_SHIFT_RAW)
		shift_pressed = true;

	switch (key.type)
	{
		case 1: // Normal keypress
			ps2Write(key.code);
			break;
		case 2: // Extended keypress
			ps2Write(PS2_KC_EXTEND);
			ps2Write(key.code);
			break;
		case 3: // Extended ignored keypress
			ps2Write(PS2_KC_EXTEND);
			ps2Write(PS2_KC_IGNORE);
			ps2Write(PS2_KC_EXTEND);
			ps2Write(key.code);
			break;
		case 4: // WTF? keypress
			if (key.code == PS2_KC_BREAK_RAW) // Special handling for KC_BREAK
			{
				// Somebody was drunk I guess
				ps2Write(PS2_KC_EXTEND1);
				ps2Write(PS2_KC_CTRL_RAW);
				ps2Write(PS2_KC_NUM_RAW);
				ps2Write(PS2_KC_SCANCODE);
				ps2Write(PS2_KC_CTRL_RAW);
				ps2Write(PS2_KC_SCANCODE);
				ps2Write(PS2_KC_NUM_RAW);
			}

			break;
		case 5: // Emulated shift keypress
			if (!shift_pressed)
				ps2Write(PS2_KC_L_SHIFT_RAW);

			ps2Write(key.code);

			for (u8t i = 0; i < 20; i++)
				_delay_micros_without_breaking_ps2(200);

			ps2Write(PS2_KC_SCANCODE);
			ps2Write(key.code);

			if (!shift_pressed)
			{
				ps2Write(PS2_KC_SCANCODE);
				ps2Write(PS2_KC_L_SHIFT_RAW);
			}

			_delay_micros_without_breaking_ps2(50);
			break;
	}
}

void setStatusLedBits(u8t flags)
{
	u8t i;

	PORTB &= ~0x02; // Set PIN_LED_SERIAL_ENABLE to LOW

	for (i = 0; i < 8; i++)
	{
		PORTD &= ~0x40; // Set PIN_LED_SERIAL_CLK to LOW
		PORTB = (PORTB & ~0x20) | ((flags & 1) << 5); // Set PIN_LED_SERIAL_DATA
		PORTD |= 0x40; // Set PIN_LED_SERIAL_CLK to HIGH
		flags >>= 1;
	}

	PORTB |= 0x02; // Set PIN_LED_SERIAL_ENABLE to HIGH
}

void readCommands()
{
	bool forceListen = false;
	u8t tmp;
	while (ps2Listen() || forceListen)
	{
		forceListen = false;
		tmp = ps2Read();

		switch (tmp)
		{
			case PS2_KC_READID:
				ack();
				ps2Write(0xAB);
				break;
			case PS2_KC_LOCK:
				ack();
				tmp = ps2Read();
				ack();
				setStatusLedBits(tmp);
				break;
			case PS2_KC_RESEND:
				break; // Ignore resend requests
			case PS2_KC_ECHO:
				ack();
				break;
			case PS2_KC_RESET:
				//!TODO
				ack();
				break;
			case PS2_KC_DEFAULTS:
				//!TODO
				ack();
				break;
			case PS2_KC_DISABLE:
				//!TODO
				ack();
				break;
			case PS2_KC_ENABLE:
				//!TODO
				ack();
				break;
			case PS2_KC_RATE:
				//!TODO
				ack();
				break;
			default:
				ps2Write(PS2_KC_RESEND); // Request resend
				forceListen = true;
				break;
		}
	}
}

// based on PS2KeyAdvanced library
u8t ps2Read()
{
	u8t data = 0x00;
	u8t p = 0x01;

	// wait for the host to release the clock
	while (DIGITAL_READ_D(PIN_DATA) == HIGH);
	while (DIGITAL_READ_D(PIN_CLK) == LOW);

	// read start bit
	_delay_us(PS2_CELL / 2);
	PS2_SET_LOW_D(PIN_CLK);
	_delay_us(PS2_CELL);
	PS2_SET_HIGH_D(PIN_CLK);
	_delay_us(PS2_CELL / 2);

	// read data bits
	for (u8t i = 0; i < 8; i++)
	{
		if (DIGITAL_READ_D(PIN_DATA) == HIGH)
		{
			data |= (1 << i);
			p ^= 1;
		}

		_delay_us(PS2_CELL / 2);
		PS2_SET_LOW_D(PIN_CLK);
		_delay_us(PS2_CELL);
		PS2_SET_HIGH_D(PIN_CLK);
		_delay_us(PS2_CELL / 2);
	}

	// read parity bit
	if (DIGITAL_READ_D(PIN_DATA) != p) {
		// handle parity bit
	}

	_delay_us(PS2_CELL / 2);
	PS2_SET_LOW_D(PIN_CLK);
	_delay_us(PS2_CELL);
	PS2_SET_HIGH_D(PIN_CLK);
	_delay_us(PS2_CELL / 2);

	// send 'ack' bit
	PS2_SET_LOW_D(PIN_DATA);
	_delay_us(PS2_CELL / 2);
	PS2_SET_LOW_D(PIN_CLK);
	_delay_us(PS2_CELL);
	PS2_SET_HIGH_D(PIN_CLK);
	PS2_SET_HIGH_D(PIN_DATA);

	return data;
}

// based on PS2KeyAdvanced library
void ps2Write(const u8t data)
{
	u8t p = 0x01;

	// set start bit
	PS2_SET_LOW_D(PIN_DATA);
	_delay_us(PS2_CELL / 2);
	PS2_SET_LOW_D(PIN_CLK);
	_delay_us(PS2_CELL);
	PS2_SET_HIGH_D(PIN_CLK);
	_delay_us(PS2_CELL / 2);

	// set data bits
	for (u8t i = 0; i < 8; i++)
	{
		if (data & (1 << i))
		{
			PS2_SET_HIGH_D(PIN_DATA);
			p ^= 1;
		}
		else
		{
			PS2_SET_LOW_D(PIN_DATA);
		}

		_delay_us(PS2_CELL / 2);
		PS2_SET_LOW_D(PIN_CLK);
		_delay_us(PS2_CELL);
		PS2_SET_HIGH_D(PIN_CLK);
		_delay_us(PS2_CELL / 2);
	}

	// set parity bit
	if (p)
	{
		PS2_SET_HIGH_D(PIN_DATA);
	}
	else
	{
		PS2_SET_LOW_D(PIN_DATA);
	}

	_delay_us(PS2_CELL / 2);
	PS2_SET_LOW_D(PIN_CLK);
	_delay_us(PS2_CELL);
	PS2_SET_HIGH_D(PIN_CLK);
	_delay_us(PS2_CELL / 2);

	// stet stop bit
	PS2_SET_HIGH_D(PIN_DATA);
	_delay_us(PS2_CELL / 2);
	PS2_SET_LOW_D(PIN_CLK);
	_delay_us(PS2_CELL);
	PS2_SET_HIGH_D(PIN_CLK);
	_delay_us(PS2_CELL / 2);
}

// based on PS2KeyAdvanced library
bool ps2Listen()
{
	if (DIGITAL_READ_D(PIN_CLK) == LOW)
		return true;

	u8t cnt = 0;

	while (DIGITAL_READ_D(PIN_CLK)) {
		if (++cnt >= 100)
			return false;
		_delay_us(1);
	}

	return true;
}
