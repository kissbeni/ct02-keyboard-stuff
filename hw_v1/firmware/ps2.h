
#ifndef PS2_H
#define PS2_H

#include "defs.h"
#include "PS2KeyCode.h"

#define PS2_CELL 40

#define _delay_micros_without_breaking_ps2(t) { readCommands(); _delay_us(t); readCommands(); }

void ack();
void sendKeyDown(const ps2_keydef key);
void sendKeyUp(const ps2_keydef key);
void setStatusLedBits(u8t flags);
void readCommands();
void ps2Init();
u8t ps2Read();
void ps2Write(const u8t data);
bool ps2Listen();

#endif
