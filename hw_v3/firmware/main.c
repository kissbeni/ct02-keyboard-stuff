
#include <stdint.h>

typedef struct {
    uint32_t Mode;
    uint32_t OutType;
    uint32_t OutSpeed;
    uint32_t PullUpDown;
    uint32_t Input;
    uint32_t Output;
    uint32_t BSRR;
    uint32_t Lock;
    uint32_t AlternateFunctions[2];
    uint32_t BRR;
} GPIO_t;

volatile GPIO_t* GPIOA = (GPIO_t*)0x48000000;
volatile GPIO_t* GPIOB = (GPIO_t*)0x48000400;
volatile GPIO_t* GPIOC = (GPIO_t*)0x48000800;

void __attribute__((noreturn)) exit() {
    while (1) ;
}

int main()
{
    uint32_t temp = 0x00;

    temp = GPIOB->Mode;
    temp &= ~3;
    temp |= 1; // Output
    GPIOB->Mode = temp;

    while (1) {
        GPIOB->Output |= 1;
        for (int i = 0; i < 100000; i++) ;
        GPIOB->Output &= ~1;
    }

    return 0;
}
