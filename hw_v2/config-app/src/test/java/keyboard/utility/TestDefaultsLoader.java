package keyboard.utility;

import keyboard.device.configuration.def.KeycodeConfigDef;
import keyboard.device.configuration.eeprom.EEPROMKeyBinding;
import keyboard.utility.DefaultsLoader;
import keyboard.utility.Utils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing the defaults loader
 */
public class TestDefaultsLoader {
    /**
     * Initializes the defaults loader
     */
    @BeforeAll
    static void initLoader() throws IOException {
        DefaultsLoader.read();
    }

    /**
     * Tests finding the default value by a KDKS id
     */
    @Test
    void testDefaultOfKDKS() {
        Optional<KeycodeConfigDef> res = DefaultsLoader.defaultOfKDKS("6666/6666");
        assertTrue(res.isEmpty());

        res = DefaultsLoader.defaultOfKDKS("13/25");
        assertTrue(res.isPresent());
        assertEquals("Enter", res.get().getName());
    }

    /**
     * Tests resetting the default keycode into an invalid EEPROM key binding
     */
    @Test
    void testRestoreInvalid() {
        EEPROMKeyBinding binding = new EEPROMKeyBinding(Utils.UBYTE_MAX, Utils.UBYTE_MAX, (short)0);
        assertFalse(DefaultsLoader.restore(binding));
    }

    /**
     * Tests checking the default keycode of an invalid EEPROM key binding
     */
    @Test
    void testCheckInvalid() {
        EEPROMKeyBinding binding = new EEPROMKeyBinding(Utils.UBYTE_MAX, Utils.UBYTE_MAX, (short)0);
        assertTrue(DefaultsLoader.isSetToDefault(binding));
    }

    /**
     * Tests restoring and checking the default value of a valid EEPROM key binding
     */
    @Test
    void testRestoreAndCheck() {
        EEPROMKeyBinding binding = new EEPROMKeyBinding((byte)1, (byte)3, (short)0);
        assertFalse(DefaultsLoader.isSetToDefault(binding));
        assertTrue(DefaultsLoader.restore(binding));
        assertTrue(DefaultsLoader.isSetToDefault(binding));
    }
}
