package keyboard.utility;

import javafx.scene.paint.Paint;
import keyboard.utility.RGBColor;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing RGB colors
 */
public class TestRGBColor {
    /**
     * Tests constructors
     */
    @Test
    void testConstructor() {
        RGBColor c = new RGBColor();

        assertEquals((byte)0, c.getR());
        assertEquals((byte)0, c.getG());
        assertEquals((byte)0, c.getB());

        c = new RGBColor((byte)123, (byte)123, (byte)123);
        assertEquals((byte)123, c.getR());
        assertEquals((byte)123, c.getG());
        assertEquals((byte)123, c.getB());

        c = new RGBColor(123, 123, 123);
        assertEquals((byte)123, c.getR());
        assertEquals((byte)123, c.getG());
        assertEquals((byte)123, c.getB());
    }

    /**
     * Tests RGB to HSV conversion
     */
    @Test
    void testToHSV() {
        RGBColor c = new RGBColor(0x00, 0x80, 0x80);
        assertEquals((byte)128, c.getHSVHue());
        assertEquals((byte)255, c.getHSVSaturation());
        assertEquals((byte)128, c.getHSVValue());
    }

    /**
     * Tests writing to a byte buffer
     */
    @Test
    void testPut() {
        ByteBuffer buffer = ByteBuffer.allocate(3);
        RGBColor c = new RGBColor(12, 34, 56);
        c.put(buffer);

        buffer.position(0);
        assertEquals((byte)12, buffer.get());
        assertEquals((byte)34, buffer.get());
        assertEquals((byte)56, buffer.get());
    }

    /**
     * Tests reading from a byte buffer
     */
    @Test
    void testGet() {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[] { (byte)0x12, (byte)0x34, (byte)0x56 });
        RGBColor c = new RGBColor(78, 91, 0);
        c.get(buffer);

        assertEquals((byte)0x12, c.getR());
        assertEquals((byte)0x34, c.getG());
        assertEquals((byte)0x56, c.getB());
    }

    /**
     * Tests equality between colors
     */
    @Test
    void testEquals() {
        RGBColor c1 = new RGBColor(0x12, 0x34, 0x56);
        RGBColor c2 = new RGBColor(0x34, 0x56, 0x78);
        RGBColor c3 = new RGBColor(0x12, 0x34, 0x56);
        RGBColor c4 = new RGBColor(0x12, 0x35, 0x56);
        RGBColor c5 = new RGBColor(0x12, 0x34, 0x57);
        RGBColor c6 = new RGBColor(0x13, 0x34, 0x56);
        assertFalse(c1.equals(null));
        assertFalse(c1.equals(c2));
        assertFalse(c2.equals(c1));
        assertTrue(c1.equals(c3));
        assertTrue(c3.equals(c1));
        assertFalse(c1.equals("Hello"));
        assertFalse(c4.equals(c1));
        assertFalse(c5.equals(c1));
        assertFalse(c6.equals(c1));
    }

    /**
     * Tests hash code
     */
    @Test
    void testHashCode() {
        RGBColor c = new RGBColor(0x12, 0x34, 0x56);
        assertEquals(c.hashCode(), 0x563412);
    }

    /**
     * Tests paint conversion
     */
    @Test
    void testToPaint() {
        RGBColor c = new RGBColor(0x12, 0x34, 0x56);
        Paint p = c.toPaint();
        assertEquals(p.toString(), "0x123456ff");
        Paint p2 = c.toPaint();
        assertSame(p, p2);
    }

    /**
     * Grayscale SHV test
     */
    @Test
    void testGrayscaleHSV() {
        RGBColor c = new RGBColor(255, 255, 255);
        assertEquals((byte)0, c.getHSVSaturation());
        assertEquals((byte)0, c.getHSVHue());

        c.setR((byte)128);
        c.setG((byte)128);
        c.setB((byte)128);

        assertEquals((byte)0, c.getHSVSaturation());
        assertEquals((byte)0, c.getHSVHue());
    }

    /**
     * Checks the values of an RGBColor
     * @param c the color
     * @param r the red value
     * @param g the green value
     * @param b the blue value
     * @param h the hue
     * @param s the saturation
     * @param v the value
     */
    private void checkColorHSV(RGBColor c, int r, int g, int b, int h, int s, int v) {
        assertEquals((byte)r, c.getR());
        assertEquals((byte)g, c.getG());
        assertEquals((byte)b, c.getB());
        assertEquals((byte)h, c.getHSVHue());
        assertEquals((byte)s, c.getHSVSaturation());
        assertEquals((byte)v, c.getHSVValue());
    }

    /**
     * Test for HSV to RGB conversion
     */
    @Test
    void testHSV() {
        // if you are modifying/altering this test case
        // please keep in mind, that the values get rounded
        // during the conversation process

        // also the given angle values in the comments are
        // rounded too (for readability), for example 180
        // is actually 180.705882353

        RGBColor c = RGBColor.fromHSV((byte)0, (byte)0, (byte)0);
        checkColorHSV(c,0, 0, 0, 0, 0, 0);

        c.setHSV((byte)0, (byte)0, (byte)10);
        checkColorHSV(c,10, 10, 10, 0, 0, 10);

        // at 0 degrees
        c.setHSV((byte)0, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,255, 0, 0, 0, 255, 255);

        // at 59 degrees
        c.setHSV((byte)42, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,255, 249, 0, 42, 255, 255);

        // at 60 degrees
        c.setHSV((byte)43, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,255, 255, 0, 43, 255, 255);

        // at 120 degrees
        c.setHSV((byte)85, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,0, 255, 0, 85, 255, 255);

        // at 178 degrees
        c.setHSV((byte)126, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,0, 255, 243, 126, 255, 255);

        // at 180 degrees
        c.setHSV((byte)128, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,0, 255, 255, 128, 255, 255);

        // at 240 degrees
        c.setHSV((byte)170, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,0, 0, 255, 170, 255, 255);

        // at 299 degrees
        c.setHSV((byte)212, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,249, 0, 255, 212, 255, 255);

        // at 360 degrees
        c.setHSV((byte)255, Utils.UBYTE_MAX, Utils.UBYTE_MAX);
        checkColorHSV(c,255, 0, 6, 255, 255, 255);
    }

    /**
     * Tests new instance with a given brightness brightness
     */
    @Test
    void testOfBrightness() {
        RGBColor c1 = new RGBColor(255, 255, 255);
        RGBColor c2 = c1.ofBrightness((byte)128);

        assertEquals((byte)128, c2.getR());
        assertEquals((byte)128, c2.getG());
        assertEquals((byte)128, c2.getB());
    }

    /**
     * Tests to string conversion
     */
    @Test
    void testToString() {
        RGBColor c = new RGBColor(0x12, 0x34, 0x56);
        assertEquals("#123456", c.toString());
    }

    /**
     * Tests basic copying
     */
    @Test
    void testCopy() {
        RGBColor c1 = new RGBColor(0x12, 0x34, 0x56);
        RGBColor c2 = new RGBColor();
        c2.copy(c1);

        assertEquals((byte)0x12, c2.getR());
        assertEquals((byte)0x34, c2.getG());
        assertEquals((byte)0x56, c2.getB());
    }

    /**
     * Tests copying with brightness
     */
    @Test
    void testCopyBrightness() {
        RGBColor c1 = new RGBColor(40, 60, 80);
        RGBColor c2 = new RGBColor();
        c2.copy(c1, (byte) 127);

        assertEquals((byte)20, c2.getR());
        assertEquals((byte)30, c2.getG());
        assertEquals((byte)40, c2.getB());
    }
}
