package keyboard.utility;

import keyboard.utility.Utils;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing utilities
 */
public class TestUtils {
    /**
     * Tests getting extension from a filename
     */
    @Test
    void testGetExtension() {
        assertThrows(NullPointerException.class, () -> Utils.getExtension(null));
        File test = new File("hello.world");
        assertEquals("world", Utils.getExtension(test));

        test = new File("something/hello.world");
        assertEquals("world", Utils.getExtension(test));

        test = new File("something/the.hello.world");
        assertEquals("world", Utils.getExtension(test));

        test = new File("something/hello_world");
        assertEquals("", Utils.getExtension(test));
    }

    /**
     * Tests byte to short conversion
     */
    @Test
    void testByteToShort() {
        assertEquals((short) 0, Utils.byte2short((byte) 0));
        assertEquals((short) 127, Utils.byte2short((byte) 127));
        assertEquals((short) 128, Utils.byte2short((byte) 128));
        assertEquals((short) 255, Utils.byte2short((byte) 255));
    }

    /**
     * Tests byte to int conversion
     */
    @Test
    void testByteToInt() {
        assertEquals(0, Utils.byte2int((byte) 0));
        assertEquals(127, Utils.byte2int((byte) 127));
        assertEquals(128, Utils.byte2int((byte) 128));
        assertEquals(255, Utils.byte2int((byte) 255));
    }

    /**
     * Tests distance calculation
     */
    @Test
    void testDistance() {
        assertEquals(0.0, Utils.distance(5, 5, 5, 5));
        assertEquals(2.0, Utils.distance(5, 5, 3, 5));
        assertEquals(2.0, Utils.distance(5, 5, 5, 3));
        assertEquals(2.0, Utils.distance(3, 5, 5, 5));
        assertEquals(2.0, Utils.distance(5, 3, 5, 5));
    }

    /**
     * Tests BSD hash calculation
     */
    @Test
    void testBSDChecksum() {
        byte[] data = new byte[] { 0x01, 0x02, (byte) 0xFE, (byte) 0xFF };
        assertEquals((short)0xa17e, Utils.calculateBSDChecksum(data, 0, data.length));
    }
}
