package keyboard.device;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the matrix id converter
 *
 * Based on https://cloud.csumge.cc/index.php/apps/onlyoffice/s/eNgtAnWEPRqZdrH
 * */
public class TestMatrixIDConverter {
    @Test
    void testFromKD() {
        assertEquals(0, MatrixIDConverter.FromKD(14));
        assertEquals(1, MatrixIDConverter.FromKD(13));
        assertEquals(2, MatrixIDConverter.FromKD(12));
        assertEquals(3, MatrixIDConverter.FromKD(11));
        assertEquals(4, MatrixIDConverter.FromKD(15));
        assertEquals(5, MatrixIDConverter.FromKD(16));
        assertEquals(6, MatrixIDConverter.FromKD(17));
        assertEquals(7, MatrixIDConverter.FromKD(18));
        assertEquals(8, MatrixIDConverter.FromKD(3));
        assertEquals(9, MatrixIDConverter.FromKD(4));
        assertEquals(10, MatrixIDConverter.FromKD(5));
        assertEquals(11, MatrixIDConverter.FromKD(6));
        assertEquals(12, MatrixIDConverter.FromKD(10));
        assertEquals(13, MatrixIDConverter.FromKD(9));
        assertEquals(14, MatrixIDConverter.FromKD(8));
        assertEquals(15, MatrixIDConverter.FromKD(7));
        assertEquals(-1, MatrixIDConverter.FromKD(1234));
    }

    @Test
    void testToKD() {
        assertEquals(14, MatrixIDConverter.ToKD(0));
        assertEquals(13, MatrixIDConverter.ToKD(1));
        assertEquals(12, MatrixIDConverter.ToKD(2));
        assertEquals(11, MatrixIDConverter.ToKD(3));
        assertEquals(15, MatrixIDConverter.ToKD(4));
        assertEquals(16, MatrixIDConverter.ToKD(5));
        assertEquals(17, MatrixIDConverter.ToKD(6));
        assertEquals(18, MatrixIDConverter.ToKD(7));
        assertEquals(3, MatrixIDConverter.ToKD(8));
        assertEquals(4, MatrixIDConverter.ToKD(9));
        assertEquals(5, MatrixIDConverter.ToKD(10));
        assertEquals(6, MatrixIDConverter.ToKD(11));
        assertEquals(10, MatrixIDConverter.ToKD(12));
        assertEquals(9, MatrixIDConverter.ToKD(13));
        assertEquals(8, MatrixIDConverter.ToKD(14));
        assertEquals(7, MatrixIDConverter.ToKD(15));
        assertEquals(-1, MatrixIDConverter.ToKD(-1));
    }

    @Test
    void testFromKS() {
        assertEquals(0, MatrixIDConverter.FromKS(28));
        assertEquals(1, MatrixIDConverter.FromKS(27));
        assertEquals(2, MatrixIDConverter.FromKS(26));
        assertEquals(3, MatrixIDConverter.FromKS(25));
        assertEquals(4, MatrixIDConverter.FromKS(24));
        assertEquals(5, MatrixIDConverter.FromKS(23));
        assertEquals(6, MatrixIDConverter.FromKS(22));
        assertEquals(7, MatrixIDConverter.FromKS(21));
        assertEquals(-1, MatrixIDConverter.FromKS(1234));
    }

    @Test
    void testToKS() {
        assertEquals(28, MatrixIDConverter.ToKS(0));
        assertEquals(27, MatrixIDConverter.ToKS(1));
        assertEquals(26, MatrixIDConverter.ToKS(2));
        assertEquals(25, MatrixIDConverter.ToKS(3));
        assertEquals(24, MatrixIDConverter.ToKS(4));
        assertEquals(23, MatrixIDConverter.ToKS(5));
        assertEquals(22, MatrixIDConverter.ToKS(6));
        assertEquals(21, MatrixIDConverter.ToKS(7));
        assertEquals(-1, MatrixIDConverter.ToKS(1234));
    }
}
