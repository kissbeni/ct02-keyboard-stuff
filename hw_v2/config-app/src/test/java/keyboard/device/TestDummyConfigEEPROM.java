package keyboard.device;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Tests the dummy EEPROM class
 */
public class TestDummyConfigEEPROM {
    /**
     * Creates a new dummy EEPROM, then writes 4 bytes into it.
     *
     * After this we are expecting the file to be 4 bytes, and
     * when we call readAll() it should contain the same values
     * that we wrote.
     */
    @Test
    void testDummyEEPROM() throws IOException {
        String file = Files.createTempFile("test", ".bin").toString();
        DummyConfigEEPROM eeprom = new DummyConfigEEPROM(file);
        assertEquals(file, eeprom.getFilename());
        eeprom.writeAll(new byte[] { 0x01, 0x02, 0x03, 0x04 });
        File f = new File(file);
        assertTrue(f.isFile());
        assertEquals(4, f.length());
        byte[] read = eeprom.readAll();
        assertEquals(4, read.length);
        assertEquals((byte)1, read[0]);
        assertEquals((byte)2, read[1]);
        assertEquals((byte)3, read[2]);
        assertEquals((byte)4, read[3]);
        f.delete();
    }

    /**
     * Tests what happens if we want to use a directory as a dummy EEPROM, and read from it
     */
    @Test
    void testReadInvalid() {
        assertThrows(IOException.class, () -> {
            DummyConfigEEPROM eeprom = new DummyConfigEEPROM(".");
            eeprom.readAll();
        });
    }

    /**
     * Tests what happens if we want to use a directory as a dummy EEPROM, and write to it
     */
    @Test
    void testWriteInvalid() {
        assertThrows(IOException.class, () -> {
            DummyConfigEEPROM eeprom = new DummyConfigEEPROM(".");
            eeprom.writeAll(new byte[] { 0 });
        });
    }
}
