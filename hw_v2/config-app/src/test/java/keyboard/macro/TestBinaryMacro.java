package keyboard.macro;

import keyboard.macro.binary.BinaryMacro;
import keyboard.macro.binary.BinaryMacroExecutionResult;
import keyboard.macro.binary.instruction.*;
import keyboard.utility.FNVHash;
import keyboard.utility.Utils;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for testing binary macro serialization and simulation
 */
public class TestBinaryMacro {

    /**
     * Tests the encoding of binary macros.
     *
     *  What we expect:
     *
     *   0             1           2               3           4           5            6     7            8     9
     * +-------------+-----------+---------------+-----------+-----------+------------+-----+------------+-----+------------+
     * | NOP | HOLDD | HOLDD_ARG | PRESS | HOLDU | PRESS_ARG | HOLDU_ARG | REPN | HI4 | LO8 | REPN | HI4 | LO8 | EXIT | NOP |
     * + ------------+-----------+---------------+-----------+-----------+------------+-----+------------+-----+------------+
     *
     * +-------------+-----------+---------------+-----------+-----------+------------+-----+------------+-----+------------+
     * | 0   | 1     | 02        | 3     | 2     | 03        | 02        | 5    | 0   | 00  | 5    | 1   | 45  | F    | 0   |
     * + ------------+-----------+---------------+-----------+-----------+------------+-----+------------+-----+------------+
     *   0x01          0x02        0x32            0x03        0x02        0x50         0x00  0x51         0x45  0xF0
     */
    @Test
    void testEncode() {
        BinaryMacro m = new BinaryMacro();
        m.instructions.add(new MacroInstructionNOP());
        m.instructions.add(new MacroInstructionHOLDD((byte)2));
        m.instructions.add(new MacroInstructionPRESS((byte)3));
        m.instructions.add(new MacroInstructionHOLDU((byte)2));
        m.instructions.add(new MacroInstructionREPN());
        m.instructions.add(new MacroInstructionREPN(5, 5));
        m.instructions.add(new MacroInstructionEXIT());

        ByteBuffer buffer = ByteBuffer.allocate(16);
        m.encode(buffer);

        System.out.println("testEncode encoded:");
        Utils.hexdump(buffer.array());
        assertEquals(10, buffer.position());
        buffer.position(0);

        assertEquals((byte)0x01, buffer.get());
        assertEquals((byte)0x02, buffer.get());
        assertEquals((byte)0x32, buffer.get());
        assertEquals((byte)0x03, buffer.get());
        assertEquals((byte)0x02, buffer.get());
        assertEquals((byte)0x50, buffer.get());
        assertEquals((byte)0x00, buffer.get());
        assertEquals((byte)0x51, buffer.get());
        assertEquals((byte)0x45, buffer.get());
        assertEquals((byte)0xF0, buffer.get());
    }

    /**
     * Tests the decoding of binary macros
     */
    @Test
    void testDecode1() {
        BinaryMacro m = new BinaryMacro();
        m.instructions.add(new MacroInstructionNOP());
        m.instructions.add(new MacroInstructionHOLDD((byte)2));
        m.instructions.add(new MacroInstructionPRESS((byte)3));
        m.instructions.add(new MacroInstructionHOLDU((byte)2));
        m.instructions.add(new MacroInstructionDELAY((byte)5));
        m.instructions.add(new MacroInstructionREPN(5, 5));
        m.instructions.add(new MacroInstructionEXIT());

        ByteBuffer buffer = ByteBuffer.allocate(16);
        m.encode(buffer);

        assertEquals(10, buffer.position());
        buffer.position(0);

        BinaryMacro m2 = new BinaryMacro();
        m2.decode(buffer);

        ByteBuffer buffer2 = ByteBuffer.allocate(16);
        m2.encode(buffer2);

        assertEquals(FNVHash.hash64(buffer.array()), FNVHash.hash64(buffer2.array()));
    }

    /**
     * Tests the decoding of binary macros with a different set of instructions
     */
    @Test
    void testDecode2() {
        BinaryMacro m = new BinaryMacro();
        m.instructions.add(new MacroInstructionNOP());
        m.instructions.add(new MacroInstructionHOLDD((byte)2));
        m.instructions.add(new MacroInstructionPRESS((byte)3));
        m.instructions.add(new MacroInstructionHOLDU((byte)2));
        m.instructions.add(new MacroInstructionREPN());
        m.instructions.add(new MacroInstructionREPN());
        m.instructions.add(new MacroInstructionNOP());
        m.instructions.add(new MacroInstructionEXIT());

        ByteBuffer buffer = ByteBuffer.allocate(16);
        m.encode(buffer);

        assertEquals(10, buffer.position());
        buffer.position(0);

        BinaryMacro m2 = new BinaryMacro();
        m2.decode(buffer);

        ByteBuffer buffer2 = ByteBuffer.allocate(16);
        m2.encode(buffer2);

        assertEquals(FNVHash.hash64(buffer.array()), FNVHash.hash64(buffer2.array()));
    }

    /**
     * Tests what happens when we want to decode a malformed binary macro
     */
    @Test
    void testDecodeMalformed1() {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[] {
            0x05, 0, 0
        });

        assertThrows(Error.class, () -> {
            BinaryMacro m = new BinaryMacro();
            m.decode(buffer);
        });
    }

    /**
     * Tests what happens when we want to decode a malformed binary macro
     */
    @Test
    void testDecodeMalformed2() {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[] {
            (byte) 0xA0, 0, 0
        });

        assertThrows(Error.class, () -> {
            BinaryMacro m = new BinaryMacro();
            m.decode(buffer);
        });
    }

    /**
     * Tests what happens when we want to decode a malformed binary macro
     */
    @Test
    void testDecodeMalformed3() {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[] {
                0x0A, 0, 0
        });

        assertThrows(Error.class, () -> {
            BinaryMacro m = new BinaryMacro();
            m.decode(buffer);
        });
    }

    /**
     * Tests the simulation of a binary macro
     * @throws MacroExecutionException
     */
    @Test
    void testSimulateExecution() throws MacroExecutionException {
        BinaryMacro m = new BinaryMacro();
        m.instructions.add(new MacroInstructionNOP());
        m.instructions.add(new MacroInstructionHOLDD((byte)2));
        m.instructions.add(new MacroInstructionPRESS((byte)3));
        m.instructions.add(new MacroInstructionHOLDU((byte)2));
        m.instructions.add(new MacroInstructionDELAY((byte)5));
        m.instructions.add(new MacroInstructionREPN(2, 2));
        m.instructions.add(new MacroInstructionNOP());
        m.instructions.add(new MacroInstructionEXIT());
        BinaryMacroExecutionResult res = m.simulateExecution();
        assertEquals(12, res.getCyclesUsed());
    }

    /**
     * Tests what happens when we exceed the execution cap
     */
    @Test
    void testSimulateExecutionOverCap() {
        BinaryMacro m = new BinaryMacro();

        for (int i = 0; i < 5000; i++)
            m.instructions.add(new MacroInstructionNOP());

        m.instructions.add(new MacroInstructionEXIT());

        assertThrows(MacroExecutionException.class, m::simulateExecution);
    }

    /**
     * Tests what happens when we simulate a macro without an exit instruction
     */
    @Test
    void testSimulateExecutionNoExit() {
        BinaryMacro m = new BinaryMacro();
        m.instructions.add(new MacroInstructionNOP());

        assertThrows(MacroExecutionException.class, m::simulateExecution);
    }

    /**
     * Tests what happens when whe try to hold down lots of keys
     */
    @Test
    void testSimulateExecutionHoldKeys() {
        BinaryMacro m = new BinaryMacro();

        for (int i = 0; i < 50; i++)
            m.instructions.add(new MacroInstructionHOLDU((byte) i)); // this should not fail

        for (int i = 0; i < 50; i++)
            m.instructions.add(new MacroInstructionHOLDD((byte) i)); // but this should

        m.instructions.add(new MacroInstructionEXIT());

        assertThrows(MacroExecutionException.class, m::simulateExecution);
    }

    /**
     * Tests what happens when the program counter reaches a negative value
     */
    @Test
    void testSimulateExecutionJumpFar() {
        BinaryMacro m = new BinaryMacro();
        m.instructions.add(new MacroInstructionREPN(2, 2));
        m.instructions.add(new MacroInstructionEXIT());

        assertThrows(MacroExecutionException.class, m::simulateExecution);
    }

    /**
     * Tests conversion to source code
     */
    @Test
    void testToSourceCode() {
        BinaryMacro m = new BinaryMacro();
        m.instructions.add(new MacroInstructionNOP());
        m.instructions.add(new MacroInstructionHOLDD((byte)4));
        m.instructions.add(new MacroInstructionPRESS((byte)5));
        m.instructions.add(new MacroInstructionHOLDU((byte)4));
        m.instructions.add(new MacroInstructionDELAY((byte)5));
        m.instructions.add(new MacroInstructionREPN(2, 2));
        m.instructions.add(new MacroInstructionEXIT());

        assertEquals(
                "NOP;" + System.lineSeparator() +
                        "HOLDD A;" + System.lineSeparator() +
                        "PRESS B;" + System.lineSeparator() +
                        "HOLDU A;" + System.lineSeparator() +
                        "DELAY 5;" + System.lineSeparator() +
                        "REPN 2, 2;" + System.lineSeparator() +
                        "EXIT;" + System.lineSeparator(),
                m.toSourceCode()
        );
    }
}
