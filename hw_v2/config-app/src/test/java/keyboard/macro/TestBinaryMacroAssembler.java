package keyboard.macro;

import keyboard.macro.binary.BinaryMacro;
import keyboard.macro.binary.BinaryMacroAssembler;
import keyboard.macro.binary.instruction.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the binary macro assembler
 */
public class TestBinaryMacroAssembler {

    /**
     * Test basics of the assembler and comments
     */
    @Test
    void testAssembler1() throws ParseException {
        BinaryMacroAssembler ass = new BinaryMacroAssembler();

        String code =
                "NOP;" + System.lineSeparator() +
                "// hello world!" + System.lineSeparator() +
                "/*" + System.lineSeparator() +
                " this is a very long comment"  + System.lineSeparator() +
                "NOP;" + System.lineSeparator() +
                "*/" + System.lineSeparator() +
                "  EXIT  ;      ";

        ass.parse(new StringReader(code));
        BinaryMacro res = ass.getResult();
        assertEquals(2, res.instructions.size());

        assertNotNull(res.instructions.get(0));
        assertNotNull(res.instructions.get(1));

        assertTrue(res.instructions.get(0) instanceof MacroInstructionNOP);
        assertTrue(res.instructions.get(1) instanceof MacroInstructionEXIT);
    }

    /**
     * Tests assembling a complex list of instructions
     */
    @Test
    void testAssembler2() throws ParseException {
        BinaryMacroAssembler ass = new BinaryMacroAssembler();

        String code =
                "NOP;" + System.lineSeparator() +
                "PRESS A;" + System.lineSeparator() +
                "REPN 2, 3;" + System.lineSeparator() +
                "DELAY 10;" + System.lineSeparator() +
                "HOLDD A;" + System.lineSeparator() +
                "HOLDU A;" + System.lineSeparator() +
                "EXIT;";

        ass.parse(new StringReader(code));
        BinaryMacro res = ass.getResult();
        assertEquals(7, res.instructions.size());

        for (MacroInstruction i : res.instructions)
            assertNotNull(i);

        assertTrue(res.instructions.get(0) instanceof MacroInstructionNOP);
        assertTrue(res.instructions.get(1) instanceof MacroInstructionPRESS);
        assertTrue(res.instructions.get(2) instanceof MacroInstructionREPN);
        assertTrue(res.instructions.get(3) instanceof MacroInstructionDELAY);
        assertTrue(res.instructions.get(4) instanceof MacroInstructionHOLDD);
        assertTrue(res.instructions.get(5) instanceof MacroInstructionHOLDU);
        assertTrue(res.instructions.get(6) instanceof MacroInstructionEXIT);

        MacroInstructionPRESS press = (MacroInstructionPRESS)res.instructions.get(1);
        assertEquals((byte)4, press.getKeycode()); // A is converted to it's keycode, which is 0x04

        MacroInstructionREPN repn = (MacroInstructionREPN)res.instructions.get(2);
        assertEquals((byte)2, repn.getOffset());
        assertEquals((byte)3, repn.getTimes());

        MacroInstructionDELAY delay = (MacroInstructionDELAY)res.instructions.get(3);
        assertEquals((byte)10, delay.getDelay());

        MacroInstructionHOLDD holdd = (MacroInstructionHOLDD)res.instructions.get(4);
        assertEquals((byte)4, holdd.getKeycode()); // A is converted to it's keycode, which is 0x04

        MacroInstructionHOLDU holdu = (MacroInstructionHOLDU)res.instructions.get(5);
        assertEquals((byte)4, holdu.getKeycode()); // A is converted to it's keycode, which is 0x04
    }

    /**
     * Tests what happens when we try to assemble a potato
     */
    @Test
    void testAssemblerGarbage() {
        assertThrows(ParseException.class, () -> {
            BinaryMacroAssembler ass = new BinaryMacroAssembler();

            String code = "Potato";
            ass.parse(new StringReader(code));
        });
    }

    /**
     * Tests what happens when we miss a semicolon (no arguments)
     */
    @Test
    void testAssemblerSemicolon1() {
        assertThrows(ParseException.class, () -> {
            BinaryMacroAssembler ass = new BinaryMacroAssembler();

            String code = "NOP";
            ass.parse(new StringReader(code));
        });
    }

    /**
     * Tests what happens when we miss a semicolon (one argument)
     */
    @Test
    void testAssemblerSemicolon2() {
        assertThrows(ParseException.class, () -> {
            BinaryMacroAssembler ass = new BinaryMacroAssembler();

            String code = "PRESS A";
            ass.parse(new StringReader(code));
        });
    }

    /**
     * Tests what happens when we miss a semicolon (two arguments)
     */
    @Test
    void testAssemblerSemicolon3() {
        assertThrows(ParseException.class, () -> {
            BinaryMacroAssembler ass = new BinaryMacroAssembler();

            String code = "REPN 1, 2";
            ass.parse(new StringReader(code));
        });
    }

    /**
     * Test what happens when we give the assembler an invalid stream
     */
    @Test
    void testAssemblerInvalidStream() throws Exception {
        BinaryMacroAssembler ass = new BinaryMacroAssembler();
        Path p = Files.createTempFile("test", ".txt");

        Files.writeString(p, "NOP;");
        FileInputStream fis = new FileInputStream(p.toFile());
        InputStreamReader isr = new InputStreamReader(fis);
        isr.close();
        p.toFile().delete();

        assertThrows(ParseException.class, () -> {
            ass.parse(isr);
        });
    }
}
