package keyboard.gui;

import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import keyboard.utility.RGBColor;
import keyboard.utility.Utils;
import lombok.Getter;
import lombok.Setter;

import java.util.function.Consumer;

/**
 * Defines a color selection wheel
 */
public class ColorWheel {
    /**
     * The base pane
     */
    @Getter
    private Pane   pane            = new Pane();
    /**
     * The canvas to draw the HSV color circle
     */
    private Canvas canvas          = new Canvas(20, 20);
    /**
     * The circle used for darkening the HSV circle
     * when the enabled is set to false
     */
    private Circle disableCircle   = new Circle();
    /**
     * The circle used to indicate the selection
     */
    private Circle selectionCircle = new Circle(4);

    
    /**
     * The radius of the color wheel
     */
    @Getter
    private double radius;
    /**
     * The position of the color wheel
     */
    private double cx, cy;
    
    /**
     * The enabled state of the color wheel
     */
    @Getter
    private boolean enabled = true;

    /**
     * Callback that is called when the color is changed
     */
    @Setter
    private Consumer<RGBColor> onChangeHandler;

    /**
     * Callback that is called when the mouse is released
     */
    @Setter
    private EventHandler<MouseEvent> onMouseReleased;

    /**
     * Stores the currently selected color
     */
    @Getter
    private RGBColor currentColor = RGBColor.WHITE;

    /**
     * The 2D context of the canvas containing the HSV color circle
     */
    private GraphicsContext graphicsContext;

    /**
     * Initializes a color wheel with a radius of 10
     */
    public ColorWheel() {
        this(10);
    }

    /**
     * Inititalizes a color wheel with the given radius
     *
     * @param radius the radius of the circle
     */
    public ColorWheel(double radius) {
        this.graphicsContext = canvas.getGraphicsContext2D();

        disableCircle.setRadius(radius);

        pane.getChildren().add(canvas);
        pane.getChildren().add(disableCircle);
        pane.getChildren().add(selectionCircle);

        canvas.setLayoutX(0);
        canvas.setLayoutY(0);

        disableCircle.setFill(Paint.valueOf("#000000"));
        disableCircle.setOpacity(0.4);
        disableCircle.setCenterY(radius);
        disableCircle.setCenterX(radius);
        disableCircle.setVisible(false);

        selectionCircle.setFill(Paint.valueOf("#000000"));
        selectionCircle.setStroke(Paint.valueOf("#FFFFFF"));
        selectionCircle.setStrokeWidth(1.2);
        selectionCircle.setCenterX(radius);
        selectionCircle.setCenterY(radius);

        setCenterX(0);
        setCenterY(0);

        currentColor = RGBColor.WHITE;

        setRadius(radius);

        canvas.setOnMousePressed(this::onMouse);
        canvas.setOnMouseReleased(event -> {
            onMouse(event);

            if (onMouseReleased != null)
                onMouseReleased.handle(event);
        });
        canvas.setOnMouseDragged(this::onMouse);
    }

    /**
     * Sets the radius of the circle
     *
     * @param r the new radius
     */
    public void setRadius(double r) {
        if (radius == r)
            return;

        selectionCircle.setCenterX(selectionCircle.getCenterX() * (r / radius));
        selectionCircle.setCenterX(selectionCircle.getCenterY() * (r / radius));

        radius = r;

        canvas.setHeight(r * 2);
        canvas.setWidth(r * 2);

        pane.setMinHeight(r * 2);
        pane.setMinWidth(r * 2);

        pane.setMaxHeight(r * 2);
        pane.setMaxWidth(r * 2);
        update2d();
    }

    /**
     * Sets the X coordinate of the circle
     *
     * @param x the new x
     */
    public void setCenterX(double x) {
        if (cx == x)
            return;

        cx = x;
        pane.setLayoutX(x - radius);
    }

    /**
     * Sets the Y coordinate of the circle
     *
     * @param y the new y
     */
    public void setCenterY(double y) {
        if (cy == y)
            return;

        cy = y;
        pane.setLayoutY(y - radius);
    }

    /**
     * Sets the current color
     *
     * @param color the new color
     */
    public void setCurrentColor(RGBColor color) {
            currentColor = color;

        double angle = (Utils.byte2short(color.getHSVHue()) * Math.PI * 2) / 255;
        double r = (Utils.byte2short(color.getHSVSaturation()) * radius) / 255;

        selectionCircle.setCenterX(radius + r * Math.sin(angle));
        selectionCircle.setCenterY(radius + r * Math.cos(angle));

        if (onChangeHandler != null)
            onChangeHandler.accept(currentColor);
    }

    /**
     * Sets the enabled state of the selector
     * (if false, all interactions with the circle are disabled)
     *
     * @param en the new enabled state
     */
    public void setEnabled(boolean en) {
        if (enabled == en)
            return;

        enabled = en;
        disableCircle.setVisible(!enabled);
    }

    /**
     * Redraws the circle
     */
    private void update2d() {
        graphicsContext.setLineWidth(0.5);
        for (double angle = 0; angle < Math.PI * 2; angle += 0.005) {
            double h = (angle / (Math.PI * 2)) * 0xff;
            for (double r = 0; r < radius; r += 0.5) {
                double s = (r / radius) * 0xff;
                RGBColor color = RGBColor.fromHSV((byte) Math.floor(h), (byte) Math.floor(s), (byte) 255);
                graphicsContext.beginPath();
                graphicsContext.setStroke(color.toPaint());
                graphicsContext.moveTo(radius + (r - 0.5) * Math.sin(angle), radius + (r - 0.5) * Math.cos(angle));
                graphicsContext.lineTo(radius + r * Math.sin(angle), radius + r * Math.cos(angle));
                graphicsContext.stroke();
                graphicsContext.closePath();
            }
        }

        graphicsContext.moveTo(0, 0);
    }

    /**
     * Handler for mouse events (press,drag,release)
     *
     * @param event the event
     */
    private void onMouse(MouseEvent event) {
        if (event.getButton() != MouseButton.PRIMARY || !enabled)
            return;

        if (Utils.distance(event.getX(), event.getY(), radius, radius) > radius)
            return;

        double x = event.getX(), y = event.getY();

        selectionCircle.setCenterX(x);
        selectionCircle.setCenterY(y);

        double s = (Utils.distance(x, y, radius, radius) / radius) * 0xff;
        double h = ((Math.PI / 2 - Math.atan2(y - radius, x - radius)) / (Math.PI * 2)) * 0xff;
        currentColor = RGBColor.fromHSV((byte) Math.floor(h), (byte) Math.floor(s), (byte) 255);

        if (onChangeHandler != null)
            onChangeHandler.accept(currentColor);
    }
}
