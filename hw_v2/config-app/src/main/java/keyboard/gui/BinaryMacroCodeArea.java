package keyboard.gui;

import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import keyboard.macro.binary.MacroOpcode;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.reactfx.Subscription;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A code area with syntax highlighting for binary macros
 */
public class BinaryMacroCodeArea extends CodeArea {
    private static final String KEYWORD_PATTERN   = "\\b(" + String.join("|", MacroOpcode.names()) + ")\\b";
    private static final String SEMICOLON_PATTERN = "\\;";
    private static final String COMMENT_PATTERN   = "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/";

    private static final Pattern PATTERN = Pattern.compile(
            "(?<KEYWORD>" + KEYWORD_PATTERN + ")"
                    + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
                    + "|(?<COMMENT>" + COMMENT_PATTERN + ")"
    );

    /**
     * Initializes the code area
     */
    public BinaryMacroCodeArea() {
        // add line numbers to the left of area
        setParagraphGraphicFactory(LineNumberFactory.get(this));
        getStylesheets().addAll(getClass().getResource("/ui/style/syntax.css").toExternalForm());

        // recompute the syntax highlighting 500 ms after user stops editing area
        Subscription cleanupWhenNoLongerNeedIt =

                // plain changes = ignore style changes that are emitted when syntax
                // highlighting is reapplied
                // multi plain changes = save computation by not rerunning the code multiple
                // times
                // when making multiple changes (e.g. renaming a method at multiple parts in
                // file)
                multiPlainChanges()

                        // do not emit an event until 500 ms have passed since the last emission of
                        // previous stream
                        .successionEnds(Duration.ofMillis(500))

                        // run the following code block when previous stream emits an event
                        .subscribe(ignore -> setStyleSpans(0, computeHighlighting(getText())));

        // when no longer need syntax highlighting and wish to clean up memory leaks
        // run: `cleanupWhenNoLongerNeedIt.unsubscribe();`

        // auto-indent: insert previous line's indents on enter
        final Pattern whiteSpace = Pattern.compile("^\\s+");
        addEventHandler(KeyEvent.KEY_PRESSED, KE -> {
            if (KE.getCode() == KeyCode.ENTER) {
                int caretPosition = getCaretPosition();
                int currentParagraph = getCurrentParagraph();
                Matcher m0 = whiteSpace.matcher(getParagraph(currentParagraph - 1).getSegments().get(0));
                if (m0.find())
                    Platform.runLater(() -> insertText(caretPosition, m0.group()));
            }
        });
    }

    /**
     * Processes highlighting
     * @param text the text to process
     * @return the styled string
     */
    private static StyleSpans<Collection<String>> computeHighlighting(String text) {
        Matcher matcher = PATTERN.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        while (matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                            matcher.group("SEMICOLON") != null ? "semicolon" :
                                    matcher.group("COMMENT") != null ? "comment" :
                                            null; /* never happens */
            assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }
}
