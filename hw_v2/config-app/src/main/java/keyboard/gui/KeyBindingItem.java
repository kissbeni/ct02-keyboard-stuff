package keyboard.gui;

import keyboard.device.configuration.def.KeycodeConfigDef;
import lombok.Getter;

/**
 * An item used for filtering key bindings
 */
public class KeyBindingItem implements Comparable<KeyBindingItem> {
    /**
     * The name of this item
     */
    private final   String           name;
    /**
     * The name of this item in lowercase
     */
    private final   String           lcName;
    @Getter private KeycodeConfigDef def;

    /**
     * Initializes the class
     * @param path the path of the key code
     * @param def the keycode definition
     */
    public KeyBindingItem(String path, KeycodeConfigDef def) {
        this.def = def;
        this.name = path + def.getName();
        this.lcName = this.name.toLowerCase();
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Tests this item against a filter
     * @param text the filter text
     * @return true if the this item passes the filter false otherwise
     */
    public boolean passesFilter(String text) {
        return (text == null || text.isEmpty() || lcName.contains(text));
    }

    @Override
    public int compareTo(KeyBindingItem keyBindingItem) {
        return lcName.compareTo(keyBindingItem.lcName);
    }
}
