package keyboard.gui.event;

import keyboard.gui.KeyViewItem;

/**
 * Handler for key item events
 */
public interface IKeyItemEventHandler {
    /**
     * Called when a key item is clicked
     * @param item the clicked item
     */
    void onKeyItemClick(KeyViewItem item);
}
