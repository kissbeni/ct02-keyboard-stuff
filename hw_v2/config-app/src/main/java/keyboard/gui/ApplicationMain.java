package keyboard.gui;

import animatefx.animation.FadeIn;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import keyboard.device.DeviceController;
import keyboard.device.configuration.KeycodeManager;
import keyboard.device.configuration.SwitchLayoutManager;
import keyboard.utility.DefaultsLoader;
import keyboard.utility.LocalStorage;
import keyboard.utility.Utils;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Timer;

/**
 * The main class of the application
 */
public class ApplicationMain extends Application {
    @Getter private static final Logger              logger = LogManager.getLogger(ApplicationMain.class);
    /**
     * The keycode database
     */
    @Getter private static       KeycodeManager      keycodeManager;
    /**
     * The switch database
     */
    @Getter private static       SwitchLayoutManager switchLayoutManager;
    /**
     * The global timer used by multiple objects
     */
    @Getter private static       Timer               globalTimer = new Timer();

    static {
        LocalStorage.initialize();

        keycodeManager = KeycodeManager.createInstance("keycodes-usb.xml");
        switchLayoutManager = SwitchLayoutManager.createInstance("ct02-config.xml");
    }

    /**
     * The application's entry point
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        logger.debug("Command line arguments:");

        for (String s : args)
            logger.debug("   {}", s);

        logger.debug("System properties:");

        for (Entry<Object, Object> p : System.getProperties().entrySet())
            logger.debug("   {} = {}", p.getKey(), p.getValue());

        if (keycodeManager == null)
            return;

        try {
            DefaultsLoader.read();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
            return;
        }

        DeviceController.initialize(args);

        Application.launch(args);
    }

    /**
     * Loads every TTF font from resources in the ui/fonts directory
     */
    private static void loadFonts() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        File dir = new File(loader.getResource("ui/fonts").getPath());

        for (File f : dir.listFiles())
            if (Utils.getExtension(f).equalsIgnoreCase("ttf")) {
                logger.debug("Loading font: {0}", f.getName());
                Font.loadFont(loader.getResourceAsStream("ui/fonts/" + f.getName()), 10);
            }
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        DeviceController.finish();
        globalTimer.cancel();
    }

    @Override
    public void start(Stage stage) {
        loadFonts();

        logger.info("Building user interface");

        Scene scene;

        try {
            scene = new Scene(FXMLLoader.load(getClass().getResource("/ui/fxml/app.fxml")), 1280, 720);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        scene.getStylesheets().addAll(getClass().getResource("/ui/style/app.css").toExternalForm());

        stage.initStyle(StageStyle.UNIFIED);
        stage.setScene(scene);
        stage.setTitle("CT-02 Configurator");
        stage.setResizable(false);
        stage.setMinHeight(720);
        stage.setMinWidth(1280);
        stage.show();

        new FadeIn(scene.getRoot()).play();

        DeviceController.defaultsLoaded();
    }
}
