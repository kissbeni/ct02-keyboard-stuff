package keyboard.gui.controller;

import animatefx.animation.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import keyboard.device.ConnectionStatus;
import keyboard.device.DeviceController;
import keyboard.device.configuration.eeprom.EEPROMConfiguration;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ResourceBundle;

/**
 * Controller wor the main window
 */
public class ApplicationController implements Initializable {
    @Getter
    private static final Logger  logger    = LogManager.getLogger(ApplicationController.class);
    /**
     * The currently selected menu index
     */
    private              int     current   = 0;
    @FXML private        Label   keyboardMenuItem;
    @FXML private        Label   hScriptingMenuItem;
    @FXML private        Label   lScriptingMenuItem;
    @FXML private        Label   lightingMenuItem;
    @FXML private        VBox    keyboardMenuView;
    @FXML private        VBox    hScriptingMenuView;
    @FXML private        VBox    lScriptingMenuView;
    @FXML private        VBox    lightingMenuView;
    @FXML private        Label   statusLabel;
    @FXML private        Circle  statusCircle;
    @FXML private        HBox    busyBox;
    @FXML private        Label   busyLabel;
    /**
     * List of the menu items
     */
    private              Label[] menuItmes = new Label[4];
    /**
     * List of the menus
     */
    private              VBox[]  menuViews = new VBox[4];

    /**
     * Jump n pages forward
     * @param n number of pages to jump
     */
    private void nextPage(int n) {
        logger.debug("Called nextPage({}) current={},next={},ok={}", n, current, current + n, (n > 0) && (current + (n - 1) < menuItmes.length));
        if (n < 1) return;
        if (current + (n - 1) < menuItmes.length) {
            final int curr = current;
            final int next = current + n;

            FadeOutUp anim1 = new FadeOutUp(menuViews[curr]);
            anim1.setSpeed(4);
            anim1.play();

            menuItmes[curr].getStyleClass().remove("selected");
            menuItmes[next].getStyleClass().add("selected");
            new FadeInUp(menuViews[next]).setDelay(new Duration(10)).setSpeed(3).play();
            menuViews[next].toFront();

            current = next;
        }
    }

    /**
     * Jump n pages backwards
     * @param n number of pages to jump
     */
    private void prevPage(int n) {
        logger.debug("Called prevPage({}) current={},next={},ok={}", n, current, current - n, (n > 0) && ((current - (n - 1)) > 0));
        if (n < 1) return;
        if ((current - (n - 1)) > 0) {
            final int curr = current;
            final int next = current - n;

            new FadeOutDown(menuViews[curr]).setSpeed(4).play();

            menuItmes[curr].getStyleClass().remove("selected");
            menuItmes[next].getStyleClass().add("selected");
            new FadeInDown(menuViews[next]).setDelay(new Duration(10)).setSpeed(3).play();
            menuViews[next].toFront();

            current = next;
        }
    }

    /**
     * Click handler for keyboard layout menu item
     */
    @FXML
    private void onClickKeyboardMenu() {
        if (current == 0)
            return;

        prevPage(current);
    }

    /**
     * Click handler for hardware scripting menu item
     */
    @FXML
    private void onClickHScriptingMenu() {
        if (current == 1)
            return;

        if (current == 0) nextPage(1);
        else prevPage(current - 1);
    }

    /**
     * Click handler for lua scripting menu item
     */
    @FXML
    private void onClickLScriptingMenu() {
        if (current == 2)
            return;

        if (current < 2) nextPage(2 - current);
        else prevPage(1);
    }

    /**
     * Click handler for lighting menu item
     */
    @FXML
    private void onClickLightingMenu() {
        if (current == 3)
            return;

        nextPage(3 - current);
    }

    /**
     * Click handler for the apply button
     */
    @FXML
    private void onClickApply() {
        Alert alert = new Alert(AlertType.CONFIRMATION, "Save all changes?", ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES)
            DeviceController.saveConfig();
    }

    /**
     * Click handler for the revert button
     */
    @FXML
    private void onClickRevert() {
        Alert alert = new Alert(AlertType.CONFIRMATION, "Revert all changes?", ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES)
            DeviceController.loadConfig();
    }

    /**
     * Click handler for reboot button
     * @param evt
     */
    @FXML
    private void onClickReset(MouseEvent evt) {
        if (evt.getButton().equals(MouseButton.PRIMARY))
            if (evt.getClickCount() == 2)
                DeviceController.resetDevice();
    }

    @FXML
    private void onClickImport() {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();

            ObjectMapper mapper = new ObjectMapper();
            try {
                String json = Files.readString(file.toPath());
                EEPROMConfiguration config = mapper.readValue(json, EEPROMConfiguration.class);
                DeviceController.getConfig().replace(config);
                DeviceController.runConfigLoadedCallbacks();
            } catch (IOException e) {
                Alert error = new Alert(AlertType.ERROR, "Failed to load the given json file:\n" + e.getMessage(), ButtonType.OK);
                error.show();
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void onClickExport() {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();

            ObjectMapper mapper = new ObjectMapper();
            try {
                String json = mapper.writeValueAsString(DeviceController.getConfig());
                Files.writeString(file.toPath(), json);
            } catch (IOException e) {
                Alert error = new Alert(AlertType.ERROR, "Failed to save the file:\n" + e.getMessage(), ButtonType.OK);
                error.show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        logger.debug("Initializing app controller");

        menuItmes[0] = keyboardMenuItem;
        menuItmes[1] = hScriptingMenuItem;
        menuItmes[2] = lScriptingMenuItem;
        menuItmes[3] = lightingMenuItem;

        menuViews[0] = keyboardMenuView;
        menuViews[1] = hScriptingMenuView;
        menuViews[2] = lScriptingMenuView;
        menuViews[3] = lightingMenuView;

        keyboardMenuItem.getStyleClass().add("selected");
        new FadeOut(hScriptingMenuView).play();
        new FadeOut(lScriptingMenuView).play();
        new FadeOut(lightingMenuView).play();
        keyboardMenuView.toFront();

        DeviceController.setStatusChangeHandler((ConnectionStatus s) -> {
            Platform.runLater(() -> {
                switch (s) {
                    case OFFLINE:
                        activateWaitOverlay("Waiting for device...");
                        statusLabel.setText("Disconnected");
                        statusCircle.setFill(Color.WHITE);
                        break;
                    case AVAILABLE:
                        activateWaitOverlay("Waiting for device...");
                        statusLabel.setText("Connecting...");
                        statusCircle.setFill(Color.CYAN);
                        break;
                    case VALID:
                        deactivateWaitOverlay();
                        statusLabel.setText("Ready");
                        statusCircle.setFill(Color.LIME);
                        break;
                    case INVALID:
                        activateWaitOverlay("Waiting for device...");
                        statusLabel.setText("Unknown device");
                        statusCircle.setFill(Color.RED);
                        break;
                    case DEVICE_OPEN:
                        statusLabel.setText("Working...");
                        statusCircle.setFill(Color.YELLOW);
                        break;
                    case OPERATION_IN_PROGRESS:
                        activateWaitOverlay("Please wait...");
                        statusLabel.setText("Working...");
                        statusCircle.setFill(Color.YELLOW);
                        break;
                }
            });
        });
    }

    /**
     * Activates the waiting overlay
     * @param text the title to show
     */
    private void activateWaitOverlay(String text) {
        logger.debug("Activating wait overlay with text '" + text + "'");
        busyBox.toFront();
        busyBox.setVisible(true);
        busyLabel.setText(text);
    }

    /**
     * Removes the waiting overlay
     */
    private void deactivateWaitOverlay() {
        busyBox.setVisible(false);
    }
}
