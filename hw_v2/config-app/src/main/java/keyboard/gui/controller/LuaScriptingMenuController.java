package keyboard.gui.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;
import keyboard.gui.BinaryMacroCodeArea;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;

import java.net.URL;
import java.util.ResourceBundle;

// This class is currently used as a placeholder.
// This feature will require a custom driver, and it's a plan for the future.
public class LuaScriptingMenuController implements Initializable {

    private static final String              sampleCode = String.join("\n", new String[]{
            "// this is a single line comment",
            "/*",
            " * this is a multi-line comment",
            "*/",
            "NOP            // Do nothing",
            "",
            "// Type 'Hello':",
            "HOLDD LShift",
            "PRESS H",
            "HOLDU LShift",
            "PRESS E",
            "PRESS L",
            "PRESS L",
            "PRESS O",
            "EXIT",
            ""
    });
    private              BinaryMacroCodeArea codeArea   = new BinaryMacroCodeArea();
    @FXML private        Pane                test;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        codeArea.replaceText(0, 0, sampleCode);

        VirtualizedScrollPane<CodeArea> pane = new VirtualizedScrollPane<>(codeArea);

        pane.setMinWidth(850);
        pane.setMinHeight(450);

        test.getChildren().add(pane);
    }
}
