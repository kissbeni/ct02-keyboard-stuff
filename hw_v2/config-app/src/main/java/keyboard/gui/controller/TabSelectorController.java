package keyboard.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Controller for a tab selector
 * @param <T> the type of the elements
 */
public class TabSelectorController<T> {
    private List<Label> items         = new ArrayList<Label>();
    @Getter
    private int         selectedIndex = -1;
    @FXML
    private HBox        rootPane;

    @Setter
    private Consumer<T>       selectedItemChangeHandler;
    @Setter
    private Consumer<Integer> selectedIndexChangeHandler;

    /**
     * @return the currently selected item
     */
    public T getSelectedItem() {
        return (T) items.get(selectedIndex).getUserData();
    }

    /**
     * Adds a new item
     * @param o the item to add
     */
    public void add(T o) {
        final int thisIndex = items.size();

        Label l = new Label(o.toString());
        l.setUserData(o);
        l.setOnMouseClicked(event -> select(thisIndex));

        l.getStyleClass().add("ts-item");
        items.add(l);
        rootPane.getChildren().add(l);

        if (selectedIndex == -1)
            select(0);
    }

    /**
     * Selects a specific item
     * @param indx the item's index
     */
    public void select(int indx) {
        if (selectedIndex == indx)
            return;

        selectedIndex = indx;
        items.forEach((Label l) -> l.getStyleClass().remove("selected"));
        items.get(indx).getStyleClass().add("selected");

        if (selectedItemChangeHandler != null)
            selectedItemChangeHandler.accept(getSelectedItem());

        if (selectedIndexChangeHandler != null)
            selectedIndexChangeHandler.accept(selectedIndex);
    }

    /**
     * Selects a specific item
     * @param item the item to select
     */
    public void select(T item) {
        for (int i = 0; i < items.size(); i++)
            if (items.get(i).getUserData() == item) {
                select(i);
                break;
            }
    }
}
