package keyboard.gui.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import keyboard.device.DeviceController;
import keyboard.device.configuration.rgb.RGBAnimationParam;
import keyboard.device.configuration.rgb.RGBAnimationType;
import keyboard.device.configuration.rgb.RGBConfiguration;
import keyboard.gui.ColorWheel;
import keyboard.utility.RGBColor;
import keyboard.utility.Utils;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for the lighting menu
 */
public class LightingMenuController implements Initializable {

    @FXML
    private Pane myAmazingPane;

    @FXML
    private HBox myAmazingHBox;

    @FXML
    private Pane colorWheelHolder;

    @FXML
    private TabSelectorController<RGBAnimationType> effectSelectorController;

    @FXML
    private Slider brightnessSlider;

    @FXML
    private Slider speedSlider;

    /**
     * The rectangle used for previewing the selected color
     */
    private Rectangle colorPreview = new Rectangle(200, 0, 50, 50);

    /**
     * The color selection wheel
     */
    private ColorWheel colorWheel = new ColorWheel(100);;

    /**
     * Indicates that the initialization is completed
     */
    private boolean initCompleted = false;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        effectSelectorController.setSelectedItemChangeHandler(this::changeAnimationType);

        for (RGBAnimationType a : RGBAnimationType.values())
            effectSelectorController.add(a);

        DeviceController.onConfigurationLoaded(this::onConfigChanged);

        myAmazingPane.setStyle("-fx-background-color: teal;");
        //colorWheelHolder.setStyle("-fx-background-color: green;");
        colorWheelHolder.getChildren().add(colorWheel.getPane());
        colorWheelHolder.setLayoutY(15);
        colorWheelHolder.setLayoutX(15);
        colorWheel.setOnChangeHandler(color -> {
            DeviceController.getConfig().rgb.setColor(color);
            colorPreview.setFill(color.ofBrightness((byte)brightnessSlider.getValue()).toPaint());
        });
        myAmazingHBox.getChildren().add(colorPreview);

        colorWheel.setOnMouseReleased(o -> apply());
        brightnessSlider.setOnMouseReleased(o -> apply());
        speedSlider.setOnMouseReleased(o -> apply());

        initCompleted = true;
    }

    /**
     * Handler for configuration changes
     */
    private void onConfigChanged() {
        Platform.runLater(() -> {
            RGBConfiguration config = DeviceController.getConfig().rgb;
            colorWheel.setCurrentColor(config.getColor());
            brightnessSlider.setValue(Utils.byte2int(config.getBrightness()));
            speedSlider.setValue(Utils.byte2int(config.getSpeed()));
            effectSelectorController.select(config.getAnimationType());

            apply();
        });
    }

    /**
     * Changes the animation type, updating all necessarry components
     *
     * @param t the new animation type
     */
    private void changeAnimationType(RGBAnimationType t) {
        boolean colorEnable = false;
        boolean brightnessEnable = false;
        boolean speedEnable = false;

        for (RGBAnimationParam param : t.getParams())
            switch (param) {
                case COLOR: colorEnable = true; break;
                case BRIGHTNESS: brightnessEnable = true; break;
                case SPEED: speedEnable = true; break;
            }

        colorWheel.setEnabled(colorEnable);
        brightnessSlider.setDisable(!brightnessEnable);
        speedSlider.setDisable(!speedEnable);

        apply();
    }

    /**
     * Builds the configuration
     *
     * @return an EEPROM compatible RGB configuration
     */
    private RGBConfiguration build() {
        RGBConfiguration c = new RGBConfiguration();
        c.setAnimationType(effectSelectorController.getSelectedItem());
        c.setBrightness((byte) brightnessSlider.getValue());
        c.setColor(colorWheel.getCurrentColor());
        c.setSpeed((byte) speedSlider.getValue());
        return c;
    }

    /**
     * Applies the configuration, which means sending a preview command
     * to the device and updating the configuration object
     */
    private void apply() {
        if (!initCompleted)
            return;

        RGBConfiguration config = DeviceController.getConfig().rgb;
        config.setBrightness((byte)brightnessSlider.getValue());
        config.setSpeed((byte)speedSlider.getValue());
        config.setAnimationType(effectSelectorController.getSelectedItem());

        if (colorWheel.isEnabled())
            colorPreview.setFill(colorWheel.getCurrentColor().ofBrightness((byte)brightnessSlider.getValue()).toPaint());
        else
            colorPreview.setFill(RGBColor.WHITE.ofBrightness((byte)brightnessSlider.getValue()).toPaint());

        DeviceController.previewRGB(build());
    }
}

