package keyboard.gui.controller;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import keyboard.device.configuration.KeySize;
import keyboard.device.configuration.KeyType;
import keyboard.device.configuration.SwitchLayoutManager;
import keyboard.device.configuration.def.*;
import keyboard.gui.ApplicationMain;
import keyboard.gui.KeyViewItem;
import keyboard.gui.event.IKeyItemEventHandler;

import java.net.URL;
import java.util.*;

/**
 * Controller for the layout view
 */
public class KeyLayoutViewController implements Initializable {

    @FXML private Pane rootPane;

    /**
     * Map of the key views (key is the KDKS id)
     */
    private Map<String, KeyViewItem>  keys            = new HashMap<String, KeyViewItem>();
    /**
     * List of attached key handlers
     */
    private Set<IKeyItemEventHandler> keyItemHandlers = new HashSet<IKeyItemEventHandler>();

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        try {
            SwitchLayoutManager m = ApplicationMain.getSwitchLayoutManager();

            List<IConfigDef> defs = m.getRootGroup().getDefs();

            double x = 0;
            double y = 0;

            for (IConfigDef d : defs) {
                if (d instanceof SeparatorDef) {
                    y += 8;
                } else if (d instanceof ConfigDefGroup) {
                    List<IConfigDef> rowDefs = ((ConfigDefGroup) d).getDefs();

                    x = 0;

                    for (IConfigDef rd : rowDefs) {
                        if (rd instanceof SeparatorDef) {
                            x += 8 + ((SeparatorDef) rd).getAlign();
                            continue;
                        }

                        if (rd instanceof LayoutKeyPlaceholderDef) {
                            LayoutKeyPlaceholderDef placeholder = (LayoutKeyPlaceholderDef) rd;
                            KeySize size = placeholder.getSize();

                            if (placeholder instanceof LayoutKeySlotDef) {
                                String id = ((LayoutKeySlotDef) placeholder).getId();

                                KeyViewItem item = new KeyViewItem(size, id, false);
                                item.setPosition(x, y);

                                if (placeholder instanceof LayoutKeyDef) {
                                    item.getContainer().setOnMouseClicked(new EventHandler<Event>() {
                                        public void handle(Event arg0) {
                                            Node n = (Node) arg0.getSource();
                                            Object obj = n.getUserData();

                                            if (obj instanceof KeyViewItem && ((KeyViewItem) obj).getType() != KeyType.Unavailable)
                                                for (IKeyItemEventHandler h : keyItemHandlers)
                                                    h.onKeyItemClick((KeyViewItem) obj);
                                        }

                                    });
                                } else {
                                    item.assignKeycode(-1);
                                }

                                rootPane.getChildren().add(item.getContainer());
                                keys.put(id, item);
                            }

                            x += size.width() + 2;
                        }
                    }

                    y += KeySize.Single.height() + 2;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a new item event handler
     * @param h the handler
     */
    public void registerHandler(IKeyItemEventHandler h) {
        keyItemHandlers.add(h);
    }

    /**
     * Finds a key item for the given KDKS id
     * @param kdks the id
     * @return the item
     */
    public KeyViewItem getViewByKDKS(String kdks) {
        return keys.get(kdks);
    }
}
