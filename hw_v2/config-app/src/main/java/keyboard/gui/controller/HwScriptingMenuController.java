package keyboard.gui.controller;

import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import keyboard.device.DeviceController;
import keyboard.gui.BinaryMacroCodeArea;
import keyboard.gui.HwScriptingSlot;
import keyboard.macro.MacroExecutionException;
import keyboard.macro.binary.BinaryMacro;
import keyboard.macro.binary.BinaryMacroAssembler;
import keyboard.macro.binary.BinaryMacroExecutionResult;
import keyboard.utility.LocalStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.ResourceBundle;

/**
 * Controller for the hardware scripting menu
 */
public class HwScriptingMenuController implements Initializable {

    private static final Logger logger    = LogManager.getLogger(HwScriptingMenuController.class);

    private static final String sampleCode = String.join("\n", new String[]{
            "// Write your macro here... :D",
            "EXIT;"
    });

    private       BinaryMacroCodeArea codeArea = new BinaryMacroCodeArea();
    @FXML private Pane                test;
    @FXML private Label               execCap;
    @FXML private Label               instCount;
    @FXML private Label               errorText;
    @FXML private VBox                slot_list;

    /**
     * List of the buttons for slot switching
     */
    private Button[] slotButtons = new Button[16];
    /**
     * The currently selected slot id
     */
    private int      selectedSlot = 0;
    /**
     * Indicates that we are transitioning between sources
     */
    private boolean  inTransition = false;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        codeArea.setOnKeyReleased(this::handleSaveKeys);
        codeArea.textProperty().addListener(this::handleTextChange);

        VirtualizedScrollPane<CodeArea> pane = new VirtualizedScrollPane<>(codeArea);

        pane.setMinWidth(890);
        pane.setMinHeight(433);

        test.getChildren().add(pane);
        reloadFiles();

        DeviceController.onConfigurationLoaded(() -> Platform.runLater(this::reloadFiles));
    }

    private void reloadFiles() {
        for (int i = 0; i < slotButtons.length; i++) {
            HwScriptingSlot data;

            if (slotButtons[i] == null) {
                slotButtons[i] = new Button("SLOT " + (i + 1));

                if (i == 0)
                    slotButtons[i].getStyleClass().add("current");

                data = new HwScriptingSlot(i);

                slotButtons[i].setOnMouseClicked(this::onSlotButtonClicked);
                slotButtons[i].setUserData(data);

                slot_list.getChildren().add(slotButtons[i]);
            } else {
                data = (HwScriptingSlot)slotButtons[i].getUserData();
            }

            File f = LocalStorage.getBinaryMacroFile(i);

            if (f.exists()) {
                try { logger.debug("Loading source code for macro from " + f.getCanonicalPath()); } catch (IOException e) { e.printStackTrace(); }

                try {
                    data.setSourceCode(Files.readString(f.toPath()));
                } catch (IOException e) {
                    logger.error("Could not load file for macro slot " + i, e);
                }
            } else {
                try { logger.debug("Creating empty macro script at " + f.getCanonicalPath()); } catch (IOException e) { e.printStackTrace(); }

                try {
                    Files.writeString(f.toPath(), sampleCode);
                } catch (IOException e) {
                    logger.error("Could not write sample code for macro slot " + i, e);
                }

                data.setSourceCode(sampleCode);
            }

            slotButtons[i].getStyleClass().remove("modified");
        }

        codeArea.clear();
        codeArea.replaceText(0, 0, ((HwScriptingSlot)slotButtons[selectedSlot].getUserData()).getSourceCode());
        testAssemble();
    }

    /**
     * Handles the text change events of the text area
     *
     * @param observable the observable of the text
     */
    private void handleTextChange(Observable observable) {
        if (inTransition)
            return;

        testAssemble();
        HwScriptingSlot slot = (HwScriptingSlot)slotButtons[selectedSlot].getUserData();

        if (!codeArea.getText().equals(slot.getSourceCode())) {
            if (!slotButtons[selectedSlot].getStyleClass().contains("modified"))
                slotButtons[selectedSlot].getStyleClass().add("modified");
        }
    }

    /**
     * Handles the Ctrl+S key combination
     * @param keyEvent the key event
     */
    private void handleSaveKeys(KeyEvent keyEvent) {
        if (keyEvent.isControlDown() && keyEvent.getCode() == KeyCode.S) {
            HwScriptingSlot slot = (HwScriptingSlot)slotButtons[selectedSlot].getUserData();
            slot.setSourceCode(codeArea.getText());

            try {
                Files.writeString(LocalStorage.getBinaryMacroFile(slot.getId()).toPath(), slot.getSourceCode());
                slotButtons[selectedSlot].getStyleClass().remove("modified");
            } catch (IOException ex) {
                logger.error("Failed to save macro #" + slot.getId(), ex);
            }
        }
    }

    /**
     * Click handler for slot switching
     * @param mouseEvent the event
     */
    private void onSlotButtonClicked(MouseEvent mouseEvent) {
        Node target = (Node)mouseEvent.getTarget();

        // For handling clicking on the button's label
        if (!(target instanceof Button))
            target = target.getParent();

        HwScriptingSlot slot = (HwScriptingSlot)target.getUserData();
        if (slot.getId() == selectedSlot)
            return;

        inTransition = true;
        ((HwScriptingSlot)slotButtons[selectedSlot].getUserData()).setSourceCode(codeArea.getText());
        slotButtons[selectedSlot].getStyleClass().remove("current");
        selectedSlot = slot.getId();
        slotButtons[selectedSlot].getStyleClass().add("current");
        codeArea.clear();
        codeArea.replaceText(0, 0, ((HwScriptingSlot)slotButtons[selectedSlot].getUserData()).getSourceCode());
        testAssemble();
        inTransition = false;
    }

    /**
     * Tries to assemble the macro
     */
    private void testAssemble() {
        BinaryMacroAssembler asm = new BinaryMacroAssembler();

        try {
            asm.parse(new StringReader(codeArea.getText()));
        } catch (ParseException e) {
            errorText.setText(String.format("%s on line %d\n", e.getMessage(), e.getErrorOffset()));
            errorText.setTextFill(Paint.valueOf("#ee4444"));
            return;
        }

        errorText.setText("No errors.");
        errorText.setTextFill(Paint.valueOf("#008800"));

        instCount.setText("Instructions:  " + asm.getResult().instructions.size() + "/50");
        try {
            BinaryMacroExecutionResult res = asm.getResult().simulateExecution();
            execCap.setText("Execution cap: " + res.getCyclesUsed() + "/" + BinaryMacro.EXEC_CAP);
        } catch (MacroExecutionException e) {
            execCap.setText("Execution cap: <ERROR>/" + BinaryMacro.EXEC_CAP);
            errorText.setText("Invalid macro: " + e.getMessage());
            errorText.setTextFill(Paint.valueOf("#ee4444"));
        }
    }
}
