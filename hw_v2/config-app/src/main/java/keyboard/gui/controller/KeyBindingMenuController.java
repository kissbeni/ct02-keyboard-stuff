package keyboard.gui.controller;

import animatefx.animation.BounceIn;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import keyboard.device.DeviceController;
import keyboard.device.FirmwareConstants;
import keyboard.device.configuration.def.ConfigDefGroup;
import keyboard.device.configuration.def.IConfigDef;
import keyboard.device.configuration.def.KeycodeConfigDef;
import keyboard.device.configuration.eeprom.EEPROMConfiguration;
import keyboard.device.configuration.eeprom.EEPROMKeyBinding;
import keyboard.gui.ApplicationMain;
import keyboard.gui.KeyBindingItem;
import keyboard.gui.KeyViewItem;
import keyboard.gui.event.IKeyItemEventHandler;
import keyboard.utility.DefaultsLoader;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

/**
 * Controller for the key layout menu
 */
public class KeyBindingMenuController implements Initializable, IKeyItemEventHandler {
    @Getter
    private static final Logger logger = LogManager.getLogger(KeyBindingMenuController.class);

    @FXML private KeyLayoutViewController  keyLayoutViewController;
    //@FXML private TabSelectorController bindingTypeSelectorController;
    @FXML private ListView<KeyBindingItem> filteredKeycodes;

    @FXML private Pane  largeKeyHolder;
    @FXML private Label largeKeyKDKS;
    @FXML private Label largeKeyMux;
    @FXML private Label largeKeyDmux;
    @FXML private Label largeKeySid;
    @FXML private Label largeKeyDefault;
    @FXML private Label largeKeyDefaultKc;
    @FXML private Label largeKeyCurrent;
    @FXML private Label largeKeyCurrentKc;
    @FXML private Label largeKeyOffset;

    @FXML private Button    profPrev;
    @FXML private TextField profCurrentId;
    @FXML private TextField profCurrentName;
    @FXML private Button    profNext;
    @FXML private Button    profEnable;
    @FXML private Button    profReset;
    @FXML private Button    profCurrent;

    @FXML private TextField keyFilter;

    /**
     * The currently displayed large key
     */
    private KeyViewItem currentLargeKey;
    /**
     * The previously selected key
     */
    private KeyViewItem prevSelected;
    /**
     * The currently selected key
     */
    private KeyViewItem currentKey;

    /**
     * Indicates that we need to bypass the selection change event
     */
    private boolean             bypassOnSelect   = false;
    /**
     * List of the key codes to list
     */
    private Set<KeyBindingItem> bindableKeycodes = new TreeSet<>();

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initKeys("", ApplicationMain.getKeycodeManager().getRootGroup());

        keyLayoutViewController.registerHandler(this);
        filteredKeycodes.getSelectionModel().clearAndSelect(0);

        DeviceController.onConfigurationLoaded(this::updateFromConfig);

        //bindingTypeSelectorController.add("Disabled");
        //bindingTypeSelectorController.add("Keycode");
        //bindingTypeSelectorController.add("Script");
        //bindingTypeSelectorController.select(1);

        filteredKeycodes.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<KeyBindingItem>() {

            @Override
            public void changed(ObservableValue<? extends KeyBindingItem> observable, KeyBindingItem oldValue, KeyBindingItem newValue) {
                if (bypassOnSelect || newValue == null) return;

                if (currentKey == null) {
                    logger.error("Current key was null!", new NullPointerException());
                    return;
                }

                if (currentLargeKey == null) {
                    logger.error("Current large key was null!", new NullPointerException());
                    return;
                }

                currentLargeKey.assignKeycode((int) newValue.getDef().getCode());
                currentKey.assignKeycode((int) newValue.getDef().getCode());
                updateLargeKey();
            }
        });

        updateKeyFilter();
    }

    /**
     * Initializes bindable keys
     * @param path path of the group
     * @param rootGroup the root group
     */
    private void initKeys(String path, ConfigDefGroup rootGroup) {
        for (IConfigDef def : rootGroup) {
            if (def instanceof ConfigDefGroup)
                initKeys(path + ((ConfigDefGroup) def).getName() + "/", (ConfigDefGroup) def);
            else if (def instanceof KeycodeConfigDef)
                bindableKeycodes.add(new KeyBindingItem(path, (KeycodeConfigDef) def));
        }
    }

    @Override
    public void onKeyItemClick(KeyViewItem item) {
        if (item == null) {
            logger.error("onKeyItemClick called with null parameter!");
            return;
        }

        currentKey = item;
        bypassOnSelect = true;

        Optional<KeyBindingItem> i = bindableKeycodes.stream().filter((KeyBindingItem b) -> b.getDef().getCode() == item.getAssignedKeycode()).findFirst();
        if (i.isPresent()) {
            if (!filteredKeycodes.getItems().contains(i.get()))
                filteredKeycodes.getItems().add(i.get());

            filteredKeycodes.getSelectionModel().select(i.get());
            updateKeyFilter();
        } else
            filteredKeycodes.getSelectionModel().select(null);

        bypassOnSelect = false;
        setLargeKey(item);

        /*if (item.getType() == KeyType.Macro)
            bindingTypeSelectorController.select(2);
        else if (item.getAssignedKeycode() == 0)
            bindingTypeSelectorController.select(0);
        else
            bindingTypeSelectorController.select(1);*/
    }

    /**
     * Updates information from the current configuration
     *
     * @see EEPROMConfiguration
     */
    private void updateFromConfig() {
        if (currentLargeKey == null) {
            DeviceController.getLastPressedKey((String kdks) -> {
                logger.debug("Got last pressed key: {}", kdks);
                Platform.runLater(() -> {
                    onKeyItemClick(keyLayoutViewController.getViewByKDKS(kdks));
                });
            });
        }

        Platform.runLater(() -> {
            EEPROMConfiguration cfg = DeviceController.getConfig();
            profCurrentId.setText(cfg.currentProfile + "");
            profCurrentName.setText(cfg.getCurrentProfile().name);

            if (cfg.getCurrentProfile().isEnabled)
                profEnable.setText("Disable");
            else
                profEnable.setText("Enable");

            profReset.setDisable(cfg.getCurrentProfile().isDefaultsOnly());
            profCurrent.setDisable(cfg.currentProfile == cfg.defaultProfile);

            if (currentLargeKey != null)
                updateLargeKey();

            updateProfButtons();
        });
    }

    /**
     * Sets the displayed large key
     * @param item the item to make large
     */
    private void setLargeKey(KeyViewItem item) {
        if (item == prevSelected)
            return;

        largeKeyHolder.getChildren().clear();

        if (prevSelected != null)
            prevSelected.setSelected(false);

        prevSelected = item;
        item.setSelected(true);

        try {
            currentLargeKey = new KeyViewItem(item.getSize(), item.getId(), true);
            currentLargeKey.setSelected(true);
            largeKeyHolder.getChildren().add(currentLargeKey.getContainer());
            new BounceIn(largeKeyHolder).setSpeed(2).play();
            updateLargeKey();
        } catch (Exception ex) {
            logger.error("Failed to create large key", ex);
        }
    }

    /**
     * Updates the current large key
     */
    private void updateLargeKey() {
        largeKeyKDKS.setText("KDKS: " + currentLargeKey.getId());

        Optional<EEPROMKeyBinding> bindOpt = currentLargeKey.getBinding();
        if (bindOpt.isPresent()) {
            EEPROMKeyBinding bind = bindOpt.get();

            largeKeyMux.setText("Mux ID: " + bind.getMuxId());
            largeKeyDmux.setText("Demux ID: " + bind.getDmuxId());
            largeKeySid.setText("Short ID: " + bind.shortId());

            Optional<KeycodeConfigDef> defOpt = DefaultsLoader.defaultOfKDKS(currentLargeKey.getId());
            if (defOpt.isPresent()) {
                largeKeyDefault.setText("Default: [" + defOpt.get().getFullName() + "]");
                largeKeyDefaultKc.setText(String.format("         0x%02x", defOpt.get().getCode()));
            }

            KeycodeConfigDef curr = ApplicationMain.getKeycodeManager().lookupKeycode(bind.getKeycode());

            if (curr == null)
                logger.warn(
                        "Keycode lookup failed for large key display (keycode: {})",
                        String.format("0x%02x", bind.getKeycode())
                );

            largeKeyCurrent.setText("Current: [" + curr.getFullName() + "]");
            largeKeyCurrentKc.setText(String.format("         0x%02x", curr.getCode()));

            if (bind.getOffset() == -1)
                largeKeyOffset.setText("EEPROM offset: NOT IN CONFIG");
            else
                largeKeyOffset.setText(String.format("EEPROM offset: 0x%04x (%d)", bind.getOffset(), bind.getOffset()));
        }
    }

    /**
     * Click handler for the previous profile button
     */
    public void onClickProfPrev() {
        if (DeviceController.getConfig().currentProfile == 0)
            return;

        DeviceController.getConfig().currentProfile--;
        DeviceController.runConfigLoadedCallbacks();
    }

    /**
     * Click handler for the next profile button
     */
    public void onClickProfNext() {
        if (DeviceController.getConfig().currentProfile == FirmwareConstants.MAX_PROFILES - 1)
            return;

        DeviceController.getConfig().currentProfile++;
        DeviceController.runConfigLoadedCallbacks();
    }

    /**
     * Updates profile buttons
     */
    private void updateProfButtons() {
        profPrev.setDisable(DeviceController.getConfig().currentProfile == 0);
        profNext.setDisable(DeviceController.getConfig().currentProfile == FirmwareConstants.MAX_PROFILES - 1);
    }

    /**
     *  Click handler for the profile enable button
     */
    public void onClickProfEnable() {
        EEPROMConfiguration cfg = DeviceController.getConfig();

        if (cfg.getCurrentProfile().isEnabled) {
            profEnable.setText("Enable");
            cfg.getCurrentProfile().isEnabled = false;
        } else {
            profEnable.setText("Disable");
            cfg.getCurrentProfile().isEnabled = true;
        }
    }

    /**
     * Click handler for the reset profile button
     */
    public void onClickProfReset() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Restore defaults?", ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            DeviceController.getConfig().getCurrentProfile().restoreDefaults();
            DeviceController.runConfigLoadedCallbacks();
        }
    }

    /**
     * Click handler for the set as current button
     */
    public void onClickSelect() {
        DeviceController.setCurrentProfile(DeviceController.getConfig().currentProfile);
        DeviceController.getConfig().defaultProfile = DeviceController.getConfig().currentProfile;
    }

    /**
     * Keyboard input handler for the profile name
     */
    public void onTypeCurrentName() {
        DeviceController.getConfig().getCurrentProfile().name = profCurrentName.getText();
    }

    /**
     * Updates the keycode filter
     */
    public void updateKeyFilter() {
        KeyBindingItem sel = filteredKeycodes.getSelectionModel().getSelectedItem();
        filteredKeycodes.getItems().clear();
        final String filter = keyFilter.getText().toLowerCase();

        bindableKeycodes.forEach((KeyBindingItem item) -> {
            if (item.passesFilter(filter))
                filteredKeycodes.getItems().add(item);
        });

        if (sel != null) {
            bypassOnSelect = true;
            if (!filteredKeycodes.getItems().contains(sel))
                filteredKeycodes.getItems().add(sel);

            filteredKeycodes.getSelectionModel().select(sel);
            bypassOnSelect = false;
        }
    }
}
