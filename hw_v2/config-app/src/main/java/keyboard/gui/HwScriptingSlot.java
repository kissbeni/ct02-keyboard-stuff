package keyboard.gui;

import lombok.Getter;
import lombok.Setter;

/**
 * A slot holding a hardware macro script
 */
public class HwScriptingSlot {
    /**
     * The id of the slot
     */
    @Getter
    private int id;

    /**
     * The source code for this slot
     */
    @Getter
    @Setter
    private String sourceCode;

    /**
     * Initializes the instance with the given id
     * @param id the slot's id
     */
    public HwScriptingSlot(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("[id=%d,code=%s]", id, sourceCode);
    }
}
