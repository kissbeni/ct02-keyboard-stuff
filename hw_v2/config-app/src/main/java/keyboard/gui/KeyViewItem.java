package keyboard.gui;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import keyboard.device.DeviceController;
import keyboard.device.configuration.KeySize;
import keyboard.device.configuration.KeyType;
import keyboard.device.configuration.def.KeycodeConfigDef;
import keyboard.device.configuration.eeprom.EEPROMKeyBinding;
import keyboard.gui.controller.KeyBindingMenuController;
import keyboard.utility.DefaultsLoader;
import lombok.Getter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Class for a key view item
 */
public class KeyViewItem {
    /**
     * The size of this key
     */
    @Getter
    private KeySize size;
    /**
     * The type of this key
     */
    @Getter
    private KeyType type = KeyType.Unavailable;
    /**
     * The KDKS id of this key
     */
    @Getter
    private String  id;
    /**
     * The container for the key
     */
    @Getter
    private Pane    container;

    /**
     * The labels on this key
     */
    private List<Label> labels = new ArrayList<Label>();

    /**
     * Creates the key view item
     * @param size the size of the key
     * @param id the kdks id of the key
     * @param large whether this is a large key or not
     * @throws IOException
     */
    public KeyViewItem(KeySize size, String id, boolean large) throws IOException {
        this.size = size;
        this.id = id;

        String res = "/ui/fxml/keys/KeyView-" + size.name() + (large && size == KeySize.Enter ? "-Large" : "") + ".fxml";

        KeyBindingMenuController.getLogger().debug("Creating KVI size={},id={},large={} using {}", size, id, large, res);

        container = FXMLLoader.load(ApplicationMain.class.getResource(res));
        container.getStyleClass().add("keysize" + size.name());

        container.setUserData(this);

        if (large)
            container.getStyleClass().add("large");

        collectLabels(container);

        DeviceController.onConfigurationLoaded(() -> {
            Platform.runLater(() -> {
                reloadFromConfig();

                if (DeviceController.shouldForceDefaults())
                    loadDefault();
            });
        });
    }

    /**
     * Sets the position of this item
     * @param x the new x
     * @param y the new y
     */
    public void setPosition(double x, double y) {
        container.setLayoutX(x);
        container.setLayoutY(y);
    }

    /**
     * Sets the label on this key
     * @param val the new label
     */
    public void setLabel(String val) {
        for (Label label : labels) {
            label.getStyleClass().remove("longtext");
            if (val.length() > size.maxChars() || (val.length() >= (size.maxChars() - 1) && Character.isLowerCase(val.charAt(1)))) {
                val = val.replaceAll(" ", "\n");
                label.getStyleClass().add("longtext");
            }

            label.setText(val);
        }
    }

    /**
     * Sets the type of the key
     * @param t the new type
     */
    public void setType(KeyType t) {
        container.getStyleClass().remove("keytype" + type.name());
        type = t;
        container.getStyleClass().add("keytype" + type.name());
    }

    /**
     * Sets whether this key is selected or not
     * @param sel the value
     */
    public void setSelected(boolean sel) {
        if (sel)
            container.getStyleClass().add("selected");
        else
            container.getStyleClass().remove("selected");
    }

    /**
     * Finds all labels in the key view item
     * @param n the root node
     */
    private void collectLabels(Node n) {
        if (n instanceof Pane) {
            for (Node d : ((Pane) n).getChildren()) {
                if (d instanceof Label && d.getStyleClass().contains("key-view-letter"))
                    labels.add((Label) d);
                else
                    collectLabels(d);
            }
        }
    }

    /**
     * Assign a new keycode to this item
     * This also updates the current profile
     *
     * @param i the new keycode
     */
    public void assignKeycode(int i) {
        KeyBindingMenuController.getLogger().debug("assignKeycode(" + i + ")");
        Optional<EEPROMKeyBinding> config = getBinding();

        if (config.isPresent()) {
            EEPROMKeyBinding bind = config.get();
            bind.setKeycode((short) i);
            reloadFromConfig();
        }
    }

    /**
     * Restores the default keycode of this key
     */
    public void loadDefault() {
        Optional<KeycodeConfigDef> key = DefaultsLoader.defaultOfKDKS(id);

        if (key.isPresent()) {
            KeyBindingMenuController.getLogger().debug("Loading defaults for {}", id);
            assignKeycode((int) key.get().getCode());
        }
    }

    /**
     * Reloads this item from configuration
     * (the current profile is being used)
     */
    public void reloadFromConfig() {
        Optional<EEPROMKeyBinding> config = getBinding();

        if (config.isPresent()) {
            EEPROMKeyBinding bind = config.get();

            setTypeByKeycode(bind.getKeycode());

            KeycodeConfigDef def = ApplicationMain.getKeycodeManager().lookupKeycode(bind.getKeycode());

            if (def != null) {
                setLabel(def.getDisplayName());
            } else {
                KeyBindingMenuController.getLogger().warn("Keycode missing from keycode manager for {} (keycode={})", id, String.format("0x%04x", bind.getKeycode()));
                setLabel("??");
            }
        } else {
            KeyBindingMenuController.getLogger().warn("Unable to get key bindig info for {}", id);
            setTypeByKeycode(-1);
        }
    }

    /**
     * @return the binding information of this key
     */
    public Optional<EEPROMKeyBinding> getBinding() {
        return DeviceController.getConfig().getCurrentProfile().keyByKDKSId(id);
    }

    /**
     * Sets the type by a keycode
     * @param i the keycode
     */
    private void setTypeByKeycode(int i) {
        if (i == 0) {
            if (!ApplicationMain.getSwitchLayoutManager().isAvailable(id)) {
                setType(KeyType.Unavailable);
                assignKeycode(-1);
                return;
            }

            setType(KeyType.Unmapped);
        } else if (i == -1 || i == 0xFF) {
            setType(KeyType.Unavailable);
        // } else if (i >= 0xF0) {
        //    setType(KeyType.Macro);
        } else {
            setType(KeyType.Generic);
        }
    }

    /**
     * @return the currently assigned keycode
     */
    public int getAssignedKeycode() {
        return getBinding().map(EEPROMKeyBinding::getKeycode).orElse((short) -1);
    }
}
