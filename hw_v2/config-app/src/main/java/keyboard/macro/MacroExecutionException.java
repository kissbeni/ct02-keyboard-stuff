package keyboard.macro;

/**
 * An exception used in macro simulation
 */
public class MacroExecutionException extends Exception {
    private static final long serialVersionUID = 473884008990548945L;

    public MacroExecutionException(String message) {
        super(message);
    }
}
