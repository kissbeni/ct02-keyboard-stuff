package keyboard.macro.binary.instruction;

import keyboard.gui.ApplicationMain;
import keyboard.macro.MacroExecutionException;
import keyboard.macro.binary.BinaryMacroExecutionContext;
import keyboard.utility.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class MacroInstructionHOLDU extends MacroInstruction {

    /**
     * The keycode argument
     */
    @Getter
    private byte keycode;

    @Override
    public void decodeArgument(short argument) {
        keycode = (byte) (argument & 0xFF);
    }

    @Override
    public short encodeArgument() {
        return Utils.byte2short(keycode);
    }

    @Override
    public void execute(BinaryMacroExecutionContext context) throws MacroExecutionException {
        context.setKeyDown(keycode, false);
    }

    @Override
    public void reset() {
    }

    @Override
    public void setArgument(int indx, String value) {
        if (indx == 0)
            keycode = (byte) ApplicationMain.getKeycodeManager().parse(value).getCode();
    }

    @Override
    public String toString() {
        return super.toString() + " " + ApplicationMain.getKeycodeManager().nameOf(keycode & 0xff);
    }
}
