package keyboard.macro.binary;

import keyboard.macro.MacroExecutionException;
import keyboard.macro.binary.instruction.MacroInstruction;
import keyboard.macro.binary.instruction.MacroInstructionEXIT;
import keyboard.macro.binary.instruction.MacroInstructionNOP;
import keyboard.utility.FNVHash;
import keyboard.utility.Utils;

import java.nio.ByteBuffer;
import java.util.*;

/**
 * Class for encoding, decoding and simulating binary macros
 */
public class BinaryMacro {

    public static final int EXEC_CAP = 1000;

    /**
     * List of instructions
     */
    public List<MacroInstruction> instructions = new ArrayList<MacroInstruction>();

    /*
     * Instruction encoding:
     *
     * Instructions are identified by a 4-bit opcode.
     *
     * If 2 instructions require a single (<=8 bit) argument, they're is encoded like this:
     * +-------------+------+------+
     * | OPC1 ; OPC2 | ARG1 | ARG2 |
     * +-------------+------+------+
     *
     * If only one of them:
     * +-------------+------+
     * | OPC1 ; OPC2 | ARG1 |
     * +-------------+------+
     * or:
     * +-------------+------+
     * | OPC1 ; OPC2 | ARG2 |
     * +-------------+------+
     *
     * If an instruction has only one argument and that argument is >8 but <=12 bit long:
     * +---------------+---------+
     * | OPC ; ARG-HI4 | ARG-LO8 |
     * +---------------+---------+
     *
     * If a single argument instruction is followed by a long argument instruction, then nop(s) are inserted:
     * +------------+------+-----------------+----------+
     * | OPC1 ; NOP | ARG1 | OPC2 ; ARG2-HI4 | ARG2-LO8 |
     * +------------+------+-----------------+----------+
     *
     * There are two-argument instructions, they use a larger argument to encode them.
     *
     * */

    /**
     * Decodes a binary macro script from the given buffer
     * @param buffer the buffer
     */
    public void decode(ByteBuffer buffer) {
        // maximum 50 instructions
        for (int i = 0; i < 50; i++) {
            short first = Utils.byte2short(buffer.get());

            MacroOpcode opc = MacroOpcode.fromNumber(first >>> 4);

            if (opc == MacroOpcode.EXIT) {
                instructions.add(new MacroInstructionEXIT());
                break;
            }

            if (opc == null)
                throw new Error("Malformed macro bytecode");

            MacroInstruction inst = opc.newInstance();

            if (opc.getArgsSize() > 8) {
                short arg = (short) ((first & 0xF) << 8 | buffer.get());

                inst.decodeArgument(arg);
                instructions.add(inst);
            } else {
                if (opc.getArgsSize() > 0)
                    inst.decodeArgument(buffer.get());

                instructions.add(inst);

                opc = MacroOpcode.fromNumber(first & 0xF);

                if (opc == MacroOpcode.EXIT) {
                    instructions.add(new MacroInstructionEXIT());
                    break;
                }

                if (opc == null || opc.getArgsSize() > 8)
                    throw new Error("Malformed macro bytecode");

                inst = opc.newInstance();
                if (opc.getArgsSize() > 0)
                    inst.decodeArgument(buffer.get());

                instructions.add(inst);
            }
        }
    }

    /**
     * Encodes this macro script into the given buffer
     * @param b the buffer
     */
    public void encode(ByteBuffer b) {
        Queue<MacroInstruction> instStack = new ArrayDeque<MacroInstruction>(instructions);

        MacroOpcode opc2;

        while (!instStack.isEmpty()) {
            MacroInstruction inst = instStack.remove();

            MacroOpcode opc = MacroOpcode.fromInstance(inst);

            if (opc.getArgsSize() > 8) {
                short arg = inst.encodeArgument();
                b.put((byte) (((opc.numeric() << 4) & 0xF0) | ((arg >>> 8) & 0xF)));
                b.put((byte) (arg & 0xFF));
            } else {
                short arg1 = inst.encodeArgument();

                inst = instStack.isEmpty() || MacroOpcode.fromInstance(instStack.peek()).getArgsSize() > 8 ? new MacroInstructionNOP() : instStack.remove();
                opc2 = MacroOpcode.fromInstance(inst);

                b.put((byte) (((opc.numeric() << 4) & 0xF0) | opc2.numeric()));

                if (opc.getArgsSize() > 0)
                    b.put((byte) arg1);

                if (opc2.getArgsSize() > 0)
                    b.put((byte) inst.encodeArgument());
            }
        }
    }

    /**
     * Simulates the execution of this macro script (no actual key presses are done,
     * everything happens inside the execution context)
     *
     * @return the result of the execution
     * @throws MacroExecutionException
     */
    public BinaryMacroExecutionResult simulateExecution() throws MacroExecutionException {
        instructions.forEach(MacroInstruction::reset);

        BinaryMacroExecutionContext context = new BinaryMacroExecutionContext();
        context.reset();

        int execCap = EXEC_CAP;

        while (!context.exited()) {
            if (context.getProgramCounter() >= instructions.size())
                throw new MacroExecutionException("Macro must end with an EXIT instruction");

            MacroInstruction inst = instructions.get(context.getProgramCounter());
            context.progressProgramCounter();
            inst.execute(context);

            if (--execCap == 0)
                throw new MacroExecutionException("Reached execution cap");
        }

        return new BinaryMacroExecutionResult(execCap);
    }

    /**
     * Generates a valid source code representation of this macro
     * @return the source code
     */
    public String toSourceCode() {
        StringBuilder res = new StringBuilder();
        instructions.forEach((MacroInstruction i) -> res.append(i).append(";\n"));
        return res.toString();
    }

    @Override
    public int hashCode() {
        ByteBuffer buffer = ByteBuffer.allocate(128);
        encode(buffer);

        return FNVHash.hash32(buffer.array(), 0, buffer.position());
    }
}
