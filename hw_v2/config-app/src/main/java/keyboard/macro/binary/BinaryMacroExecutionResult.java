package keyboard.macro.binary;

/**
 * The result of the simulation of a binary macro
 */
public class BinaryMacroExecutionResult {
    private int remainingExecCap;

    /**
     * Initializes an execution result
     * @param remainingExecCap the remaining execution cap
     */
    BinaryMacroExecutionResult(int remainingExecCap) {
        this.remainingExecCap = remainingExecCap;
    }

    /**
     * @return the number of cycles used by the simulation
     */
    public int getCyclesUsed() {
        return BinaryMacro.EXEC_CAP - remainingExecCap;
    }
}
