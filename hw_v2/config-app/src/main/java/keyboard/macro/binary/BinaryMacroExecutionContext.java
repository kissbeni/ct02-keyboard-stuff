package keyboard.macro.binary;

import keyboard.macro.MacroExecutionException;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;

/**
 * Context for binary macro execution
 */
public class BinaryMacroExecutionContext {
    @Getter
    @Setter
    private int     programCounter;
    private boolean exited;

    private byte[] keysDown = new byte[5];

    /**
     * Resets the context
     */
    public void reset() {
        setProgramCounter(0);
        exited = false;

        Arrays.fill(keysDown, (byte) 0);
    }

    /**
     * Sets a key's state
     * @param kc the keycode
     * @param down the state of the key (pressed down or not)
     * @throws MacroExecutionException
     */
    public void setKeyDown(byte kc, boolean down) throws MacroExecutionException {
        for (int i = 0; i < keysDown.length; i++) {
            if (down && keysDown[i] == 0) {
                keysDown[i] = kc;
                return;
            }

            if (!down && keysDown[i] == kc) {
                keysDown[i] = 0;
                return;
            }
        }

        if (down) throw new MacroExecutionException("too many keys are down");
    }

    /**
     * Preform a relative jump
     * @param offs the offset to jump
     * @throws MacroExecutionException
     */
    public void jumpRelative(int offs) throws MacroExecutionException {
        programCounter += offs;

        if (programCounter < 0)
            throw new MacroExecutionException("relative jump is out of range");
    }

    /**
     * Advances the program counter
     */
    public void progressProgramCounter() {
        programCounter++;
    }

    /**
     * Indicates that the program is exited
     */
    public void exit() {
        exited = true;
    }

    /**
     * @return true if the program is exited
     */
    public boolean exited() {
        return exited;
    }
}
