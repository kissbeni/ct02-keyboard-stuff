package keyboard.macro.binary.instruction;

import keyboard.macro.binary.BinaryMacroExecutionContext;

public class MacroInstructionEXIT extends MacroInstruction {

    @Override
    public void decodeArgument(short argument) {
    }

    @Override
    public short encodeArgument() {
        return 0;
    }

    @Override
    public void execute(BinaryMacroExecutionContext context) {
        context.exit();
    }

    @Override
    public void reset() {
    }

    @Override
    public void setArgument(int indx, String value) {
    }
}
