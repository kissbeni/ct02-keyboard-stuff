package keyboard.macro.binary.instruction;

import keyboard.macro.MacroExecutionException;
import keyboard.macro.binary.BinaryMacroExecutionContext;
import keyboard.utility.Utils;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MacroInstructionREPN extends MacroInstruction {

    /**
     * The offset to jump back to
     */
    @Getter
    private int offset;
    /**
     * Number of times to repeat
     */
    @Getter
    private int times;

    /**
     * The number of repeats remaining
     */
    private int remaining;

    public MacroInstructionREPN(int offset, int times) {
        this.offset = offset & 0x3F;
        this.times = times & 0x3F;
    }

    @Override
    public void decodeArgument(short argument) {
        offset = argument & 0x3F;
        times = (argument >>> 6) & 0x3F;
    }

    @Override
    public short encodeArgument() {
        return (short) ((offset & 0x3F) | ((times & 0x3F) << 6));
    }

    @Override
    public void execute(BinaryMacroExecutionContext context) throws MacroExecutionException {
        if (remaining > 0) {
            context.jumpRelative(-offset);
            remaining--;
        }
    }

    @Override
    public void reset() {
        remaining = times;
    }

    @Override
    public void setArgument(int indx, String value) {
        if (indx == 0)
            offset = Utils.byte2short(Byte.parseByte(value));
        else if (indx == 1)
            times = Utils.byte2short(Byte.parseByte(value));
    }

    @Override
    public String toString() {
        return super.toString() + " " + offset + ", " + times;
    }
}
