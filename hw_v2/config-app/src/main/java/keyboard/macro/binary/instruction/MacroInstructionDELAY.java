package keyboard.macro.binary.instruction;

import keyboard.macro.binary.BinaryMacroExecutionContext;
import keyboard.utility.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class MacroInstructionDELAY extends MacroInstruction {
    /**
     * The delay argument
     */
    @Getter
    private byte delay;

    @Override
    public void decodeArgument(short argument) {
        delay = (byte) (argument & 0xFF);
    }

    @Override
    public short encodeArgument() {
        return Utils.byte2short(delay);
    }

    @Override
    public void execute(BinaryMacroExecutionContext context) {
        Utils.sleep(delay);
    }

    @Override
    public void reset() {
    }

    @Override
    public void setArgument(int indx, String value) {
        if (indx == 0)
            delay = Byte.parseByte(value);
    }

    @Override
    public String toString() {
        return super.toString() + " " + delay;
    }
}
