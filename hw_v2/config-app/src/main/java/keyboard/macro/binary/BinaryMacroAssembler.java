package keyboard.macro.binary;

import keyboard.macro.binary.instruction.MacroInstruction;
import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for parsing and assembling binary macros
 */
public class BinaryMacroAssembler {

    private static final Pattern p0arg, p1arg, p2arg;

    static {
        List<String> arg0 = new ArrayList<String>();
        List<String> arg1 = new ArrayList<String>();
        List<String> arg2 = new ArrayList<String>();

        for (MacroOpcode o : MacroOpcode.values()) {
            if (o.getArgsSize() == 0)
                arg0.add(o.name());
            else if (o.getArgsSize() <= 8)
                arg1.add(o.name());
            else
                arg2.add(o.name());
        }

        p0arg = Pattern.compile("^\\s*(" + String.join("|", arg0) + ")\\s*;\\s*$");
        p1arg = Pattern.compile("^\\s*(" + String.join("|", arg1) + ")\\s+(.+?)\\s*;\\s*$");
        p2arg = Pattern.compile("^\\s*(" + String.join("|", arg2) + ")\\s+(.+?)\\s*,\\s*(.+?)\\s*;\\s*$");
    }

    @Getter
    private BinaryMacro result    = new BinaryMacro();
    private boolean     inComment = false;

    /**
     * Resets the result
     */
    public void reset() {
        result.instructions.clear();
        inComment = false;
    }

    /**
     * Assembles a script read from a reader
     * @param rd the reader
     * @throws ParseException
     */
    public void parse(Reader rd) throws ParseException {
        BufferedReader br = new BufferedReader(rd);

        int n = 0;
        String ln;
        try {
            while ((ln = br.readLine()) != null)
                feedLine(++n, ln);
        } catch (IOException e) {
            ParseException ex = new ParseException("Failed to read source", n);
            ex.initCause(e);
            throw ex;
        } catch (Exception e) {
            ParseException ex = new ParseException(e.getMessage(), n);
            ex.initCause(e);
            throw ex;
        }

        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Processes a single line into an instruction
     * @param lineNo the line number
     * @param rawLine the line as a string
     * @throws ParseException
     */
    private void feedLine(int lineNo, String rawLine) throws ParseException {
        int offset = 0;

        char prev = 0;
        while (inComment && offset < rawLine.length()) {
            char curr = rawLine.charAt(offset++);

            if (prev == '*' && curr == '/')
                inComment = false;

            prev = curr;
        }

        if (offset == rawLine.length())
            return;

        rawLine = rawLine.substring(offset);

        for (offset = 0; offset < rawLine.length(); offset++) {
            char curr = rawLine.charAt(offset);

            if (prev == '/') {
                if (curr == '*')
                    inComment = true;

                if (curr == '/' || curr == '*') {
                    offset--;
                    break;
                }
            }

            prev = curr;
        }

        rawLine = rawLine.substring(0, offset);

        if (Strings.isBlank(rawLine))
            return;

        Matcher m;

        if ((m = p0arg.matcher(rawLine)).matches()) {
            result.instructions.add(MacroOpcode.valueOf(m.group(1)).newInstance());
        } else if ((m = p1arg.matcher(rawLine)).matches()) {
            MacroInstruction inst = MacroOpcode.valueOf(m.group(1)).newInstance();
            inst.setArgument(0, m.group(2));
            result.instructions.add(inst);
        } else if ((m = p2arg.matcher(rawLine)).matches()) {
            MacroInstruction inst = MacroOpcode.valueOf(m.group(1)).newInstance();
            inst.setArgument(0, m.group(2));
            inst.setArgument(1, m.group(3));
            result.instructions.add(inst);
        } else {
            boolean missingSc = false;

            if ((m = p0arg.matcher(rawLine + ";")).matches())
                missingSc = true;
            else if ((m = p1arg.matcher(rawLine + ";")).matches())
                missingSc = true;
            else if ((m = p2arg.matcher(rawLine + ";")).matches())
                missingSc = true;

            if (missingSc)
                throw new ParseException("Missing semicolon", lineNo);

            throw new ParseException("Found garbage (" + rawLine + ")", lineNo);
        }
    }
}
