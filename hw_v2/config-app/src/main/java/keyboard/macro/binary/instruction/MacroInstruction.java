package keyboard.macro.binary.instruction;

import keyboard.macro.MacroExecutionException;
import keyboard.macro.binary.BinaryMacroExecutionContext;

/**
 * Base class for all macro instructions
 */
public abstract class MacroInstruction {
    /**
     * Decodes the given argument
     * @param argument the argument
     */
    public abstract void decodeArgument(short argument);

    /**
     * Encodes the argument(s)
     * @return the encoded argument(s)
     */
    public abstract short encodeArgument();

    /**
     * Sets a specific argument
     * @param indx the argument's index
     * @param value the value
     */
    public abstract void setArgument(int indx, String value);

    /**
     * Executes the instruction in the given execution context
     * @param context the context to run in
     * @throws MacroExecutionException
     */
    public abstract void execute(BinaryMacroExecutionContext context) throws MacroExecutionException;

    /**
     * Resets the instruction
     */
    public abstract void reset();

    @Override
    public String toString() {
        return getClass().getSimpleName().replace("MacroInstruction", "");
    }
}
