package keyboard.macro.binary;

import keyboard.gui.ApplicationMain;
import keyboard.macro.binary.instruction.MacroInstruction;

import java.lang.reflect.InvocationTargetException;

/**
 * Opcode of a macro instruction
 */
public enum MacroOpcode {
    NOP(0, 0),
    HOLDD(1, 8),
    HOLDU(2, 8),
    PRESS(3, 8),
    DELAY(4, 8),
    REPN(5, 12),
    EXIT(15, 0);

    private byte     opcode;
    private int      argbits;
    private Class<?> owner;

    MacroOpcode(int opcode, int argbits) {
        this.opcode = (byte) opcode;
        this.argbits = argbits;

        try {
            this.owner = Class.forName(getClass().getPackageName() + ".instruction.MacroInstruction" + name());
        } catch (ClassNotFoundException e) {
            ApplicationMain.getLogger().error("Failed to get class for instruction <" + name() + ">", e);
        }
    }

    /**
     * Tells the opcode from an instruction's instance
     * @param inst the instruction
     * @return the opcode for the given instruction
     */
    public static MacroOpcode fromInstance(MacroInstruction inst) {
        return valueOf(inst.getClass().getName().split("MacroInstruction")[1]);
    }

    /**
     * Finds the enum value for the given numeric opcode
     * @param n the opcode
     * @return the enum value for the opcode or null
     */
    public static MacroOpcode fromNumber(int n) {
        for (MacroOpcode o : values())
            if (o.opcode == n)
                return o;

        return null;
    }

    /**
     * @return the names of the opcodes
     */
    public static String[] names() {
        String[] res = new String[values().length];
        int i = 0;
        for (MacroOpcode o : values())
            res[i++] = o.name();
        return res;
    }

    /**
     * @return the full size of the instruction in bits
     */
    public int getFullSize() {
        return argbits + 4;
    }

    /**
     * @return the size of the argument in bits
     */
    public int getArgsSize() {
        return argbits;
    }

    /**
     * @return the numeric opcode
     */
    public byte numeric() {
        return opcode;
    }

    /**
     * @return the class fro the given instruction
     */
    public Class<?> getInstructionClass() {
        return owner;
    }

    /**
     * @return a new instruction instance for this opcode
     */
    public MacroInstruction newInstance() {
        try {
            return (MacroInstruction) getInstructionClass().getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
            return null;
        }
    }
}
