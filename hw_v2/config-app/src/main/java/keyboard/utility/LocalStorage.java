package keyboard.utility;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.nio.file.Paths;

/**
 * Manages filesystem storage
 */
public class LocalStorage {
    private static final Logger logger = LogManager.getLogger(LocalStorage.class);

    @Getter private static String storageRoot;
    @Getter private static String macroRoot;

    /**
     * Initializes the storage manager and creates all necessary folders
     */
    public static void initialize() {
        logger.info("Initializing local storage...");

        String home = System.getProperty("user.home");
        if (home == null)
            home = System.getenv("HOME");

        if (home == null) {
            logger.error("Could not find home directory");
            home = Paths.get("").toAbsolutePath().toString();
            logger.error("Using working directory instead ({})", home);
        }

        storageRoot = Paths.get(home, ".ctcfg").toString();

        logger.info("Storage root: {}", storageRoot);

        new File(storageRoot).mkdirs();

        macroRoot = Paths.get(storageRoot, "macro").toString();

        new File(Paths.get(macroRoot, "binary").toString()).mkdirs();
        new File(Paths.get(macroRoot, "lua").toString()).mkdirs();
    }

    /**
     * Gives the path where a given binary macro slot's source code is stored
     * @param slot the slot id
     * @return the path of the file
     */
    public static String getBinaryMacroPath(int slot) {
        return Paths.get(macroRoot, "binary", String.format("SLOT%X", slot)).toString();
    }

    /**
     * Gives a file object for the path where a given binary macro slot's source code is stored
     * @param slot the slot id
     * @return the file
     */
    public static File getBinaryMacroFile(int slot) {
        return new File(getBinaryMacroPath(slot));
    }

    /**
     * @return the path for storing Lua macros
     */
    public static String getLuaMacroPath() {
        return Paths.get(macroRoot, "lua").toString();
    }
}
