package keyboard.utility;

import keyboard.device.MatrixIDConverter;
import keyboard.device.configuration.def.KeycodeConfigDef;
import keyboard.device.configuration.eeprom.EEPROMKeyBinding;
import keyboard.gui.ApplicationMain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Class for managing default key bindings
 */
public class DefaultsLoader {
    private static final Logger                        logger = LogManager.getLogger(DefaultsLoader.class);
    private static       Map<String, KeycodeConfigDef> keys   = new HashMap<>();

    /**
     * Reads the configuration file, and initializes the default binding database
     * @throws IOException
     */
    public static void read() throws IOException {
        keys.clear();
        InputStream inputStream = DefaultsLoader.class.getClassLoader().getResourceAsStream("DEFAULTS");
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        while ((line = reader.readLine()) != null) {
            line = line.trim();

            if (line.isEmpty())
                continue;

            String[] splitted = line.split("\\s*=\\s*");

            if (splitted.length != 2)
                throw new Error("Invalid default binding (line:'" + line + "')");

            KeycodeConfigDef def = ApplicationMain.getKeycodeManager().lookupKeycode(splitted[1]);

            if (def == null) {
                logger.warn("No keycode defined with name '{}'", splitted[1]);
                continue;
            }

            keys.put(splitted[0], def);
        }

        logger.info("Loaded default binding config ({} entries)", keys.size());
    }

    /**
     * Finds the default keycode definition for the given KDKS id
     * @param id the id
     * @return the optional key definition
     */
    public static Optional<KeycodeConfigDef> defaultOfKDKS(String id) {
        if (keys.containsKey(id))
            return Optional.of(keys.get(id));

        return Optional.empty();
    }

    /**
     * Restores the default keycode into an EEPROM key binding
     * @param bind the key binding
     * @return true if the default keycode was found and restored, false otherwise
     */
    public static boolean restore(EEPROMKeyBinding bind) {
        String id = MatrixIDConverter.ToKD(bind.getMuxId()) + "/" + MatrixIDConverter.ToKS(bind.getDmuxId());
        Optional<KeycodeConfigDef> key = defaultOfKDKS(id);

        if (key.isEmpty()) {
            logger.warn("No default binding found for {} (sid={})", id, String.format("%04x", bind.shortId()));
            return false;
        }

        bind.setKeycode((short) key.get().getCode());
        return true;
    }

    /**
     * Checks whether an EEPROM key binding is set to the default keycode
     * @param bind the binding
     * @return true if it's set to the default keycode or no default has been found, false otherwise
     */
    public static boolean isSetToDefault(EEPROMKeyBinding bind) {
        String id = MatrixIDConverter.ToKD(bind.getMuxId()) + "/" + MatrixIDConverter.ToKS(bind.getDmuxId());
        Optional<KeycodeConfigDef> key = defaultOfKDKS(id);

        if (key.isEmpty()) {
            logger.warn("No default binding found for {} (sid={})", id, String.format("%04x", bind.shortId()));
            return true;
        }

        return bind.getKeycode() == key.get().getCode();
    }
}
