package keyboard.utility;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.scene.paint.Paint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.nio.ByteBuffer;

/**
 * Class for managing RGB colors
 *
 * Also supports conversion from and to HSV.
 */
@NoArgsConstructor
public class RGBColor {
    public static final RGBColor WHITE = new RGBColor(0xff, 0xff, 0xff);
    public static final RGBColor BLACK = new RGBColor(0, 0, 0);
    @Getter
    @Setter
    private byte r;
    @Getter
    @Setter
    private byte g;
    @Getter
    @Setter
    private byte b;
    private int   cachedPaintHc = -1;
    private Paint cachedPaint;

    /**
     * Creates a color with the given RGB values
     * @param r the red value
     * @param g the green value
     * @param b the blue value
     */
    public RGBColor(byte r, byte g, byte b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    /**
     * Creates a color with the given RGB values
     * @param r the red value
     * @param g the green value
     * @param b the blue value
     */
    public RGBColor(int r, int g, int b) {
        this((byte) r, (byte) g, (byte) b);
    }

    /**
     * Creates a color from the given HSV values
     * @param h the hue
     * @param s the saturation
     * @param v the value
     */
    public static RGBColor fromHSV(byte h, byte s, byte v) {
        RGBColor res = new RGBColor();
        res.setHSV(h,s,v);
        return res;
    }

    /**
     * Writes the RGB values into the given byte buffer
     * @param buffer the buffer to put to
     */
    public void put(ByteBuffer buffer) {
        buffer.put(r);
        buffer.put(g);
        buffer.put(b);
    }

    /**
     * Reads the RGB values from the given byte buffer
     * @param buffer the buffer to get from
     */
    public void get(ByteBuffer buffer) {
        r = buffer.get();
        g = buffer.get();
        b = buffer.get();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (obj instanceof RGBColor) {
            RGBColor c = (RGBColor) obj;
            return c.r == r && c.g == g && c.b == b;
        }

        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return (Utils.byte2short(r) | (Utils.byte2int(g) << 8) | (Utils.byte2int(b) << 16)) & 0xFFFFFF;
    }

    /**
     * @return this color as a JavaFX paint
     */
    public Paint toPaint() {
        if (cachedPaintHc == hashCode())
            return cachedPaint;

        cachedPaint = Paint.valueOf(toString());
        cachedPaintHc = hashCode();
        return cachedPaint;
    }

    /**
     * @return the hue of the color
     */
    @JsonIgnore
    public byte getHSVHue() {
        double _r = Utils.byte2short(r) / 255.0;
        double _g = Utils.byte2short(g) / 255.0;
        double _b = Utils.byte2short(b) / 255.0;

        double maxRGB = Math.max(_r, Math.max(_g, _b));
        double minRGB = Math.min(_r, Math.min(_g, _b));

        if (minRGB == maxRGB)
            return 0;

        double delta = maxRGB - minRGB;
        double res = 0;

        if (maxRGB == _r)
            res = (((_g - _b)/delta) % 6);
        else if (maxRGB == _g)
            res = ((_b - _r)/delta) + 2;
        else if (maxRGB == _b)
            res = ((_r - _g)/delta) + 4;

        return (byte) Math.round((60 * res) / 360 * 255);
    }

    /**
     * @return the saturation of the color
     */
    @JsonIgnore
    public byte getHSVSaturation() {
        double _r = Utils.byte2short(r) / 255.0;
        double _g = Utils.byte2short(g) / 255.0;
        double _b = Utils.byte2short(b) / 255.0;

        double maxRGB = Math.max(_r, Math.max(_g, _b));
        double minRGB = Math.min(_r, Math.min(_g, _b));

        if (minRGB == maxRGB)
            return 0;

        return (byte) (Math.round((maxRGB - minRGB) / maxRGB * 255));
    }

    /**
     * @return the HSV value of the color
     */
    @JsonIgnore
    public byte getHSVValue() {
        return (byte) Math.max(Utils.byte2short(r), Math.max(Utils.byte2short(g), Utils.byte2short(b)));
    }

    /**
     * Creates a copy of this color with the specific brightness
     * @param br the brigntess
     * @return the new color object
     */
    public RGBColor ofBrightness(byte br) {
        double m = 1 + ((Utils.byte2short(br) - 255) / 255.0);
        return new RGBColor((int) Math.min(Utils.byte2int(r), Math.round(Utils.byte2int(r) * m)), (int) Math.min(Utils.byte2int(g), Math.round(Utils.byte2int(g) * m)), (int) Math.min(Utils.byte2int(b), Math.round(Utils.byte2int(b) * m)));
    }

    @Override
    public String toString() {
        return String.format("#%02x%02x%02x", r, g, b);
    }

    /**
     * Copy values from another color object
     * @param other the other object
     */
    public void copy(RGBColor other) {
        this.r = other.r;
        this.g = other.g;
        this.b = other.b;
    }

    /**
     * Copy values from another color object with the given brightness
     * @param other the other object
     * @param brightness the brightness
     */
    public void copy(RGBColor other, byte brightness) {
        double m = 1 + ((Utils.byte2short(brightness) - 255) / 255.0);
        this.r = (byte)Math.min(Utils.byte2int(other.r), Math.round(Utils.byte2int(other.r) * m));
        this.g = (byte)Math.min(Utils.byte2int(other.g), Math.round(Utils.byte2int(other.g) * m));
        this.b = (byte)Math.min(Utils.byte2int(other.b), Math.round(Utils.byte2int(other.b) * m));
    }

    /**
     * Set the HSV values of this color
     * @param _h the new hue
     * @param _s the new saturation
     * @param _v the new value
     */
    public void setHSV(byte _h, byte _s, byte _v) {
        int h = Utils.byte2short(_h);
        int s = Utils.byte2short(_s);
        int v = Utils.byte2short(_v);

        int r, g, b;
        double minV, maxV;
        int chroma;
        double tempH;

        r = g = b = 0;

        if (v != 0) {
            if (s == 0) {
                r = g = b = v;
            } else {
                maxV = v;
                chroma = (int) Math.round(s / 255.0 * maxV);
                minV = (maxV - chroma) % 256;

                if (h >= 170) {
                    tempH = (h - 170.0) / 43;
                    g = 0;
                    if (tempH < 1) {
                        b = (int) Math.floor(maxV);
                        r = (int) Math.round(maxV * tempH) % 256;
                    } else {
                        r = (int) Math.floor(maxV);
                        b = (int) Math.round(maxV * (2 - tempH)) % 256;
                    }
                } else if (h >= 85) {
                    tempH = (h - 85.0) / 43.0;
                    r = 0;
                    if (tempH < 1) {
                        g = (int) Math.floor(maxV);
                        b = (int) Math.round(maxV * tempH) % 256;
                    } else {
                        b = (int) Math.floor(maxV);
                        g = (int) Math.round(maxV * (2 - tempH)) % 256;
                    }
                } else {
                    tempH = h / 43.0;
                    b = 0;
                    if (tempH < 1) {
                        r = (int) Math.floor(maxV);
                        g = (int) Math.round(maxV * tempH) % 256;
                    } else {
                        g = (int) Math.floor(maxV);
                        r = (int) Math.round(maxV * (2 - tempH)) % 256;
                    }
                }

                r = (int) Math.floor((r / maxV * (maxV - minV) + minV)) % 256;
                g = (int) Math.floor((g / maxV * (maxV - minV) + minV)) % 256;
                b = (int) Math.floor((b / maxV * (maxV - minV) + minV)) % 256;
            }
        }

        this.r = (byte)r;
        this.g = (byte)g;
        this.b = (byte)b;
    }
}
