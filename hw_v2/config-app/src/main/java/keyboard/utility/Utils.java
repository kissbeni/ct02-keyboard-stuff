package keyboard.utility;

import java.io.File;

/**
 * Class containing utility functions
 */
public class Utils {
    public final static byte UBYTE_MAX = (byte) 0xFF;
    public final static byte UBYTE_MIN = (byte) 0x00;
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * Prints 4 spaces @p t times
     * @param t how many tabs you need?
     */
    public static void printTabSpaces(int t) {
        for (int i = 0; i < t; i++)
            System.out.print("    ");
    }

    /**
     * Prints a hexdump
     * @param bytes the bytes to dump
     */
    public static void hexdump(byte[] bytes) {
        for (int i = 0; i < 32; i++)
            System.out.format("%02x ", i);
        System.out.println();
        for (int i = 0; i < 32; i++)
            System.out.print("---");
        System.out.println("+");
        for (int j = 0; j < bytes.length; j++) {
            if ((j % 32) == 0 && j != 0) {
                System.out.print("| ");
                for (int i = j - 32; i < j; i++) {
                    int v = bytes[i] & 0xFF;

                    if (v >= 0x20 && v < 0x7F)
                        System.out.print((char) v);
                    else
                        System.out.print('.');
                }

                System.out.println(" |");
            }

            int v = bytes[j] & 0xFF;
            System.out.print(hexArray[v >>> 4] + "" + hexArray[v & 0x0F] + " ");
        }

        System.out.println();
    }

    /**
     * Finds the extension of a file
     * @param f the file
     * @return the file's extension
     */
    public static String getExtension(File f) {
        String[] splitted = f.getName().split("\\/");
        splitted = splitted[splitted.length - 1].split("\\.");

        if (splitted.length == 1)
            return "";

        return splitted[splitted.length - 1];
    }

    /**
     * A wrapper for sleep
     * @param ms the time to sleep
     * @return false if the sleep was interrupted, false otherwise
     */
    public static boolean sleep(int ms) {
        try {
            Thread.sleep(ms);
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    /**
     * Converts a byte to a short (unsigned)
     * @param b the byte to convert
     * @return the short containing the byte
     */
    public static short byte2short(byte b) {
        return (short) (((short) b) & 0xff);
    }

    /**
     * Converts a byte to an int (unsigned)
     * @param b the byte to convert
     * @return the int containing the byte
     */
    public static int byte2int(byte b) {
        return (((short) b) & 0xff);
    }

    /**
     * Calculates the distance between two points
     * @param x1 x of the first point
     * @param y1 y of the first point
     * @param x2 x of the second point
     * @param y2 y of the second point
     * @return the distance
     */
    public static double distance(double x1, double y1, double x2, double y2) {
        return Math.hypot(Math.abs(y2 - y1), Math.abs(x2 - x1));
    }

    /**
     * Calculates the BSD checksum for the given data
     *
     * @param data the data
     * @param offset the offset where to start
     * @param len the number of bytes to use from the start offset
     * @return the calculated checksum
     */
    public static short calculateBSDChecksum(final byte[] data, final int offset, final int len) {
        int checksum = 0;

        for (int i = offset; i < offset + len; i++) {
            checksum = (checksum >>> 1) + ((checksum & 1) << 15);
            checksum += byte2int(data[i]);
            checksum &= 0xffff;
        }

        return (short) checksum;
    }
}
