package keyboard.device;

/**
 * Constants from the firmware
 */
public class FirmwareConstants {
    /**
     * The size of the EEPROM used in the keyboard
     */
    public static final short KEYBD_EEPROM_SIZE    = 2048;

    /**
     * Number of hardware macro slots
     */
    public static final int NUM_HW_MACRO_SLOTS = 16;

    /**
     * The supported configuration version number
     */
    public static final int CONFIG_VERSION = 3;

    /**
     * The maximum number of layout profiles
     */
    public static final int MAX_PROFILES = 4;
}
