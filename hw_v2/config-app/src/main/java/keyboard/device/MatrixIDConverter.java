package keyboard.device;

/**
 * Class for converting between MUX/DMUX ids and KDKS ids
 */
public class MatrixIDConverter {
    /**
     * Converts the given KD id to MUX id
     * @param kd the KD id to convert
     * @return the MUX id
     */
    public static int FromKD(int kd) {
        switch (kd) {
            case 14:
                return 0;
            case 13:
                return 1;
            case 12:
                return 2;
            case 11:
                return 3;
            case 15:
                return 4;
            case 16:
                return 5;
            case 17:
                return 6;
            case 18:
                return 7;
            case 3:
                return 8;
            case 4:
                return 9;
            case 5:
                return 10;
            case 6:
                return 11;
            case 10:
                return 12;
            case 9:
                return 13;
            case 8:
                return 14;
            case 7:
                return 15;
            default:
                return -1;
        }
    }

    /**
     * Converts the given KS id to DMUX id
     * @param ks the KS id to convert
     * @return the DMUX id
     */
    public static int FromKS(int ks) {
        switch (ks) {
            case 28:
                return 0;
            case 27:
                return 1;
            case 26:
                return 2;
            case 25:
                return 3;
            case 24:
                return 4;
            case 23:
                return 5;
            case 22:
                return 6;
            case 21:
                return 7;
            default:
                return -1;
        }
    }

    /**
     * Converts the given MUX id to KD id
     * @param mux the MUX id to convert
     * @return the KD id
     */
    public static int ToKD(int mux) {
        switch (mux) {
            case 0:
                return 14;
            case 1:
                return 13;
            case 2:
                return 12;
            case 3:
                return 11;
            case 4:
                return 15;
            case 5:
                return 16;
            case 6:
                return 17;
            case 7:
                return 18;
            case 8:
                return 3;
            case 9:
                return 4;
            case 10:
                return 5;
            case 11:
                return 6;
            case 12:
                return 10;
            case 13:
                return 9;
            case 14:
                return 8;
            case 15:
                return 7;
            default:
                return -1;
        }
    }

    /**
     * Converts the given DMUX id to KS id
     * @param dmux the DMUX id to convert
     * @return the KS id
     */
    public static int ToKS(int dmux) {
        switch (dmux) {
            case 0:
                return 28;
            case 1:
                return 27;
            case 2:
                return 26;
            case 3:
                return 25;
            case 4:
                return 24;
            case 5:
                return 23;
            case 6:
                return 22;
            case 7:
                return 21;
            default:
                return -1;
        }
    }
}
