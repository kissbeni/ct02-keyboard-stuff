package keyboard.device;

import java.io.IOException;

/**
 * Interface for a readable and writeable byte storage
 */
public interface IRWStorage {
    /**
     * Writes a byte array to this storage starting at address 0
     * @param data the data to write
     * @throws IOException
     */
    void writeAll(byte[] data) throws IOException;

    /**
     * Reads all bytes from this storage
     * @return the read bytes
     * @throws IOException
     */
    byte[] readAll() throws IOException;
}
