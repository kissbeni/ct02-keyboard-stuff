package keyboard.device;

import keyboard.device.configuration.eeprom.EEPROMConfiguration;
import keyboard.device.configuration.rgb.RGBConfiguration;
import keyboard.device.usb.CTAPIDeviceWrapper;
import keyboard.device.usb.CTAPIWrapper;
import keyboard.device.usb.USBMainThread;
import keyboard.device.usb.command.*;
import keyboard.gui.ApplicationMain;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TimerTask;
import java.util.function.Consumer;

/**
 * Class for managing and communicating with the keyboard
 */
public class DeviceController {
    private static final Logger logger = LogManager.getLogger(DeviceController.class);
    private static final Random random = new Random();

    /**
     * The currently active configuration object
     */
    @Getter
    private static EEPROMConfiguration     config          = new EEPROMConfiguration();
    /**
     * The thread for managing USB communication
     */
    private static USBMainThread           usbmt;
    /**
     * Command for reading the version
     */
    private static CommandGetVersion       versionReader   = new CommandGetVersion();
    /**
     * Command for reading the whole EEPROM
     */
    private static CommandReadEEPROM       configReader    = new CommandReadEEPROM((short) 0, FirmwareConstants.KEYBD_EEPROM_SIZE);
    /**
     * Command for getting the last pressed key
     */
    private static CommandGetLastKey       lastKeyReader   = new CommandGetLastKey();
    /**
     * Command for syncing time
     */
    private static CommandSetCurrentTime   timeSyncCommand = new CommandSetCurrentTime();
    @Getter
    /**
     * The status of the connection
     */
    private static ConnectionStatus        status          = ConnectionStatus.OFFLINE;
    /**
     * Handler for processing status changes
     */
    private static Consumer<ConnectionStatus> statusChangeHandler;
    /**
     * Indicates that we should load default settings when set to true
     */
    private static boolean                 loadDefaults    = false;
    /**
     * Indicates that the configuration has been loaded when set to true
     */
    private static boolean                 configLoaded    = false;
    /**
     * Used for debugging and testing, disables all of the USB communication process
     */
    private static boolean                 disableUsb      = false;
    /**
     * File name for the dummy EEPROM used when USB is diabled
     */
    private static String                  configFile      = "testconfig.bin";

    /**
     * List of callbacks to call when the configuration changes
     */
    private static List<Runnable> configurationLoadedCallbacks = new ArrayList<Runnable>();

    /**
     * Sends an RGB preview command to the keyboard
     * @param build the configuration to preview
     */
    public static void previewRGB(RGBConfiguration build) {
        if (disableUsb) return;

        usbmt.queueRequest(new CommandPreviewRGB(build));
    }

    /**
     * Sets the currently active profile on the keyboard
     * @param profile the profile id to select
     */
    public static void setCurrentProfile(int profile) {
        if (disableUsb) return;

        usbmt.queueRequest(new CommandSetProfile(profile));
    }

    /**
     * Reboots the device
     */
    public static void resetDevice() {
        if (disableUsb) return;

        usbmt.queueRequest(new USBDeviceCommand() {
            @Override
            public void completeTask(CTAPIDeviceWrapper dev) {
                dev.ctapi_hard_reset();
                done();
            }

            @Override
            public boolean isBackgroundTask() {
                return false;
            }
        });
    }

    /**
     * Task for syncing the current time periodically
     */
    private static final class TimeUpdaterTask extends TimerTask {
        @Override
        public void run() {
            usbmt.queueRequest(timeSyncCommand);
        }
    }

    private static TimeUpdaterTask timeUpdaterTask = new TimeUpdaterTask();

    /**
     * Initializes the device controller
     * @param args the application's command line arguments
     */
    public static void initialize(String[] args) {
        logger.info("Starting device controller");

        for (String s : args) {
            if (s.equals("--offline-config"))
                disableUsb = true;
            else if (s.startsWith("--local-config=")) {
                configFile = s.split("=")[1];
            }
        }

        if (disableUsb) {
            loadConfig();
            setStatus(ConnectionStatus.VALID);
        } else {
            CTAPIWrapper.init();

            versionReader.setCallback(() -> {
                int result = versionReader.getResult();

                logger.debug("Device responded version: {}", result);

                if (result == FirmwareConstants.CONFIG_VERSION) {
                    setStatus(ConnectionStatus.VALID);
                } else {
                    setStatus(ConnectionStatus.INVALID);
                }
            });

            configReader.setCallback(() -> {
                if (status.isValid()) {
                    loadConfig();
                    timeUpdaterTask.cancel();
                    timeUpdaterTask = new TimeUpdaterTask();
                    ApplicationMain.getGlobalTimer().scheduleAtFixedRate(timeUpdaterTask, 0, 30000);
                }
            });

            usbmt = new USBMainThread();
            usbmt.setDeviceChangedCallback(() -> {
                logger.debug("Received device change event");

                if (usbmt.deviceAvailable()) {
                    setStatus(ConnectionStatus.AVAILABLE);
                    logger.debug("Got new device, requesting version");
                    usbmt.queueRequest(versionReader);
                    usbmt.queueRequest(timeSyncCommand);
                    usbmt.queueRequest(configReader);
                } else {
                    timeUpdaterTask.cancel();
                    setStatus(ConnectionStatus.OFFLINE);
                }
            });
            usbmt.setForegroundOperationStartedCallback(() -> {
                if (status == ConnectionStatus.DEVICE_OPEN)
                    setStatus(ConnectionStatus.OPERATION_IN_PROGRESS);
            });
            usbmt.setDeviceOpenCallback(() -> {
                if (status == ConnectionStatus.VALID)
                    setStatus(ConnectionStatus.DEVICE_OPEN);
            });
            usbmt.setDeviceCloseCallback(() -> {
                if (status.isOpen())
                    setStatus(ConnectionStatus.VALID);
            });
            usbmt.start();
        }
    }

    /**
     * Finishes all commands
     */
    public static void finish() {
        if (!disableUsb) {
            usbmt.queueRequest(new CommandReloadConfig());
            usbmt.abort();
        }
    }

    /**
     * Updates the connection status and fires the change handler
     * @param st the new status
     */
    private static void setStatus(ConnectionStatus st) {
        if (st == status)
            return;

        logger.debug("Connection status changing {} -> {}", status, st);

        status = st;

        if (!status.isValid())
            configLoaded = false;

        if (statusChangeHandler != null)
            statusChangeHandler.accept(status);
    }

    /**
     * Sets a new status changed handler
     * @param handler the new handler
     */
    public static void setStatusChangeHandler(Consumer<ConnectionStatus> handler) {
        statusChangeHandler = handler;

        if (statusChangeHandler != null && status != ConnectionStatus.OFFLINE)
            statusChangeHandler.accept(status);
    }

    /**
     * Loads the configuration
     */
    public static void loadConfig() {
        logger.info("Loading configuration");

        if (configReader.isReady()) {
            try {
                config.read(configReader);
                logger.info("Loaded configuration from EEPROM!");

                configLoaded = true;
                runConfigLoadedCallbacks();
                return;
            } catch (Exception ex) {
                logger.error("Could not load config, using defults");
                ex.printStackTrace();
                loadDefaults = true;
            }

            return;
        }

        try {
            DummyConfigEEPROM file = new DummyConfigEEPROM(configFile);

            if (!new File(configFile).exists())
                config.write(file);

            config.read(file);
            logger.info("Loaded configuration from local file!");

            configLoaded = true;
            runConfigLoadedCallbacks();
        } catch (Exception ex) {
            logger.error("Could not load config, using defaults");
            ex.printStackTrace();
            loadDefaults = true;
        }

        runConfigLoadedCallbacks();
    }

    /**
     * Saves the configuration
     */
    public static void saveConfig() {
        ApplicationMain.getGlobalTimer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    if (disableUsb) {
                        config.write(new DummyConfigEEPROM(configFile));
                    } else {
                        CommandWriteEEPROM wr = new CommandWriteEEPROM((short) 0);
                        config.write(wr);
                        usbmt.queueRequest(wr);
                        usbmt.queueRequest(new CommandReloadConfig());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 10);
    }

    /**
     * @return whether default configuration should be used
     */
    public static boolean shouldForceDefaults() {
        return loadDefaults;
    }

    /**
     * Notify that the defaults has been loaded
     */
    public static void defaultsLoaded() {
        loadDefaults = false;
    }

    /**
     * Adds a configuration loaded handler
     * @param r the handler to add
     */
    public static void onConfigurationLoaded(Runnable r) {
        if (r == null)
            return;

        if (!configurationLoadedCallbacks.contains(r))
            configurationLoadedCallbacks.add(r);

        if (configLoaded)
            r.run();
    }

    /**
     * Requests the last pressed key
     * @param callback the callback when the command finished
     */
    public static void getLastPressedKey(Consumer<String> callback) {
        if (disableUsb) {
            callback.accept((random.nextInt(17) + 3) + "/" + (random.nextInt(8) + 21));
        } else {
            lastKeyReader.setCallback(() -> callback.accept(lastKeyReader.getResultKDKS()));
            usbmt.queueRequest(lastKeyReader);
        }
    }

    /**
     * Notifies all handlers that the configuration is updated
     */
    public static void runConfigLoadedCallbacks() {
        if (!configLoaded)
            return;

        for (Runnable r : configurationLoadedCallbacks)
            r.run();
    }
}
