package keyboard.device.configuration;

/**
 * Represents a key size
 */
public enum KeySize {
    Single(40, 40, 3),
    Medium(57.5, 40, 4),
    Large(75, 40, 6),
    Large_vertical(40, 81, 3),
    Long(82, 40, 6),
    Enter(57.5, 82, 4),
    Spacebar(313, 40, 20);

    private double h, w;
    private int maxchars;

    KeySize(double w, double h, int maxchars) {
        this.w = w;
        this.h = h;
        this.maxchars = maxchars;
    }

    /**
     * @return the height of the key in pixels
     */
    public final double height() {
        return h;
    }

    /**
     * @return the width of the key in pixels
     */
    public final double width() {
        return w;
    }

    /**
     * @return the maximum number of characters, before the font has to be smaller
     */
    public final int maxChars() {
        return maxchars;
    }
}
