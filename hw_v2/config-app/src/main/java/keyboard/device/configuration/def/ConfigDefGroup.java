package keyboard.device.configuration.def;

import keyboard.utility.Utils;
import lombok.Getter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a group of definitions. This class can be and is used for XML configuration parsing.
 * It finds classes which implement IConfigDef for XML nodes in the keyboard.device.configuration.def package,
 * said classes must include a static field called "_tag" that contains the XML tag to mach.
 *
 * @see IConfigDef
 * */
public class ConfigDefGroup implements IConfigDef, INeedsGroupPath, Iterable<IConfigDef> {
    public static final String _tag = "group";

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Array of the contained nodes
     * */
    private List<IConfigDef> defs = new ArrayList<IConfigDef>();

    /**
     * Name of this group
     * */
    @Getter
    private String           name;

    /**
     * Path of this group (styled similar to java's package naming)
     * */
    private String           path = "";

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new group with no name
     * */
    public ConfigDefGroup() {
        name = null;
    }

    /**
     * Constructs a new group with the given name
     *
     * @param name the name of the group
     * */
    public ConfigDefGroup(String name) {
        this.name = name;
    }

    /**
     * Tells whether this group has a name or not.
     * */
    public boolean hasName() {
        return name != null;
    }

    /**
     * Gives the child nodes of this group.
     *
     * @return an unmodifiable list of nodes
     * */
    public List<IConfigDef> getDefs() {
        return Collections.unmodifiableList(defs);
    }

    /**
     * Parses a node then adds it to the child node list.
     *
     * @param e the XML element to parse
     * */
    public void add(Element e) {
        IConfigDef def = defFromTagName(e.getNodeName());

        if (def == null)
            System.err.println("Could not find ConfigDef for tag: " + e.getNodeName());

        if (def instanceof INeedsGroupPath)
            ((INeedsGroupPath) def).setGroupPath(path);

        def.parseXMLElement(e);
        defs.add(def);
    }

    /**
     * Adds another group to the child nodes
     *
     * @param group the group to add
     * */
    public void addChildGroup(ConfigDefGroup group) {
        defs.add(group);
    }

    /**
     * Finds a suitable IConfigDef class for the given XML tag name using reflection.
     *
     * @param tag the XML tag
     * @return a new instance of an IConfigDef or null in case there wasn't any matches
     * */
    private IConfigDef defFromTagName(String tag) {
        ClassLoader cld = Thread.currentThread().getContextClassLoader();
        String path = "keyboard/device/configuration/def";
        URL resource = cld.getResource(path);

        File directory = new File(resource.getFile());
        String[] files = directory.list();
        for (int i = 0; i < files.length; i++)
            if (files[i].endsWith(".class"))
                try {
                    Class<?> c = Class.forName("keyboard.device.configuration.def." + files[i].substring(0, files[i].length() - 6));

                    if (IConfigDef.class.isAssignableFrom(c) && !IConfigDef.class.equals(c)) {
                        try {
                            String _tag = (String) c.getField("_tag").get(null);
                            if (_tag.equalsIgnoreCase(tag))
                                return (IConfigDef) c.getConstructor().newInstance();
                        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException | InstantiationException | InvocationTargetException
                                | NoSuchMethodException e) {
                            System.err.println("Error while searching for ConfigDef classes, happend at: " + c.getName());
                            e.printStackTrace();
                        }
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
        return null;
    }

    /**
     * Prints the group recursively using every non-group nodes' toString() function.
     * */
    public void print() {
        print(0);
    }

    /**
     * Prints the group recursively using every non-group nodes' toString() function with every line indented.
     *
     * @param tab the amount to indent
     * */
    private void print(int tab) {
        Utils.printTabSpaces(tab);

        if (name == null)
            System.out.println("group {");
        else
            System.out.println("group '" + name + "' {");

        for (IConfigDef d : defs) {
            if (d instanceof ConfigDefGroup) {
                ((ConfigDefGroup) d).print(tab + 1);
            } else {
                Utils.printTabSpaces(tab + 1);
                System.out.println(d.toString());
            }
        }

        Utils.printTabSpaces(tab);
        System.out.println("}");
    }

    @Override
    public void parseXMLElement(Element e) {
        if (e.hasAttribute("name")) {
            name = e.getAttribute("name");
            path += "/" + name;
        }

        NodeList nodes = e.getChildNodes();

        if (nodes.getLength() <= 0)
            return;

        for (int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);

            if (n.getNodeType() == Node.ELEMENT_NODE)
                add((Element) n);
        }
    }

    @Override
    public Iterator<IConfigDef> iterator() {
        return defs.iterator();
    }

    @Override
    public void setGroupPath(String groupPath) {
        path = groupPath;
    }
}
