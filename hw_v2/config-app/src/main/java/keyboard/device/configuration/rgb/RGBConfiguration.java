package keyboard.device.configuration.rgb;

import keyboard.device.configuration.eeprom.EEPROMConfigurationPartBase;
import keyboard.device.usb.CTAPIRGBConfig;
import keyboard.utility.RGBColor;
import lombok.Getter;

/**
 * Configuration for the RGB animation
 */
public class RGBConfiguration extends EEPROMConfigurationPartBase {
    /**
     * The set brightness
     */
    @Getter private byte             brightness    = 0;
    /**
     * The type of the animation
     */
    @Getter private RGBAnimationType animationType = RGBAnimationType.STATIC;
    /**
     * The color used by the animation
     */
    @Getter private RGBColor         color         = new RGBColor();
    /**
     * The speed of the animation
     */
    @Getter private byte             speed         = 100;

    /**
     * Sets the brightness
     * @param b the new brightness
     */
    public void setBrightness(byte b) {
        if (b != brightness) {
            brightness = b;
            changed();
        }
    }

    /**
     * Sets the animation type
     * @param t the new animation type
     */
    public void setAnimationType(RGBAnimationType t) {
        if (t != animationType) {
            animationType = t;
            changed();
        }
    }

    /**
     * Sets the color using r g and b values
     * @param r the red component
     * @param g the green component
     * @param b the blue component
     */
    public void setColor(byte r, byte g, byte b) {
        if (color.getR() != r || color.getG() != g || color.getB() != b) {
            color = new RGBColor(r, g, b);
            changed();
        }
    }

    /**
     * Sets the color
     * @param c the new color
     */
    public void setColor(RGBColor c) {
        if (!c.equals(color)) {
            color = c;
            changed();
        }
    }

    /**
     * Sets the speed
     * @param b the new speed
     */
    public void setSpeed(byte b) {
        if (b != speed) {
            speed = b;
            changed();
        }
    }

    /**
     * Converts this class into a class used by the native component
     * @return the converted config
     */
    public CTAPIRGBConfig convert() {
        CTAPIRGBConfig c = new CTAPIRGBConfig();
        c.anim_type = animationType.getId();
        c.brightness = brightness;
        c.speed = speed;
        c.r = color.getR();
        c.g = color.getG();
        c.b = color.getB();
        return c;
    }

    @Override
    public String toString() {
        return String.format("[type=%02x,brightness=%02x,speed=%02x,color=%s]",
                animationType.getId(),
                brightness,
                speed,
                color.toString());
    }
}
