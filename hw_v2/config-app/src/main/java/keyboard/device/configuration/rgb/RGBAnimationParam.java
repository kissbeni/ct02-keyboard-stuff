package keyboard.device.configuration.rgb;

/**
 * Parameter types for RGB animations
 */
public enum RGBAnimationParam {
    COLOR, SPEED, BRIGHTNESS
}
