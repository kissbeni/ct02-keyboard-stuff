package keyboard.device.configuration.def;

import org.w3c.dom.Element;

/**
 * Common interface for XML configuration definitions
 */
public interface IConfigDef {

    /**
     * Loads configuration for the given XML node
     *
     * @param e the XML node to process
     */
    void parseXMLElement(Element e);
}
