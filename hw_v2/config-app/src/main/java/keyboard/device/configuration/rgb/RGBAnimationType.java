package keyboard.device.configuration.rgb;

import lombok.Getter;

import java.lang.reflect.InvocationTargetException;

/**
 * Type of an RGB animation
 */
public enum RGBAnimationType {
    OFF(0),
    STATIC(1, RGBAnimationParam.COLOR, RGBAnimationParam.BRIGHTNESS),
    BREATHING(2, RGBAnimationParam.COLOR, RGBAnimationParam.SPEED),
    FLASHING(3, RGBAnimationParam.COLOR, RGBAnimationParam.BRIGHTNESS, RGBAnimationParam.SPEED),
    WHEEL_FLASHING(4, RGBAnimationParam.SPEED, RGBAnimationParam.BRIGHTNESS),
    WHEEL_BREATHING(5, RGBAnimationParam.SPEED, RGBAnimationParam.BRIGHTNESS),
    WHEEL(6, RGBAnimationParam.SPEED);

    /**
     * The ID of this animation type
     */
    @Getter
    private byte                id;

    /**
     * Parameters of this animation type
     */
    @Getter
    private RGBAnimationParam[] params;

    RGBAnimationType(int id, RGBAnimationParam... params) {
        this.id = (byte) id;
        this.params = params;
    }

    /**
     * Translates an animation id to an animation type
     * @param id the id to search for
     * @return the animation type for the id
     */
    public static RGBAnimationType fromId(byte id) {
        for (RGBAnimationType t : values())
            if (t.getId() == id)
                return t;

        return null;
    }
}
