package keyboard.device.configuration.def;

import lombok.Getter;
import org.w3c.dom.Element;

/**
 * Describes a keyboard led code (USB HID stuff)
 */
public class LedConfigDef implements IConfigDef {
    public static final String _tag = "led";

    /**
     * The name of this led code
     */
    @Getter
    private String name;

    /**
     * The code of this led
     */
    @Getter
    private long   code;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void parseXMLElement(Element e) {
        name = e.getAttribute("name");
        code = Long.decode(e.getAttribute("code"));
    }

    @Override
    public String toString() {
        return "LedDef[name='" + name + "',code=" + code + "]";
    }
}
