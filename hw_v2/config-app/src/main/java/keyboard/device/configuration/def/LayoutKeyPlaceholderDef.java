package keyboard.device.configuration.def;

import keyboard.device.configuration.KeySize;
import lombok.Getter;
import org.w3c.dom.Element;

/**
 * Describes a placeholder key, only used for rendering the keyboard
 *
 * @see LayoutKeyDef
 * @see LayoutKeySlotDef
 */
public class LayoutKeyPlaceholderDef implements IConfigDef {
    public static final String _tag = "key-placeholder";

    /**
     * The size of this key
     *
     * @see KeySize
     */
    @Getter
    protected KeySize size = KeySize.Single;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void parseXMLElement(Element e) {
        if (e.hasAttribute("size"))
            size = KeySize.valueOf(e.getAttribute("size"));
    }

    @Override
    public String toString() {
        return "LayoutKeyPlaceholderDef[size='" + size.name() + "]";
    }
}
