package keyboard.device.configuration.eeprom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import keyboard.device.FirmwareConstants;
import keyboard.device.IRWStorage;
import keyboard.device.configuration.rgb.RGBAnimationType;
import keyboard.device.configuration.rgb.RGBConfiguration;
import keyboard.gui.ApplicationMain;
import keyboard.macro.binary.BinaryMacro;
import keyboard.macro.binary.BinaryMacroAssembler;
import keyboard.utility.FNVHash;
import keyboard.utility.LocalStorage;
import keyboard.utility.RGBColor;
import keyboard.utility.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.FileSystem;
import java.nio.file.Files;

/**
 * Class for managing the configuration stored in the keyboard's EEPROM
 *
 * The numbers are encoded Little endian because the AVR8 architecture uses that
 * and it makes reading the configuration on the firmware side easier and faster.
 */
@JsonIgnoreProperties(value = { "y" })
public class EEPROMConfiguration implements IEEPROMConfigurationChangedHandler {
    /**
     * The number expected as the first 2 bytes in the configuration
     */
    private static final short                   HEADER_MAGIC   = (short) 0x02C7;

    /**
     * Version number of the header
     */
    private static final byte                    HEADER_VERSION = 0x03;

    /**
     * The number indicating the begining of the RGB configuration
     */
    private static final int                     RGB_MAGIC      = 0x42475221;

    /**
     * The size of the RGB configuration
     *
     * (TODO: maybe move elsewhere?)
     */
    private static final int                     RGB_DATA_SIZE  = 12; // uint32, uint8, uint8, uint8, uint8[3], uint16

    /**
     * The size of the macro header
     */
    private static final int                    MACRO_HEADER_SIZE = FirmwareConstants.NUM_HW_MACRO_SLOTS * 2 + 1; // NUM_HW_MACRO_SLOTS * sizeof(uint16) + sizeof('M')

    /**
     * The RGB part of the configuration
     */
    public               RGBConfiguration        rgb;

    /**
     * The currently selected profile
     */
    public               byte                    currentProfile = 0;

    /**
     * The default profile
     */
    public               byte                    defaultProfile = 0;
    
    /**
     * A list of all the keyboard profiles
     */
    public               EEPROMKeyboardProfile[] profiles       = new EEPROMKeyboardProfile[FirmwareConstants.MAX_PROFILES];
    
    /**
     * Storing whether the configuration has been changed since it was last loaded/written
     */
    @JsonIgnore
    private              boolean                 isChanged      = false;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Initializes every part of the configuration by setting default values
     */
    public EEPROMConfiguration() {
        for (int i = 0; i < FirmwareConstants.MAX_PROFILES; i++)
            profiles[i] = new EEPROMKeyboardProfile();

        rgb = new RGBConfiguration();
        rgb.setAnimationType(RGBAnimationType.WHEEL);
        rgb.setColor(new RGBColor(0x71, 0, 0x50));
    }

    /**
     * @return the object for the current profile
     */
    @JsonIgnore
    public EEPROMKeyboardProfile getCurrentProfile() {
        return profiles[currentProfile];
    }

    /**
     * Serializes this configuration into it's binary form
     *
     * @return the serialized data
     */
    public byte[] generate() {
        ByteBuffer buffer = ByteBuffer.allocate(2048);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        buffer.putShort(HEADER_MAGIC);
        buffer.put(HEADER_VERSION);

        int nprofiles = FirmwareConstants.MAX_PROFILES;
        boolean anyProfileEnabled = false;

        byte flags = 0;

        int beginAddress = buffer.position() + RGB_DATA_SIZE + MACRO_HEADER_SIZE + (4 * 4) + 1;

        for (int i = 0; i < FirmwareConstants.MAX_PROFILES; i++) {
            if (profiles[i].isEnabled) {
                flags |= (1 << (4 + i));
                anyProfileEnabled = true;
            }

            profiles[i].dataAddress = (short) beginAddress;
            beginAddress += profiles[i].getSize();
            profiles[i].nameAddress = (short) beginAddress;
            beginAddress += profiles[i].name.length() + 1;
        }

        // if none of the profiles enabled, enable one of the default ones
        if (!anyProfileEnabled)
            for (int i = 0; i < FirmwareConstants.MAX_PROFILES; i++)
                if (profiles[i].isDefaultsOnly()) {
                    anyProfileEnabled = true;
                    flags |= (1 << (4 + i));
                    break;
                }

        buffer.put((byte) (flags | nprofiles));

        for (int i = 0; i < nprofiles; i++) {
            buffer.putShort(profiles[i].dataAddress);
            buffer.putShort(profiles[i].nameAddress);
        }

        generateRGB(buffer);

        buffer.put((byte)'M');

        int macroListOffset = buffer.position();
        for (int i = 0; i < MACRO_HEADER_SIZE - 1; i++)
            buffer.put((byte)0);

        for (int i = 0; i < FirmwareConstants.MAX_PROFILES; i++) {
            buffer.put(profiles[i].getData());

            for (int j = 0; j < profiles[i].name.length(); j++)
                buffer.put((byte) profiles[i].name.charAt(j));

            buffer.put((byte) 0);
        }

        generateMacroData(buffer, macroListOffset);

        byte[] dst = new byte[buffer.position()];

        System.arraycopy(buffer.array(), 0, dst, 0, buffer.position());

        return dst;
    }

    /**
     * Encodes all macros
     *
     * @param buffer the buffer to write to
     * @param macroListOffset the offset of the addresses
     */
    private void generateMacroData(ByteBuffer buffer, int macroListOffset) {
        BinaryMacroAssembler assembler = new BinaryMacroAssembler();

        for (int i = 0; i < FirmwareConstants.NUM_HW_MACRO_SLOTS; i++) {
            int dataOffset = buffer.position();
            buffer.position(macroListOffset + (i * 2));

            try {
                File f = LocalStorage.getBinaryMacroFile(i);

                if (!f.exists()) {
                    buffer.putShort((short)0);
                    buffer.position(dataOffset);
                    continue;
                }

                FileReader fr = new FileReader(f);

                assembler.reset();
                assembler.parse(fr);
                buffer.putShort((short)dataOffset);
                buffer.position(dataOffset);
                assembler.getResult().encode(buffer);

                fr.close();
            } catch (Exception ex) {
                buffer.putShort((short)0);
                ApplicationMain.getLogger().error("Failed to encode macro data:", ex);
            }
        }
    }

    /**
     * Serializes the RGB configuration
     *
     * @param buffer the target buffer
     */
    private void generateRGB(ByteBuffer buffer) {
        buffer.putInt(RGB_MAGIC);

        int dataBegin = buffer.position();

        buffer.put(rgb.getAnimationType().getId());
        buffer.put(rgb.getBrightness());
        buffer.put(rgb.getSpeed());
        rgb.getColor().put(buffer);

        buffer.putShort(Utils.calculateBSDChecksum(buffer.array(), dataBegin, buffer.position() - dataBegin));
    }

    /**
     * Deserializes a configuration from it's binary form
     * @param data the data to decode
     */
    public void decode(byte[] data) {
        ByteBuffer buffer = ByteBuffer.wrap(data);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        if (buffer.getShort() != HEADER_MAGIC)
            throw new InternalError("Invalid data header");

        byte version = buffer.get();
        if (version != HEADER_VERSION)
            throw new InternalError("Unsupported header version");

        byte flags = buffer.get();
        for (int i = 0; i < (flags & 0xf); i++) {
            profiles[i].isEnabled = (flags & (1 << (4 + i))) != 0;
            profiles[i].dataAddress = buffer.getShort();
            profiles[i].nameAddress = buffer.getShort();
        }

        for (int i = (flags & 0xf); i < FirmwareConstants.MAX_PROFILES; i++)
            profiles[i].restoreDefaults();

        decodeRGB(buffer);

        short[] addresses = new short[FirmwareConstants.NUM_HW_MACRO_SLOTS];
        if (buffer.get() != 'M')
            throw new InternalError("Invalid macro address header");

        for (int i = 0; i < FirmwareConstants.NUM_HW_MACRO_SLOTS; i++)
            addresses[i] = buffer.getShort();

        for (int i = 0; i < (flags & 0xf); i++) {
            buffer.position(profiles[i].dataAddress);
            profiles[i].loadKeyBindings(buffer);
            buffer.position(profiles[i].nameAddress);

            String name = "";
            while (true) {
                byte r = buffer.get();
                if (r == 0)
                    break;

                name += (char) r;
            }

            profiles[i].name = name;
        }

        decodeMacros(buffer, addresses);

        if (data.length == FirmwareConstants.KEYBD_EEPROM_SIZE) {
            defaultProfile = data[data.length - 1];
            currentProfile = data[data.length - 1];
        } else {
            defaultProfile = 0;
            currentProfile = 0;
        }
    }

    /**
     * Decodes the RGB part of the configuration
     * @param buffer the source buffer
     */
    private void decodeRGB(ByteBuffer buffer) {
        int magic = buffer.getInt();

        if (magic != RGB_MAGIC)
            throw new InternalError(String.format("Invalid RGB header 0x%08x!=0x%08x position=0x%04x", magic, RGB_MAGIC, buffer.position() - 4));

        byte[] rgbData = new byte[RGB_DATA_SIZE - 2 - 4];
        buffer.get(rgbData); // Read the RGB data, except the hash and the magic

        ByteBuffer buffer2 = ByteBuffer.wrap(rgbData);

        rgb.setAnimationType(RGBAnimationType.fromId(buffer2.get()));
        rgb.setBrightness(buffer2.get());
        rgb.setSpeed(buffer2.get());
        rgb.getColor().get(buffer2);

        short calculated = Utils.calculateBSDChecksum(rgbData, 0, rgbData.length);
        short read = buffer.getShort();
        if (calculated != read)
            throw new InternalError(String.format("Invalid RGB header checksum (0x%04x != 0x%04x)", calculated, read));
    }

    /**
     * Decodes macro information from the configuration
     * @param buffer the source buffer
     * @param addresses the address list for macros
     */
    private void decodeMacros(ByteBuffer buffer, short[] addresses) {
        BinaryMacroAssembler assembler = new BinaryMacroAssembler();
        for (int i = 0; i < FirmwareConstants.NUM_HW_MACRO_SLOTS; i++) {
            if (addresses[i] == 0)
                continue;

            buffer.position(addresses[i]);

            BinaryMacro macro = new BinaryMacro();
            macro.decode(buffer);

            try {
                File f = LocalStorage.getBinaryMacroFile(i);
                FileReader fr = new FileReader(f);

                assembler.reset();
                assembler.parse(fr);

                fr.close();

                if (assembler.getResult().hashCode() != macro.hashCode()) {
                    ApplicationMain.getLogger().info("Got a different source code from the keyboard than in local storage, overwriting the local version");
                    Files.writeString(f.toPath(), macro.toSourceCode());
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    /**
     * Reads the configuration from a readable binary storage
     * @param store the storage to read
     * @throws IOException
     */
    public void read(IRWStorage store) throws IOException {
        decode(store.readAll());
        isChanged = false;
    }

    /**
     * Writes the configuration to a writeable binary storage
     * @param store the storage to write to
     * @throws IOException
     */
    public void write(IRWStorage store) throws IOException {
        isChanged = true;
        if (isChanged) {
            store.writeAll(generate());
            isChanged = false;
        } else {
            System.out.println("Called write, but the configuration is unchanged!");
        }
    }

    @Override
    public void onConfigurationChanged() {
        isChanged = true;
    }

    /**
     * Loads the configuration from an other instance
     * @param config the other instance
     */
    public void replace(EEPROMConfiguration config) {
        rgb = config.rgb;
        currentProfile = config.currentProfile;

        for (int i = 0; i < 4; i++) {
            if (i < config.profiles.length) {
                profiles[i].replace(config.profiles[i]);
                continue;
            }

            profiles[i].restoreDefaults();
        }
    }
}
