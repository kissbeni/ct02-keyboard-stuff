package keyboard.device.configuration.def;

import lombok.Getter;
import org.w3c.dom.Element;

/**
 * Class for parsed keycode key definitions
 *
 * XML format:
 * &lt;key name="name" displayname="displayName" code="0x69"/&gt;
 */
public class KeycodeConfigDef implements IConfigDef, INeedsGroupPath {
    public static final String _tag = "key";

    /**
     * Name of the key (like 'Keypad Divide')
     */
    @Getter
    private String name;

    /**
     * The display name of the key (like '/')
     */
    private String displayName;

    /**
     * The group path of this key (like '/Keypad Common')
     * */
    private String path = "";

    /**
     * The keycode of the key
     * */
    @Getter
    private long   code;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gives the full group path of the key (like '/Keypad Common/Keypad Divide')
     *
     * @return the full path of the key
     */
    public String getFullName() {
        return path + "/" + name;
    }

    /**
     * @return the display name of the key, if it's not present, the name
     * */
    public String getDisplayName() {
        if (displayName != null)
            return displayName;

        return name;
    }

    @Override
    public void parseXMLElement(Element e) {
        name = e.getAttribute("name");
        code = Long.decode(e.getAttribute("code"));

        if (e.hasAttribute("displayname"))
            displayName = e.getAttribute("displayname");
    }

    @Override
    public String toString() {
        return "KeyDef[name='" + name + "',code=" + code + "]";
    }

    @Override
    public void setGroupPath(String groupPath) {
        path = groupPath;
    }
}
