package keyboard.device.configuration.def;

/**
 * An interface to indicate that a configuration definition requires the knowledge
 * of the parent group's path.
 */
public interface INeedsGroupPath {

    /**
     * Sets the group path
     *
     * @param groupPath the new group path
     * @see ConfigDefGroup
     */
    void setGroupPath(String groupPath);
}
