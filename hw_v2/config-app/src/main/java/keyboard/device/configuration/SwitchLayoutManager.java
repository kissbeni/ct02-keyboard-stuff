package keyboard.device.configuration;

import keyboard.device.configuration.def.ConfigDefGroup;
import keyboard.device.configuration.def.IConfigDef;
import keyboard.device.configuration.def.LayoutKeyDef;
import keyboard.device.configuration.def.LayoutKeySlotDef;
import lombok.Getter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * Key switch layout database
 */
public class SwitchLayoutManager {
    /**
     * The root group for storing the key switch configuration
     */
    @Getter private ConfigDefGroup rootGroup;

    /**
     * A list for unavailable key switches
     */
    private Set<String> unavailable = new HashSet<String>();

    /**
     * Initializes the database from an XML stream
     * @param xmlFile the input stream of the XML file
     * @throws Throwable
     */
    private SwitchLayoutManager(InputStream xmlFile) throws Throwable {
        initialize(xmlFile);
    }

    /**
     * Creates a new instance of the Switch layout database sourcing the information from the given XML file
     * (the filename can point to a file in the classpath, or to a file in the filesystem; the latter is checked first)
     *
     * @param filename the file to read from
     * @return a new instance
     */
    public static SwitchLayoutManager createInstance(String filename) {
        try {
            File f = new File(filename);

            if (!f.exists())
                return new SwitchLayoutManager(SwitchLayoutManager.class.getResourceAsStream("/" + filename));

            return new SwitchLayoutManager(new FileInputStream(f));
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Checks if a key switch with the given KDKS id is not a non-populated key switch
     * @param kdks the KDKS id to check
     */
    public boolean isAvailable(String kdks) {
        return !unavailable.contains(kdks);
    }

    /**
     * Reads and processes the XML stream
     * @param xmlFile the XML stream
     * @throws Throwable
     *
     * @see ConfigDefGroup
     */
    private void initialize(InputStream xmlFile) throws Throwable {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);

        Element root = doc.getDocumentElement();

        root.normalize();

        if (!root.getNodeName().equals("config"))
            throw new Exception("Invalid XML configuration document");

        rootGroup = new ConfigDefGroup();
        unavailable.clear();

        NodeList nodes = root.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);

            if (n.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) n;

                if (e.getTagName().equalsIgnoreCase("separator"))
                    rootGroup.add(e);
                else if (e.getTagName().equalsIgnoreCase("row")) {
                    NodeList nodes1 = e.getChildNodes();

                    ConfigDefGroup row = new ConfigDefGroup();

                    for (int j = 0; j < nodes1.getLength(); j++) {
                        Node n1 = nodes1.item(j);

                        if (n1.getNodeType() == Node.ELEMENT_NODE) {
                            Element e1 = (Element) n1;

                            if (e1.getTagName().equalsIgnoreCase("key-switch") || e1.getTagName().equalsIgnoreCase("key-slot") || e1.getTagName().equalsIgnoreCase("key-placeholder")
                                    || e1.getTagName().equalsIgnoreCase("separator"))
                                row.add(e1);
                            else
                                throw new RuntimeException("Invalid node [" + e1.getTagName() + "] in XML configuration");
                        }
                    }

                    rootGroup.addChildGroup(row);
                } else
                    throw new RuntimeException("Invalid node [" + e.getTagName() + "] in XML configuration");
            }
        }

        findUnavailableSlots(rootGroup);
    }

    /**
     * Searches for unavailable key switches recursively
     * @param root the root group for recursion
     */
    private void findUnavailableSlots(ConfigDefGroup root) {
        for (IConfigDef def : root) {
            if (def instanceof ConfigDefGroup)
                findUnavailableSlots((ConfigDefGroup) def);

            if ((def instanceof LayoutKeySlotDef) && !(def instanceof LayoutKeyDef))
                unavailable.add(((LayoutKeySlotDef) def).getId());
        }
    }
}
