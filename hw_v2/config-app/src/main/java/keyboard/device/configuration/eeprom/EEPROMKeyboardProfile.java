package keyboard.device.configuration.eeprom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import keyboard.device.FirmwareConstants;
import keyboard.device.MatrixIDConverter;
import keyboard.utility.DefaultsLoader;
import keyboard.utility.FNVHash;
import keyboard.utility.Utils;
import lombok.Getter;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;

/**
 * A key binding profile that is stored in the EEPROM configuration
 *
 * @see EEPROMKeyBinding
 */
public class EEPROMKeyboardProfile implements IEEPROMConfigurationChangedHandler {
    /**
     * The name of this profile
     */
    public String  name      = "Default";
    /**
     * The enabled state of this profile
     */
    public boolean isEnabled = false;

    /**
     * The offset in the configuration where this profile starts
     */
    @JsonIgnore
    public short dataAddress;
    /**
     * The offset in the configuration where the name of this profile starts
     * (The name is stored as a null terminated string)
     */
    @JsonIgnore
    public short nameAddress;

    /**
     * List of the keys managed by this profile
     */
    public List<EEPROMKeyBinding>       keys   = new ArrayList<EEPROMKeyBinding>();
    /**
     * A map for matching short ids to key bindings easily
     */
    private Map<Short, EEPROMKeyBinding> keyMap = new HashMap<Short, EEPROMKeyBinding>();

    /**
     * The serialized form of this profile
     */
    @Getter
    @JsonIgnore
    private byte[] data = null;
    /**
     * Stores whether this profile only has default key bindings or not
     */
    @Getter
    @JsonIgnore
    private boolean defaultsOnly = false;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Initializes this profile with default values
     */
    public EEPROMKeyboardProfile() {
        for (byte d = 0; d < 8; d++)
            for (byte m = 0; m < 16; m++) {
                EEPROMKeyBinding kb = new EEPROMKeyBinding(m, d, (short) 0);
                keyMap.put(kb.shortId(), kb);
                keys.add(kb);
            }

        restoreDefaults();
        generateKeyBindings();

        for (byte d = 0; d < 8; d++)
            for (byte m = 0; m < 16; m++)
                keys.get(d * 16 + m).setChangedCallback(this);
    }

    @Override
    public void onConfigurationChanged() {
        generateKeyBindings();
    }

    /**
     * @return the size of the generated configuration data
     */
    @JsonIgnore
    public int getSize() {
        return data.length;
    }

    /**
     * Loads the default key bindings to this profile
     */
    public void restoreDefaults() {
        if (defaultsOnly)
            return;

        // keys.forEach(DefaultsLoader::restore); <-- throws ConcurrentModificationException sometimes
        for (int i = 0; i < keys.size(); i++)
            DefaultsLoader.restore(keys.get(i));

        defaultsOnly = true;
    }

    /**
     * Finds the binding of a key by it's KDKS id
     * @param id the KDKS id
     * @return the binding
     */
    public Optional<EEPROMKeyBinding> keyByKDKSId(String id) {
        int kd = Integer.parseInt(id.split("/")[0]);
        int ks = Integer.parseInt(id.split("/")[1]);

        Short sid = (short) (MatrixIDConverter.FromKS(ks) * 256 + MatrixIDConverter.FromKD(kd));

        EEPROMKeyBinding bind = keyMap.get(sid);

        if (bind == null) {
            System.err.println("Could not find config entry for KDKS ID: " + id + " (kd=" + kd + ",ks=" + ks + ",sid=" + sid + ")");
            return Optional.empty();
        }

        return Optional.of(bind);
    }

    /**
     * Loads key bindings from a byte buffer to this profile
     * @param buffer the source buffer
     */
    public void loadKeyBindings(ByteBuffer buffer) {
        loadKeyBindings(buffer, false);
    }

    /**
     * Loads key bindings from a byte buffer to this profile
     * @param buffer the source buffer
     * @param long_key_codes specifies the reading format, whether use long or short key codes
     */
    public void loadKeyBindings(ByteBuffer buffer, boolean long_key_codes) {
        for (int i = 0; i < keys.size(); i++) {
            keys.get(i).setOffset(-1);
            keys.get(i).setKeycode((short) 0);
        }

        byte r, c, k;

        while ((c = buffer.get()) != (byte) 0xFF) {
            r = buffer.get();
            k = buffer.get();

            k += c;
            for (; c < k; c++) {
                Short id = (short) (r * 256 + c);

                if (!keyMap.containsKey(id))
                    System.err.format("%04x is not in the keyMap!\n", id);
                else {
                    keyMap.get(id).setOffset(buffer.position());
                    keyMap.get(id).setKeycode(long_key_codes ? buffer.getShort() : (short) (((short) buffer.get()) & 0xff)); // java pls?
                }
            }
        }

        defaultsOnly = true;
        for (EEPROMKeyBinding b : keyMap.values())
            if (!DefaultsLoader.isSetToDefault(b)) {
                defaultsOnly = false;
                break;
            }

        buffer.getShort(); // TODO: verify hash
    }

    /**
     * Serializes this profile's key bindings
     */
    private void generateKeyBindings() {
        ByteBuffer buffer = ByteBuffer.allocate(FirmwareConstants.KEYBD_EEPROM_SIZE);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        Collections.sort(keys);

        int keycodeDataBegin = buffer.position();

        short prevId = -1;
        for (int i = 0; i < keys.size(); i++) {
            EEPROMKeyBinding kb = keys.get(i);

            if (kb.getKeycode() == 0) {
                prevId = -1;
                continue;
            }

            if (prevId == -1 || kb.shortId() != prevId + 1) {
                short size = 0;
                short _prevId = kb.shortId();

                for (int j = i; j < keys.size(); j++) {
                    EEPROMKeyBinding _kb = keys.get(j);

                    if (_kb.shortId() != _prevId)
                        break;

                    if (_kb.getKeycode() != 0)
                        size++;
                    else
                        break;

                    _prevId++;
                }

                if (size == 0)
                    continue;

                prevId = kb.shortId();
                buffer.putShort(prevId);
                buffer.put((byte) size);
            } else
                prevId++;

            buffer.put((byte) kb.getKeycode());
        }

        buffer.put((byte) 0xff);

        short s = Utils.calculateBSDChecksum(buffer.array(), keycodeDataBegin, buffer.position() - keycodeDataBegin);
        buffer.putShort(s);

        byte[] dst = new byte[buffer.position()];

        System.arraycopy(buffer.array(), 0, dst, 0, buffer.position());

        data = dst;
    }

    /**
     * Loads the configuration from an other instance
     * @param profile the other instance
     */
    public void replace(EEPROMKeyboardProfile profile) {
        restoreDefaults();
        name = profile.name;
        isEnabled = profile.isEnabled;

        for (EEPROMKeyBinding key : profile.keys) {
            keys.add(key);
            keyMap.replace(key.shortId(), key);
        }

        generateKeyBindings();
    }
}
