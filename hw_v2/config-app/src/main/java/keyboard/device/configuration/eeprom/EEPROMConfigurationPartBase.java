package keyboard.device.configuration.eeprom;

/**
 * Base class for each part of the EEPROM configuration
 */
public class EEPROMConfigurationPartBase {
    /**
     * Change handler
     */
    private IEEPROMConfigurationChangedHandler onChangedCallback = null;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets the onChangedCallback
     * @param cb the new callback
     */
    public void setChangedCallback(IEEPROMConfigurationChangedHandler cb) {
        onChangedCallback = cb;
    }

    /**
     * Indicates that this part of the EEPROM configuration is changed
     */
    protected void changed() {
        if (onChangedCallback != null)
            onChangedCallback.onConfigurationChanged();
    }
}
