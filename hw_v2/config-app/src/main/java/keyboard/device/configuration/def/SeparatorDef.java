package keyboard.device.configuration.def;

import lombok.Getter;
import org.w3c.dom.Element;

/**
 * Describes a separator
 */
public class SeparatorDef implements IConfigDef {
    public static final String _tag = "separator";

    /**
     * The alignment of this separator (works like padding)
     */
    @Getter
    private double align = 0;

    @Override
    public void parseXMLElement(Element e) {
        if (e.hasAttribute("align")) {
            align = Double.parseDouble(e.getAttribute("align"));
        }
    }
}
