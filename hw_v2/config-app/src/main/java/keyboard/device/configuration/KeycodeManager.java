package keyboard.device.configuration;

import keyboard.device.configuration.def.ConfigDefGroup;
import keyboard.device.configuration.def.IConfigDef;
import keyboard.device.configuration.def.KeycodeConfigDef;
import lombok.Getter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Keycode database
 */
public class KeycodeManager {
    /**
     * The protocol which uses this keycode list
     */
    @Getter private String         protocol;
    /**
     * Name of the layout used for naming the keycodes
     */
    @Getter private String         layoutName;
    /**
     * The root group for storing the keys
     */
    @Getter private ConfigDefGroup rootGroup;

    /**
     * Initializes the database from an XML stream
     * @param xmlFile the input stream of the XML file
     * @throws Throwable
     */
    private KeycodeManager(InputStream xmlFile) throws Throwable {
        initialize(xmlFile);
    }

    /**
     * Creates a new instance of the Keycode Database sourcing the information from the given XML file
     * (the filename can point to a file in the classpath, or to a file in the filesystem; the latter is checked first)
     *
     * @param filename the file to read from
     * @return a new instance
     */
    public static KeycodeManager createInstance(String filename) {
        try {
            File f = new File(filename);

            if (!f.exists())
                return new KeycodeManager(KeycodeManager.class.getResourceAsStream("/" + filename));

            return new KeycodeManager(new FileInputStream(f));
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Looks up a keycode definition by a keycode
     * @param kc the keycode
     * @return a keycode definition for the keycode or null
     */
    public KeycodeConfigDef lookupKeycode(long kc) {
        return lookupKeycodeConfigDef(kc, null, rootGroup);
    }

    /**
     * Looks up a keycode definition by it's name
     * @param name the name
     * @return a keycode definition for the name or null
     */
    public KeycodeConfigDef lookupKeycode(String name) {
        return lookupKeycodeConfigDef(-1, name, rootGroup);
    }

    /**
     * Looks up a keycode definition which either matches the given keycode or the name
     * @param kc the keycode to look for
     * @param name the name to look for
     * @param root the root element for the search (used for recursion)
     * @return the matching keycode definition or null
     */
    private KeycodeConfigDef lookupKeycodeConfigDef(long kc, String name, ConfigDefGroup root) {
        for (IConfigDef def : root) {
            if (def instanceof KeycodeConfigDef) {
                KeycodeConfigDef kcd = (KeycodeConfigDef) def;

                if (kcd.getCode() == kc || kcd.getName().equals(name))
                    return kcd;
            } else if (def instanceof ConfigDefGroup) {
                KeycodeConfigDef res = lookupKeycodeConfigDef(kc, name, (ConfigDefGroup) def);

                if (res != null)
                    return res;
            }
        }

        return null;
    }

    /**
     * Finds a keycode definition for the given string
     *
     * The string can be the name of the keycode or a keycode as decimal integer
     *
     * @param value the string to process
     * @return the found keycode definition
     *
     * @throws RuntimeException when the parsing failed
     */
    public KeycodeConfigDef parse(String value) {
        KeycodeConfigDef def = lookupKeycode(value);
        if (def != null)
            return def;

        long keycode;

        try {
            keycode = Long.parseLong(value);
        } catch (NumberFormatException ex) {
            throw new RuntimeException("could not find any keycode named '" + value + "'");
        }

        return lookupKeycode(keycode);
    }

    /**
     * Gives the name of a keycode
     * @param kc the keycode
     * @return the name of the given keycode
     */
    public String nameOf(long kc) {
        KeycodeConfigDef def = lookupKeycode(kc);
        if (def != null)
            return def.getName();

        return "" + kc;
    }

    /**
     * Reads and processes the XML stream
     * @param xmlFile the XML stream
     * @throws Throwable
     *
     * @see ConfigDefGroup
     */
    private void initialize(InputStream xmlFile) throws Throwable {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);

        Element root = doc.getDocumentElement();

        root.normalize();

        if (!root.getNodeName().equals("codes"))
            throw new Exception("Invalid XML configuration document");

        if (root.hasAttribute("protocol"))
            protocol = root.getAttribute("protocol");

        if (root.hasAttribute("naming"))
            layoutName = root.getAttribute("naming");

        rootGroup = new ConfigDefGroup();
        rootGroup.parseXMLElement(root);
    }
}
