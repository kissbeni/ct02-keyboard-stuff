package keyboard.device.configuration.eeprom;

/**
 * Handler interface for configuration changes
 */
public interface IEEPROMConfigurationChangedHandler {
    /**
     * Handler function which is called when the a part of the configuration is changed
     */
    void onConfigurationChanged();
}
