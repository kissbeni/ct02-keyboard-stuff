package keyboard.device.configuration.def;

/**
 * A class for information about a populated key switch on the keyboard
 *
 * @see LayoutKeySlotDef
 * @see LayoutKeyPlaceholderDef
 */
public class LayoutKeyDef extends LayoutKeySlotDef {
    public static final String _tag = "key-switch";
}
