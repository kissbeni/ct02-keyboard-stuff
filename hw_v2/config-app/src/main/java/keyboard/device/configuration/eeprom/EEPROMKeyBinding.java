package keyboard.device.configuration.eeprom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A key binding that is stored in the EEPROM configuration
 */
public class EEPROMKeyBinding extends EEPROMConfigurationPartBase implements Comparable<EEPROMKeyBinding> {
    /**
     * The MUX id of this binding
     */
    @Getter
    private final byte  muxId;
    /**
     * The DMUx id of this binding
     */
    @Getter
    private final byte  dmuxId;
    /**
     * The bound keycode
     */
    @Getter
    private       short keycode;
    /**
     * The offset in the EEPROM of this binding
     */
    @Getter
    @Setter
    @JsonIgnore
    private       int   offset = -1;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Constructs a key binding
     *
     * @param mux the MUX id of the key
     * @param dmux the DMUX id of the key
     * @param kc the initial keycode
     */
    public EEPROMKeyBinding(byte mux, byte dmux, short kc) {
        muxId = mux;
        dmuxId = dmux;
        keycode = kc;
    }

    private EEPROMKeyBinding() {
        this((byte)0, (byte)0, (short)0);
    }

    /**
     * Changes the keycode for this binding
     * @param kc the new keycode
     */
    public void setKeycode(short kc) {
        if (keycode != kc) {
            keycode = kc;
            changed();
        }
    }

    @Override
    public int compareTo(EEPROMKeyBinding arg0) {
        return Short.compareUnsigned(shortId(), arg0.shortId());
    }

    /**
     * Gives the short id of this binding
     * @return the short id
     */
    public short shortId() {
        return (short) (dmuxId * 256 + muxId);
    }
}
