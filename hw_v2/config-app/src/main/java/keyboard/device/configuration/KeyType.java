package keyboard.device.configuration;

/**
 * Type of a key
 */
public enum KeyType {
    Generic, Macro, Unmapped, Unavailable
}
