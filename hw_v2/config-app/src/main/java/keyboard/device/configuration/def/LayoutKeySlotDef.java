package keyboard.device.configuration.def;

import lombok.Getter;
import org.apache.logging.log4j.util.Strings;
import org.w3c.dom.Element;

/**
 * A class for information about a non-populated key switch on the keyboard
 *
 * @see LayoutKeyDef
 * @see LayoutKeyPlaceholderDef
 */
public class LayoutKeySlotDef extends LayoutKeyPlaceholderDef {
    public static final String _tag = "key-slot";

    /**
     * The KDKS id of the switch
     */
    @Getter
    protected String  id;

    /**
     * Stores whether this switch has a led or not
     */
    protected boolean _hasLed = false;

    /**
     * @return whether this switch has a led or not
     */
    public boolean hasLed() {
        return _hasLed;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void parseXMLElement(Element e) {
        super.parseXMLElement(e);

        id = e.getAttribute("id");

        if (id == null || Strings.isBlank(id))
            throw new NullPointerException("id");

        if (e.hasAttribute("led")) {
            String s = e.getAttribute("led").strip();
            _hasLed = s.equalsIgnoreCase("yes") || s.equals("1");
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[size='" + size.name() + ",id='" + id + "',hasLed=" + _hasLed + "]";
    }
}
