package keyboard.device;

import lombok.Getter;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * A dummy EEPROM that uses a file to write to and read from
 */
public class DummyConfigEEPROM implements IRWStorage {
    /**
     * The name of the file
     */
    @Getter
    private String filename;

    /**
     * Constructs a new dummy EEPROM
     * @param file the file name
     */
    public DummyConfigEEPROM(String file) {
        filename = file;
    }

    @Override
    public void writeAll(byte[] data) throws IOException {
        FileOutputStream fos = new FileOutputStream(filename);
        fos.write(data);
        fos.close();
    }

    @Override
    public byte[] readAll() throws IOException {
        byte[] res;

        FileInputStream fis = new FileInputStream(filename);
        res = fis.readAllBytes();
        fis.close();

        return res;
    }
}
