package keyboard.device.usb.command;

import keyboard.device.usb.CTAPIDeviceWrapper;

/**
 * Command for reloading configuration
 */
public class CommandReloadConfig extends USBDeviceCommand {

    @Override
    public void completeTask(CTAPIDeviceWrapper dev) {
        dev.ctapi_reload_config();
        done();
    }

    @Override
    public boolean isBackgroundTask() {
        return false;
    }
}