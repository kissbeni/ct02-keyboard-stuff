package keyboard.device.usb.command;

import keyboard.device.IRWStorage;
import keyboard.device.usb.CTAPIDeviceWrapper;
import lombok.Getter;

/**
 * Command for reading the EEPROM
 */
public class CommandReadEEPROM extends USBDeviceCommand implements IRWStorage {
    /**
     * The buffer to read to
     */
    @Getter
    private byte[] buffer;
    /**
     * The address to start reading from
     */
    private short  addr;

    /**
     * @param addr the address to read from
     * @param size the amount of bytes to read
     */
    public CommandReadEEPROM(short addr, short size) {
        this.addr = addr;
        buffer = new byte[size];
    }

    @Override
    public void completeTask(CTAPIDeviceWrapper dev) {
        dev.ctapi_read_eeprom(buffer, addr);
        done();
    }

    @Override
    public boolean isBackgroundTask() {
        return false;
    }

    @Override
    public void writeAll(byte[] data) {
        throw new RuntimeException("This object is read-only");
    }

    @Override
    public byte[] readAll() {
        return buffer;
    }
}
