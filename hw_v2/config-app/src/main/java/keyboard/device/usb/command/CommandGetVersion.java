package keyboard.device.usb.command;

import keyboard.device.usb.CTAPIDeviceWrapper;
import lombok.Getter;

/**
 * Command for reading the version
 */
public class CommandGetVersion extends USBDeviceCommand {
    /**
     * The result of the command
     */
    @Getter
    private int result;

    @Override
    public void completeTask(CTAPIDeviceWrapper dev) {
        result = dev.ctapi_get_version();
        done();
    }

    @Override
    public boolean isBackgroundTask() {
        return false;
    }
}
