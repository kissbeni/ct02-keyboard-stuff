package keyboard.device.usb.command;

import keyboard.device.usb.CTAPIDeviceWrapper;
import lombok.Getter;
import lombok.Setter;

/**
 * Base class for all USB commands
 */
public abstract class USBDeviceCommand {
    /**
     * Indicates that the command is being processed
     */
    private volatile boolean  running = false;
    /**
     * Indicates that the command is ready (aka finished)
     */
    @Getter
    private volatile boolean  ready   = false;
    /**
     * The callback to call when the command finishes
     */
    @Setter
    private          Runnable callback;

    /**
     * Notifies this command that the async processing has begun
     * @return false when the processing is in progress, true otherwise
     */
    public boolean notifyBegin() {
        if (running)
            return false;

        ready = false;
        running = true;
        return true;
    }

    /**
     * Notifies this command, that the async processing is completed
     */
    protected void done() {
        running = false;
        ready = true;

        if (callback != null)
            callback.run();
    }

    /**
     * Completes this async task (synchronously)
     * @param dev the device to execute the commands with
     */
    public abstract void completeTask(CTAPIDeviceWrapper dev);

    /**
     * Tells whether this task is a background task or not
     */
    public abstract boolean isBackgroundTask();
}
