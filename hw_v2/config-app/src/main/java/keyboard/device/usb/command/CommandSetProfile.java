package keyboard.device.usb.command;

import keyboard.device.usb.CTAPIDeviceWrapper;
import lombok.Getter;

/**
 * Command for setting the current profile
 */
public class CommandSetProfile extends USBDeviceCommand {

    private int id;

    public CommandSetProfile(int profileId) {
        id = profileId;
    }

    @Override
    public void completeTask(CTAPIDeviceWrapper dev) {
        dev.ctapi_select_profile(id);
        done();
    }

    @Override
    public boolean isBackgroundTask() {
        return true;
    }
}
