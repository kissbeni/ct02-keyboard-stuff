package keyboard.device.usb;

import keyboard.device.usb.command.USBDeviceCommand;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * USB command manager thread
 */
public class USBMainThread extends Thread {
    @Getter
    private static final Logger                          logger          = LogManager.getLogger(USBMainThread.class);
    /**
     * The maximum tries before dropping the connection to the device
     */
    private static final int                             MAX_RETRY_COUNT = 5;
    /**
     * Indicates that the thread is aborted when set to true
     */
    private volatile     boolean                         aborted         = false;
    /**
     * Set to true when a device is available
     */
    private volatile     boolean                         deviceOk        = false;
    /**
     * The queue for handling requests
     */
    private              BlockingQueue<USBDeviceCommand> handlingQueue   = new ArrayBlockingQueue<>(20);
    /**
     * Callback that is run when the current device is changed (or found or lost)
     */
    @Setter
    private              Runnable                        deviceChangedCallback;
    /**
     * Callback that is run when the device is opened for command processing
     */
    @Setter
    private              Runnable                        deviceOpenCallback;
    /**
     * Callback that is tun when the device is closed after command processing
     */
    @Setter
    private              Runnable                        deviceCloseCallback;
    /**
     * Callback that is run when a foreground operation is started
     */
    @Setter
    private              Runnable                        foregroundOperationStartedCallback;
    /**
     * Stores the number of retries
     */
    private              int                             retry_count     = 0;

    public USBMainThread() {
        super("USBMainThread");
    }

    /**
     * Aborts the thread
     */
    public void abort() {
        aborted = true;
    }

    /**
     * @return whether there are devices available or not
     */
    public boolean deviceAvailable() {
        return deviceOk;
    }

    /**
     * Adds a new command to the command queue
     * @param request the request to add
     * @return true if added successfully
     */
    public boolean queueRequest(USBDeviceCommand request) {
        try {
            logger.debug("Adding new request to the command queue [{}]", request);
            handlingQueue.put(request);
        } catch (InterruptedException e) {
            logger.error("Could not add USB command to queue", e);
            return false;
        }

        synchronized (this) {
            notify();
        }
        return true;
    }

    @Override
    public void run() {
        logger.debug("USB main thread started");

        try {
            while (true) {
                if (CTAPIWrapper.ctapi_any_device_present()) {
                    if (!deviceOk) {
                        deviceOk = true;

                        if (deviceChangedCallback != null)
                            deviceChangedCallback.run();
                    }

                    synchronized (this) {
                        wait(1000);
                        handleQueue();
                    }

                    if (aborted) {
                        if (handlingQueue.isEmpty())
                            break;
                        else
                            logger.info("USB thread is aborted, but there is still stuff to process, doing another cycle");
                    }
                } else {
                    if (aborted) break;
                    if (deviceOk) {
                        deviceOk = false;

                        if (deviceChangedCallback != null)
                            deviceChangedCallback.run();
                    }

                    Thread.sleep(100);
                    handlingQueue.clear();
                }
            }
        } catch (InterruptedException e) {
            logger.warn("R.I.P. USB Main thread", e);
        }

        logger.debug("USB main thread exited");
    }

    /**
     * Processes the command queue, executing commands on this thread, then calling their callbacks
     * @throws InterruptedException
     */
    private void handleQueue() throws InterruptedException {
        List<USBDeviceCommand> putback = new ArrayList<>();

        if (!handlingQueue.isEmpty()) {
            logger.debug("Queue is not empty, processing commands...");
            CTAPIDeviceWrapper dev = CTAPIWrapper.ctapi_first_device();

            if (dev == null) {
                logger.warn("Could not open device");
                if (retry_count == MAX_RETRY_COUNT) {
                    logger.error("Maximum retry count reached ({}), command queue cleared!", retry_count);
                    handlingQueue.clear();
                    retry_count = 0;
                } else {
                    retry_count++;
                    logger.warn("Retrying command processing later... ({} tries remaining)", MAX_RETRY_COUNT - retry_count);
                }
                return;
            }

            retry_count = 0;

            dev.ctapi_open();

            if (!dev.ctapi_ping()) {
                logger.warn("Device failed ping");
                dev.ctapi_close();
                dev.ctapi_destroy_device();
                deviceOk = false;

                if (deviceChangedCallback != null)
                    deviceChangedCallback.run();

                return;
            }

            while (!handlingQueue.isEmpty()) {
                USBDeviceCommand handler = handlingQueue.take();

                logger.debug("Processing command [{}]", handler);

                if (deviceOpenCallback != null)
                    deviceOpenCallback.run();

                if (handler.notifyBegin()) {
                    if (!handler.isBackgroundTask() && foregroundOperationStartedCallback != null)
                        foregroundOperationStartedCallback.run();

                    handler.completeTask(dev);
                } else
                    putback.add(handler);
            }

            dev.ctapi_close();

            if (deviceCloseCallback != null)
                deviceCloseCallback.run();

            dev.ctapi_destroy_device();
            logger.debug("Finished processing queued commands ({} left unprocessed)", putback.size());
        }

        for (USBDeviceCommand handler : putback)
            handlingQueue.put(handler);
    }
}
