package keyboard.device.usb.command;

import keyboard.device.usb.CTAPIDeviceWrapper;
import keyboard.device.usb.USBMainThread;

import java.time.LocalTime;

/**
 * Command for setting the current time
 */
public class CommandSetCurrentTime extends USBDeviceCommand {

    @Override
    public void completeTask(CTAPIDeviceWrapper dev) {
        LocalTime time = LocalTime.now();
        USBMainThread.getLogger().info("Setting time to {}:{}", time.getHour(), time.getMinute());
        int res = dev.ctapi_set_time(time.getHour(), time.getMinute());

        if (res != 0) USBMainThread.getLogger().error("Failed to set time: {}", res);

        dev.ctapi_update_display();
        done();
    }

    @Override
    public boolean isBackgroundTask() {
        return true;
    }
}
