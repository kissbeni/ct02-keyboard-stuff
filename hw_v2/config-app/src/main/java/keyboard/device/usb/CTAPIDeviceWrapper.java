package keyboard.device.usb;

/**
 * Wrapper for a native CTAPI device
 */
public class CTAPIDeviceWrapper {
    /**
     * Used by the native library to store the state pointer
     *
     * DO NOT TOUCH!
     */
    private long obj_ptr;

    public CTAPIDeviceWrapper(long obj_ptr) {
        this.obj_ptr = obj_ptr;
    }

    /**
     * Destroys the device
     */
    public native void ctapi_destroy_device();

    /**
     * Pings the device
     * @return true if the device answers
     */
    public native boolean ctapi_ping();

    /**
     * Opens the device for command execution
     * @return true if successful
     */
    public native boolean ctapi_open();

    /**
     * Closes the device
     * @return true if successful
     */
    public native boolean ctapi_close();

    /**
     * Writes the EEPROM
     * @param buffer the bytes to write
     * @param addr the address to write to
     * @return the number of bytes written
     */
    public native int ctapi_write_eeprom(byte[] buffer, short addr);

    /**
     * Reads the EEPROM
     * @param buffer the buffer to read into
     * @param addr the address to read from
     * @return the number of bytes read
     */
    public native int ctapi_read_eeprom(byte[] buffer, short addr);

    /**
     * Reloads the configuration
     * @return an error code (0 if successful)
     */
    public native int ctapi_reload_config();

    /**
     * Reads the last pressed key
     * @return the last pressed key's short id
     */
    public native int ctapi_get_lastkey();

    /**
     * Reads the state of the whole keyboard matrix
     * @param buffer the buffer to read to
     * @return the number of bytes read
     */
    public native int ctapi_get_matrix(byte[] buffer);

    /**
     * Reads the firmware version
     * @return the firmware version
     */
    public native int ctapi_get_version();

    /**
     * Sets the keyboards RGB configuration temporarily
     * @param config the configuration to set
     * @return an error code (0 if successful)
     */
    public native int ctapi_preview_rgb(CTAPIRGBConfig config);

    /**
     * Reboots the keyboard
     * @return an error code (0 if successful)
     */
    public native int ctapi_hard_reset();

    /**
     * Writes the display on the keyboard
     * @param row which row to write
     * @param data the data to write
     * @return an error code (0 if successful)
     */
    public native int ctapi_write_display(int row, byte[] data);

    /**
     * Updates the display
     * @return an error code (0 if successful)
     */
    public native int ctapi_update_display();

    /**
     * Changes the selected profile
     * @param id the id of the profile to select
     * @return an error code (0 if successful)
     */
    public native int ctapi_select_profile(int id);

    /**
     * Sets the time on the keyboard
     * @param h the hours (0..23)
     * @param m the minutes (0..59)
     * @return an error code (0 if successful)
     */
    public native int ctapi_set_time(int h, int m);

    /**
     * Reads the display on the keyboard
     * @param row the row to read
     * @param buffer the buffer to read to
     * @return an error code (0 if successful)
     */
    public native int ctapi_read_display(int row, byte[] buffer);

    /**
     * Reads the currently selected profile
     * @return the selected profile's id
     */
    public native int ctapi_get_profile();
}
