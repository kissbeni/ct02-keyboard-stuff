package keyboard.device.usb.command;

import keyboard.device.MatrixIDConverter;
import keyboard.device.usb.CTAPIDeviceWrapper;
import lombok.Getter;

/**
 * Command for reading the last pressed key
 */
public class CommandGetLastKey extends USBDeviceCommand {
    /**
     * The result of the command
     */
    @Getter private int result;

    @Override
    public void completeTask(CTAPIDeviceWrapper dev) {
        result = dev.ctapi_get_lastkey();
        done();
    }

    @Override
    public boolean isBackgroundTask() {
        return true;
    }

    /**
     * @return the result converted to KDKS id
     */
    public String getResultKDKS() {
        return MatrixIDConverter.ToKD(result & 0xFF) + "/" + MatrixIDConverter.ToKS(result >> 8);
    }
}
