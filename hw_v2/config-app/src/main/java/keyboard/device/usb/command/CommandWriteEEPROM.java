package keyboard.device.usb.command;

import keyboard.device.FirmwareConstants;
import keyboard.device.IRWStorage;
import keyboard.device.usb.CTAPIDeviceWrapper;
import lombok.Setter;

/**
 * Command for writing the EEPROM
 */
public class CommandWriteEEPROM extends USBDeviceCommand implements IRWStorage {
    /**
     * The buffer to write
     */
    @Setter
    private byte[] buffer;
    /**
     * The address to start writing
     */
    private short  addr;

    /**
     * @param addr the address to write to
     */
    public CommandWriteEEPROM(short addr) {
        this.addr = addr;
        buffer = new byte[FirmwareConstants.KEYBD_EEPROM_SIZE];
    }

    @Override
    public void completeTask(CTAPIDeviceWrapper dev) {
        int res = dev.ctapi_write_eeprom(buffer, addr);
        System.out.println(res);
        done();
    }

    @Override
    public boolean isBackgroundTask() {
        return false;
    }

    @Override
    public void writeAll(byte[] data) {
        buffer = data;
    }

    @Override
    public byte[] readAll() {
        throw new RuntimeException("This object is write-only");
    }
}
