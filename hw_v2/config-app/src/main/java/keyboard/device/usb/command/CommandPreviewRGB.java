package keyboard.device.usb.command;

import keyboard.device.configuration.rgb.RGBConfiguration;
import keyboard.device.usb.CTAPIDeviceWrapper;
import keyboard.device.usb.USBMainThread;
import lombok.NonNull;

/**
 * Command for previewing an RGB configuration
 */
public class CommandPreviewRGB extends USBDeviceCommand {
    /**
     * The configuration to preview
     */
    private RGBConfiguration config;

    /**
     * @param config the configuration to preview
     */
    public CommandPreviewRGB(@NonNull RGBConfiguration config) {
        this.config = config;
    }

    @Override
    public void completeTask(CTAPIDeviceWrapper dev) {
        USBMainThread.getLogger().debug("Sending RGB preview: {}", config.toString());

        dev.ctapi_preview_rgb(config.convert());
        done();
    }

    @Override
    public boolean isBackgroundTask() {
        return true;
    }
}
