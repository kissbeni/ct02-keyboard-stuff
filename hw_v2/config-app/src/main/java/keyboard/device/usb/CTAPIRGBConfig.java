package keyboard.device.usb;

public class CTAPIRGBConfig {
    /**
     * The type of the animation
     */
    public byte anim_type;
    /**
     * The brightness of the leds
     */
    public byte brightness;
    /**
     * The speed of the animation
     */
    public byte speed;
    /**
     * The red part of the color
     */
    public byte r;
    /**
     * The green part of the color
     */
    public byte g;
    /**
     * The blue part of the color
     */
    public byte b;
}
