package keyboard.device.usb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Main wrapper for the native CTAPI library
 */
public class CTAPIWrapper {
    private static final Logger logger = LogManager.getLogger(CTAPIWrapper.class);

    /**
     * Initializes the native library
     */
    public static void init() {
        String path;

        try {
            path = new File("libctcfg.so").getCanonicalPath();
        } catch (IOException e1) {
            e1.printStackTrace();
            return;
        }

        logger.info("Loading libctcfg (path: {})", path);
        try {
            System.load(path);
            logger.info("Loaded native library");
        } catch (UnsatisfiedLinkError e) {
            logger.error("Failed to load native ctapi library", e);
            System.exit(1);
        }
    }

    /**
     * @return true if any supported keyboards are connected to the system
     */
    public static native boolean ctapi_any_device_present();

    /**
     * @return the first available CTAPI device
     */
    public static native CTAPIDeviceWrapper ctapi_first_device();
}
