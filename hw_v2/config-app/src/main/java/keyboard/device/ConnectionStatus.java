package keyboard.device;

/**
 * The status of the connection with the keyboard
 */
public enum ConnectionStatus {
    OFFLINE, AVAILABLE, VALID, INVALID, DEVICE_OPEN, OPERATION_IN_PROGRESS;

    /**
     * @return true if this status considered valid
     */
    public boolean isValid() {
        return this == VALID || this.isOpen();
    }

    /**
     * @return true if this status indicates that the device is open
     */
    public boolean isOpen() { return this == DEVICE_OPEN || this == OPERATION_IN_PROGRESS; }
}
