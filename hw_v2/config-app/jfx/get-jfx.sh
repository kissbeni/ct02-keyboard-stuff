#!/bin/bash

git clone -j8 https://github.com/javafxports/openjdk-jfx.git -b jfx-13
cd openjdk-jfx
git apply ../*.patch
chmod +x ./gradlew
./gradlew
cd ..

# hmm, gradle does not add resources?
# anyway, a little bash magic can fix anything :D

resources=$(find . -type d -wholename '*modules/*main/resources')

for x in ${resources[@]}; do
    jar="$PWD/openjdk-jfx/build/sdk/lib/"$(echo $x | grep -oP --regex='javafx.\w+')".jar"
    pushd $x
    zip -r $jar .
    popd
done

#cp ./openjdk-jfx/build/sdk/lib/* ../
