#!/bin/bash

mkdir -p thirdparty

cd thirdparty

if [ -d ./lufa ]; then
    rm -rf ./lufa
fi

git clone https://github.com/abcminiuser/lufa.git

if [ -d ./light_ws2812 ]; then
    rm -rf ./light_ws2812
fi

git clone https://github.com/cpldcpu/light_ws2812.git
cd light_ws2812
git apply ../../ws2812*.diff
cd ..

if [ ! -f ../src/ws2812_config.h ]; then
    cp light_ws2812/light_ws2812_AVR/ws2812_config.h ../src
fi

rm light_ws2812/light_ws2812_AVR/ws2812_config.h
ln -s $(realpath "$PWD/../src/ws2812_config.h") light_ws2812/light_ws2812_AVR/ws2812_config.h
