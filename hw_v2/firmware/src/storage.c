
// SEE: https://www.st.com/resource/en/datasheet/m93c86-w.pdf

// TODO: Use SPI instead of bit-banging (if it is even possible)

#include "storage.h"

#include <avr/io.h>

#include <util/delay.h>

#ifdef __ECC__
#undef PROGMEM
#define PROGMEM
#endif

#define SET_EEPROM_CS       {PORTB |=  (1 << PB4);}
#define CLEAR_EEPROM_CS     {PORTB &= ~(1 << PB4);}

#define SET_SPI_SCK         {PORTB |=  (1 << PB1);}
#define CLEAR_SPI_SCK       {PORTB &= ~(1 << PB1);}

#define SET_SPI_MOSI        {PORTB |=  (1 << PB2);}
#define CLEAR_SPI_MOSI      {PORTB &= ~(1 << PB2);}

#define SPI_MISO_VAL        !!(PINB &  (1 << PB3))

#define EEPROM_CMD_OPCODE_READ  0b10
#define EEPROM_CMD_OPCODE_WRITE 0b01
#define EEPROM_CMD_OPCODE_WEN   0b00
#define EEPROM_CMD_OPCODE_WDS   0b00
#define EEPROM_CMD_OPCODE_ERASE 0b11
#define EEPROM_CMD_OPCODE_ERAL  0b00
#define EEPROM_CMD_OPCODE_WRAL  0b00

#define EEPROM_CMD_OPERAND_WEN  0b11000000000
#define EEPROM_CMD_OPERAND_WDS  0b00000000000
#define EEPROM_CMD_OPERAND_ERAL 0b10000000000
#define EEPROM_CMD_OPERAND_WRAL 0b01000000000

#define EEPROM_CMD_OPERAND_MASK 0b11111111111

#define EEPROM_DELAY 5

static inline __attribute__((always_inline)) void spi_tx(bool bit) {
    if (bit) SET_SPI_MOSI
    else CLEAR_SPI_MOSI

    SET_SPI_SCK
    _delay_us(EEPROM_DELAY);

    CLEAR_SPI_SCK
    CLEAR_SPI_MOSI
    _delay_us(EEPROM_DELAY);
}

static inline __attribute__((always_inline)) bool spi_rx(void) {
    SET_SPI_SCK
    _delay_us(EEPROM_DELAY);

    bool d = SPI_MISO_VAL;

    CLEAR_SPI_SCK
    _delay_us(EEPROM_DELAY);

    return d;
}

void microwire_transfer_begin(uint8_t opcode) {
    SET_EEPROM_CS

    spi_tx(1);
    spi_tx(opcode & 2);
    spi_tx(opcode & 1);
}

void microwire_transfer_operand(uint16_t operand) {
    operand &= EEPROM_CMD_OPERAND_MASK;

    for (uint8_t i = 0; i < 11; i++)
        spi_tx(operand & (1 << (10 - i)));
}

void microwire_transmit_data(uint8_t data) {
    for (uint8_t i = 0; i < 8; i++)
        spi_tx(data & (1 << (7 - i)));
}

uint8_t microwire_receive_data(void) {
    uint8_t tmp = 0;

    for (uint8_t i = 0; i < 8; i++)
        tmp = (tmp << 1) | spi_rx();

    return tmp;
}

void microwire_transfer_finish(void) {
    CLEAR_EEPROM_CS
}

/**
 * Timing:
 *
 * (H=high,F=floating,L=low)
 *
 *       H     |----------------------|
 *       F     |                      |
 * CS:   L  ___|                      |__
 *
 *              <  BUSY  > <  READY  >
 *       H                |-----------|
 *       F  ---|          |           |--
 * MISO: L     |__________|
 *
 */
void eeprom_wait_ready(void) {
    SET_SPI_SCK
    _delay_us(EEPROM_DELAY);
    CLEAR_SPI_SCK
    _delay_us(EEPROM_DELAY);

    SET_EEPROM_CS

    do {
        SET_SPI_SCK
        _delay_us(EEPROM_DELAY);
        CLEAR_SPI_SCK
        _delay_us(EEPROM_DELAY);
    } while (!SPI_MISO_VAL);

    CLEAR_EEPROM_CS
}

uint8_t eeprom_read_one(uint16_t addr) {
    microwire_transfer_begin(EEPROM_CMD_OPCODE_READ);
    microwire_transfer_operand(addr);
    uint8_t data = microwire_receive_data();
    microwire_transfer_finish();

    return data;
}

void eeprom_write_one(uint16_t addr, uint8_t data) {
    microwire_transfer_begin(EEPROM_CMD_OPCODE_WRITE);
    microwire_transfer_operand(addr);
    microwire_transmit_data(data);
    microwire_transfer_finish();
    eeprom_wait_ready();
}

void eeprom_write(uint16_t addr, uint8_t* data, uint16_t len) {
    for (uint16_t i = 0; i < len; i++)
        eeprom_write_one(addr + i, data[i]);
}

void eeprom_read(uint16_t addr, uint8_t* data, uint16_t len) {
    for (uint16_t i = 0; i < len; i++)
        data[i] = eeprom_read_one(addr + i);
}

void eeprom_set_write_enable(bool state) {
    microwire_transfer_begin(state ? EEPROM_CMD_OPCODE_WEN : EEPROM_CMD_OPCODE_WDS);
    microwire_transfer_operand(state ? EEPROM_CMD_OPERAND_WEN : EEPROM_CMD_OPERAND_WDS);
    microwire_transfer_finish();
    _delay_us(EEPROM_DELAY);
}

void eeprom_erase_one(uint16_t addr) {
    microwire_transfer_begin(EEPROM_CMD_OPCODE_ERASE);
    microwire_transfer_operand(addr);
    microwire_transfer_finish();
    eeprom_wait_ready();
}

void eeprom_erase_all(void) {
    microwire_transfer_begin(EEPROM_CMD_OPCODE_ERAL);
    microwire_transfer_operand(EEPROM_CMD_OPERAND_ERAL);
    microwire_transfer_finish();
    eeprom_wait_ready();
}

void eeprom_write_all(uint8_t data) {
    microwire_transfer_begin(EEPROM_CMD_OPCODE_WRAL);
    microwire_transfer_operand(EEPROM_CMD_OPERAND_WRAL);
    microwire_transmit_data(data);
    microwire_transfer_finish();
    eeprom_wait_ready();
}

bool check_hash(void* data, uint16_t data_len, uint16_t sum) {
    uint16_t checksum = 0;

    for (uint16_t i = 0; i < data_len; i++) {
        checksum = (checksum >> 1) + ((checksum & 1) << 15);
        checksum += ((uint8_t*)data)[i];
    }

    return checksum == sum;
}
