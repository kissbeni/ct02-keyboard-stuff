
#ifndef STORAGE_H
#define STORAGE_H

#include <stdint.h>
#include <stdbool.h>

#define FNV_32_INIT 0x811c9dc5
#define FNV_32_PRIME 0x01000193

#define HASHREAD(b) (b) = eeprom_read_one(address++); hash = (hash >> 1) + ((hash & 1) << 15) + b;

#define EEPROM_SIZE_BYTES 2048

enum {
    EEPROM_FLAGS_NONE = 0,
    EEPROM_FLAGS_COMP_MODE = 1
};

uint8_t eeprom_read_one(uint16_t);
void eeprom_write_one(uint16_t, uint8_t);
void eeprom_write(uint16_t,uint8_t*,uint16_t);
void eeprom_read(uint16_t,uint8_t*,uint16_t);
void eeprom_set_write_enable(bool);
void eeprom_erase_one(uint16_t);
void eeprom_erase_all(void);
void eeprom_write_all(uint8_t);

bool check_hash(void*, uint16_t, uint16_t);

#endif
