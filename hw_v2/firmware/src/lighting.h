
#ifndef LIGHTING_H
#define LIGHTING_H

// LUFA defines a CONCAT macro as well as the ws2812 library
#ifdef CONCAT
#undef CONCAT
#endif

#include <stdint.h>
#include <stdbool.h>

#define LED_FLAG_NUMLOCK 2
#define LED_FLAG_CAPSLOCK 4
#define LED_FLAG_SCROLLLOCK 8

#define LED_FLAGS_EMPTY 0
#define LED_FLAGS_TEST (LED_FLAG_NUMLOCK | LED_FLAG_CAPSLOCK | LED_FLAG_SCROLLLOCK)
#define LED_FLAGS_CONFIG_FAILED LED_FLAG_CAPSLOCK

#define RGB_ANIMATION_TYPE_OFF 0
#define RGB_ANIMATION_TYPE_STATIC 1
#define RGB_ANIMATION_TYPE_BREATHING 2
#define RGB_ANIMATION_TYPE_FLASHING 3
#define RGB_ANIMATION_TYPE_WHEEL_FLASHING 4
#define RGB_ANIMATION_TYPE_WHEEL_BREATHING 5
#define RGB_ANIMATION_TYPE_WHEEL 6
#define RGB_ANIMATION_TYPE_MAX 7

typedef struct pixel {
    uint8_t g;
    uint8_t r;
    uint8_t b;
}  pixel_t;

typedef struct rgb_config {
    uint8_t anim_type;
    uint8_t brightness;
    uint8_t speed;
    struct {
        uint8_t r, g, b;
    } color;
} rgb_config_t;

extern rgb_config_t rgb_cfg;

void set_rgb_enabled(bool);
bool get_rgb_enabled(void);
void set_status_led_bits(uint8_t);
void rgb_tick_animation(void);
bool lighting_reload_config(uint16_t*);
void lighting_copy_config(rgb_config_t* cfg);

void rgb_anim_tick_static(void);
void rgb_anim_tick_breathing(void);
void rgb_anim_tick_flashing(void);
void rgb_anim_tick_wheel_flashing(void);
void rgb_anim_tick_wheel_breathing(void);
void rgb_anim_tick_wheel(void);

#endif
