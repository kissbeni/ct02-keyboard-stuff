
#ifndef MACRO_H
#define MACRO_H

#include <stdint.h>
#include <stdbool.h>

void run_macro(uint8_t slot);
void step_macro(void);

#endif
