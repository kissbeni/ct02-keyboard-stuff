
#ifndef DEFS_H
#define DEFS_H

#include <stdint.h>
#include <stdbool.h>

#include <avr/io.h>

#include <util/delay.h>

#define MAX_CONFIG_VERSION  0x03
#define CONFIG_VERSION      0x03

// making EasyClangComplete happy:
#ifdef __ECC__
#undef PROGMEM
#define PROGMEM

#undef pgm_read_byte
#define pgm_read_byte(a) 0
#endif

#ifndef ATTR_NO_INIT
#define ATTR_NO_INIT __attribute__ ((section (".noinit")))
#endif

#define SPLASH_STATUS_STARTUP               0x00
#define SPLASH_STATUS_HEADER_BAD_MAGIC      0x20
#define SPLASH_STATUS_HEADER_BAD_VERSION    0x21
#define SPLASH_STATUS_NO_PROFILES           0x22
#define SPLASH_STATUS_BAD_RGB_CONFIG        0x23
#define SPLASH_STATUS_BAD_MACRO_HEADER      0x24
#define SPLASH_STATUS_KEY_MAP_FAILED        0x25
#define SPLASH_STATUS_OK                    0xFF

void process_keys(void);
bool load_keymappings(void);
void reload_configuration(void);
void set_current_profile(uint8_t id);

typedef struct profile_info {
    uint16_t map_addr;
    uint16_t name_addr;
    bool enabled;
} profile_info_t;

#define PROFILE_INFO_CFG_SIZE (sizeof(uint16_t) * 2)

extern uint8_t current_profile;
extern profile_info_t profiles[4];

extern bool compatibility_mode;

#endif
