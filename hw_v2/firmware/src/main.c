
#include "storage.h"
#include "usb.h"
#include "lighting.h"
#include "display.h"
#include "macro.h"
#include "main.h"

#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>

uint8_t num_profiles;
uint8_t current_profile;

profile_info_t profiles[4];

bool last_btn_state, last_data_act, compatibility_mode;

extern uint16_t keys_down[8];

#if ENABLE_MACROES
extern uint16_t macro_address_list_address;
#endif

void init_io(void) {
    // Every not-connected port is set as an input,
    // to prevent damaging the MCU.
    DDRB = //--+--------+-------------------------------------------
    (0 << PB0) | //  IN | Not-conected
    (1 << PB1) | // OUT | SPI clock
    (1 << PB2) | // OUT | SPI MOSI
    (0 << PB3) | //  IN | SPI MISO
    (1 << PB4) | // OUT | EEPROM CS
    (0 << PB5) | //  IN | I2C SDA
    (0 << PB6) | //  IN | I2C SCL
    (1 << PB7) ; // OUT | RGB strip data
    DDRC = //--+--------+-------------------------------------------
    (0 << PC2) | //  IN | Power button
    (1 << PC4) | // OUT | Shift-register (for leds) clock
    (0 << PC5) | // OUT | Shift-register (for leds) data
                 //     | (Only set as an output when used, because
                 //     | this is indirectly connected to a part of
                 //     | the keyboard mx)
    (0 << PC6) | //  IN | Reserved
    (0 << PC7) ; //  IN | Not-conected
    DDRD = //--+--------+-------------------------------------------
    (1 << PD0) | // OUT | Output demux selector A
    (1 << PD1) | // OUT | Output demux selector B
    (1 << PD2) | // OUT | Output demux selector C
    (0 << PD3) | //  IN | Input mux output 0
    (0 << PD4) | //  IN | Input mux output 1
    (1 << PD5) | // OUT | Input mux selector A
    (1 << PD6) | // OUT | Input mux selector B
    (1 << PD7) ; // OUT | Input mux selector C
    // --------+--------+-------------------------------------------

    PORTC |= (1 << PC2); // Pullup for the power button
}

void reload_configuration(void) {
    uint16_t address = 0;

    display_splash();

    // Read config header magic
    uint16_t magic;
    eeprom_read(address, (uint8_t*)&magic, 2);
    if (magic != 0x02C7) {
        display_splash_status(SPLASH_STATUS_HEADER_BAD_MAGIC);
        return;
    }
    address += 2;

    // Check configuration version
    if (eeprom_read_one(address++) != CONFIG_VERSION) {
        display_splash_status(SPLASH_STATUS_HEADER_BAD_VERSION);
        return;
    }

    uint8_t tmp = eeprom_read_one(address++);
    num_profiles = tmp & 0xf;

    if (num_profiles == 0 || !(tmp & 0x0F)) {
        display_splash_status(SPLASH_STATUS_NO_PROFILES);
        return;
    }

    current_profile = eeprom_read_one(EEPROM_SIZE_BYTES - 1);

    uint8_t first_valid_profile = 0xFF;

    for (uint8_t i = 0; i < 4; i++)
        profiles[i].enabled = false;

    for (uint8_t i = 0; i < num_profiles; i++) {
        eeprom_read(address, (uint8_t*)&profiles[i], PROFILE_INFO_CFG_SIZE);
        address += PROFILE_INFO_CFG_SIZE;
        profiles[i].enabled = !!(tmp & (1 << (i + 4)));

        if (first_valid_profile == 0xFF && profiles[i].enabled)
            first_valid_profile = i;
    }

    if (current_profile >= num_profiles)
        set_current_profile(first_valid_profile);

    if (!lighting_reload_config(&address)) {
        display_splash_status(SPLASH_STATUS_BAD_RGB_CONFIG);
        return;
    }

    if (eeprom_read_one(address++) != 'M') {
        display_splash_status(SPLASH_STATUS_BAD_MACRO_HEADER);
        return;
    }

    #if ENABLE_MACROES
    macro_address_list_address = address;
    #endif
    address += 16 * sizeof(uint16_t);

    if (!load_keymappings()) {
        display_splash_status(SPLASH_STATUS_KEY_MAP_FAILED);
        return;
    }

    display_splash_status(SPLASH_STATUS_OK);
    display_enter_text_mode();
}

void set_current_profile(uint8_t id) {
    current_profile = id;
    eeprom_set_write_enable(true);
    eeprom_write_one(EEPROM_SIZE_BYTES - 1, id);
    eeprom_set_write_enable(false);
}

void set_compatibility_mode(bool v) {
    if (v == compatibility_mode)
        return;

    compatibility_mode = v;
    uint8_t flags = eeprom_read_one(EEPROM_SIZE_BYTES - 2);

    if (v) flags |= EEPROM_FLAGS_COMP_MODE;
    else flags &= ~EEPROM_FLAGS_COMP_MODE;

    eeprom_set_write_enable(true);
    eeprom_write_one(EEPROM_SIZE_BYTES - 2, flags);
    eeprom_set_write_enable(false);

    wdt_enable(WDTO_250MS);
}

#include "softi2c.h"

int main(void) {
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    clock_prescale_set(clock_div_1);

    init_io();

    uint8_t flags = eeprom_read_one(EEPROM_SIZE_BYTES - 2);
    compatibility_mode = !!(flags & EEPROM_FLAGS_COMP_MODE);

    TCCR0A = 0x00;
    TCCR0B = (1<<CS00) | (1<<CS02);
    TIMSK0 = 1 << TOIE0;

    usb_init();
    GlobalInterruptEnable();

    display_init();
    //display_splash_status(SPLASH_STATUS_STARTUP);
    //display_update_status(DISPL_STATUS_ACTIVITY, 0);
    //display_update_status(DISPL_STATUS_BATTERY, 0);
    //display_update_status(DISPL_STATUS_BT, 0);
    //display_update_status(DISPL_STATUS_LEDS, 0);
    //display_update_status(DISPL_STATUS_EXTPWR, 1);
    //display_update_status(DISPL_STATUS_BUTTON, 0);
    display_set_text_visibility(false);

    for (;;) {
        if (last_btn_state) {
            // right: 6/22
            // left: 13/21
            // compatibility mode on: 3/27
            // compatibility mode off: 3/25

            process_keys();

            if (keys_down[6] & ((uint16_t)1 << 11)) {
                display_handle_key(DISPLAY_KEY_RIGHT);
            } else if (keys_down[7] & ((uint16_t)1 << 1)) {
                display_handle_key(DISPLAY_KEY_LEFT);
            } else if (keys_down[1] & ((uint16_t)1 << 8)) {
                set_compatibility_mode(true);
            } else if (keys_down[3] & ((uint16_t)1 << 8)) {
                set_compatibility_mode(false);
            }

            _delay_ms(100);
            continue;
        }
        usb_task();
    }
}

ISR (TIMER0_OVF_vect) {
    bool bt = !(PINC & (1 << PC2));

    if (bt != last_btn_state) {
        last_btn_state = bt;
        display_set_text_visibility(bt);
        display_update_status(DISPL_STATUS_BUTTON, bt);
    }

    rgb_tick_animation();
    display_tick();
}
