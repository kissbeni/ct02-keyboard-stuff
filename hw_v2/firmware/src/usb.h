
#ifndef USB_H
#define USB_H

#include "LUFAConfig.h"

#include <avr/io.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>

#include <LUFA/Platform/Platform.h>
#include <LUFA/Drivers/USB/USB.h>

#ifdef __ECC__
#include <LUFA/Drivers/USB/Class/Common/HIDClassCommon.h>
#endif

#include <stdint.h>

#define INTERFACE_ID_KEYBOARD 1
#define INTERFACE_ID_CDC_CCI  0
#define INTERFACE_ID_CDC_DCI  2

#define STRING_ID_LANG 0
#define STRING_ID_MF 1
#define STRING_ID_PROD 2

#define KEYBOARD_EPADDR (ENDPOINT_DIR_IN | 1)
#define KEYBOARD_EPSIZE 8

#define CFG_NOTIFY_EPADDR   (ENDPOINT_DIR_IN  | 2)
#define CFG_TX_EPADDR       (ENDPOINT_DIR_IN  | 3)
#define CFG_RX_EPADDR       (ENDPOINT_DIR_OUT | 4)

#define CFG_EPSIZE 32
#define CFG_NOTIFY_EPSIZE 8

// USB Descriptor Configuration Header
typedef struct {
    USB_Descriptor_Configuration_Header_t Config;

    // Keyboard interface
    USB_Descriptor_Interface_t               HID_Interface;
    USB_HID_Descriptor_HID_t                 HID_KeyboardHID;
    USB_Descriptor_Endpoint_t                HID_ReportINEndpoint;

    // CDC Control interface
    USB_Descriptor_Interface_t               CDC_CCI_Interface;
    USB_CDC_Descriptor_FunctionalHeader_t    CDC_Functional_Header;
    USB_CDC_Descriptor_FunctionalACM_t       CDC_Functional_ACM;
    USB_CDC_Descriptor_FunctionalUnion_t     CDC_Functional_Union;
    USB_Descriptor_Endpoint_t                CDC_NotificationEndpoint;

    // CDC Data Interface
    USB_Descriptor_Interface_t               CDC_DCI_Interface;
    USB_Descriptor_Endpoint_t                CDC_DataOutEndpoint;
    USB_Descriptor_Endpoint_t                CDC_DataInEndpoint;
} USB_Descriptor_Configuration_t;

typedef struct {
    USB_Descriptor_Configuration_Header_t Config;

    // Keyboard interface
    USB_Descriptor_Interface_t            HID_Interface;
    USB_HID_Descriptor_HID_t              HID_KeyboardHID;
    USB_Descriptor_Endpoint_t             HID_ReportINEndpoint;
} USB_Descriptor_Configuration_C_t;

void usb_task(void);
void usb_init(void);
void usb_cfg_task(void);
void usb_cfg_handle_read(uint16_t addr, uint16_t nblocks);
void usb_cfg_handle_write(uint16_t addr, uint16_t nblocks);

#endif
