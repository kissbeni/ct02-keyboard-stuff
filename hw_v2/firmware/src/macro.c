
#include "macro.h"
#include "storage.h"

#define MONOP   0
#define MOHOLDD 1
#define MOHOLDU 2
#define MOPRESS 3
#define MODELAY 4
#define MOREPN  5
#define MOEXIT  15

#if ENABLE_MACROES
uint8_t macro_program_counter = 0xFF;
uint8_t current_byte, exec_offset, press_key, delay_counter;
uint16_t start_address, repn_info, macro_address_list_address;
uint8_t macro_keys_down[5];

void run_macro(uint8_t slot) {
    if (slot > 15)
        return;

    eeprom_read(macro_address_list_address + sizeof(uint16_t)*slot, (uint8_t*)&start_address, sizeof(uint16_t));

    if (start_address == 0 || start_address >= 2048)
        return;

    for (uint8_t i = 0; i < 5; i++)
        macro_keys_down[i] = 0;

    current_byte = eeprom_read_one(start_address);
    exec_offset = 0;
    macro_program_counter = 0;
    press_key = 0xFF;
}

uint8_t get_byte(void) {
    exec_offset++;
    return eeprom_read_one(start_address + exec_offset);
}

void set_hold(uint8_t arg, uint8_t set) {
    for (uint8_t i = 0; i < 5; i++) {
        uint8_t kd = macro_keys_down[i];

        if (kd == arg) {
            if (!(set & 1))
                macro_keys_down[i] = 0;

        } else if (!kd && (set & 1)) {
            macro_keys_down[i] = arg;

            if (set & 2)
                press_key = i;

            break;
        }
    }
}

void do_repn(uint8_t arg) {
    if (repn_info == 0)
        repn_info = ((uint16_t)(current_byte & 7) << 8) | arg;
    else {
        uint8_t c = repn_info >> 6;

        if (c == 0) {
            repn_info = 0;
            return;
        }

        c -= 1;
        repn_info = (repn_info & 0x3F) | ((uint16_t)c << 6);
    }

    uint8_t a = repn_info & 0x3F;
    macro_program_counter -= a * 2;
    exec_offset -= a + 1;
    current_byte = get_byte();
}

void step_macro(void) {
    if (press_key < 5) {
        macro_keys_down[press_key] = 0;
        press_key = 0xFF;
        return;
    }

    if (delay_counter > 0) {
        delay_counter--;
        return;
    }

    if (macro_program_counter > 50)
        return;

    uint8_t opcode, arg_byte = 0;

    if ((macro_program_counter % 2) == 0)
        opcode = current_byte >> 4;
    else
        opcode = current_byte & 0xF;

    if (opcode > 0 && opcode < 10)
        arg_byte = get_byte();

    switch (opcode) {
        case MONOP:
            break;
        case MOHOLDD:
            set_hold(arg_byte, 1);
            break;
        case MOHOLDU:
            set_hold(arg_byte, 0);
            break;
        case MOPRESS:
            set_hold(arg_byte, 3);
            break;
        case MODELAY:
            delay_counter = arg_byte;
            break;
        case MOREPN:
            do_repn(arg_byte);
            return;
        case MOEXIT:
            macro_program_counter = 0xFF;
            return;
    }

    if ((macro_program_counter++ % 2) == 1)
        current_byte = get_byte();
}
#endif
