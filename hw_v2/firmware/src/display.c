
#include "display.h"
#include "softi2c.h"
#include "storage.h"
#include "splash.h"
#include "lighting.h"
#include "main.h"
#include <string.h>

#define SLEEP_COUNTER_BEGIN 2000 // about 30 seconds
#define SPLASH_TIMEOUT_COUNTER 200 // this pervents blinking of the splash screen

enum {
    DFLG_NONE           = 0,
    DFLG_IN_TEXT_MODE   = 1,
    DFLG_AWAKE          = 2,
    DFLG_TEXT_MODE_REQ  = 4,
    DFLG_EXT_DATA       = 8,
    DFLG_SHOW_EXT_DATA  = 16
};

char display_data[DISPLAY_HEIGHT_CHARS * DISPLAY_WIDTH_CHARS];
char display_exdata[(DISPLAY_HEIGHT_CHARS - 2) * DISPLAY_WIDTH_CHARS];

uint8_t display_flags;

uint16_t sleep_counter ATTR_NO_INIT;
uint8_t splash_counter ATTR_NO_INIT;

uint16_t time_bcd ATTR_NO_INIT;

bool act_status = false;

void display_send_command_list(const uint8_t *c, uint8_t n) {
    i2c_start(DISPLAY_ADDR);
    i2c_write(0x00);
    while (n--) i2c_write(pgm_read_byte(c++));
    i2c_stop();
}

void display_send_command(uint8_t c) {
    i2c_start(DISPLAY_ADDR);
    i2c_write(0x00);
    i2c_write(c);
    i2c_stop();
}

void display_init(void) {
    i2c_init();

    display_flags = DFLG_EXT_DATA;

    // initialize display
    static const uint8_t PROGMEM init[] = {
        0xAE, // display off
        0xD5,
        0x80,
        0xA8,
        DISPLAY_HEIGHT - 1,   // height
        0xD3, 0x00, // set display offset
        0x40, // set startline to 0
        0x8D,
        0x14,
        0x20, 0x00, // addressing mode
        0xA1,
        0xC8,
        0xDA,
        0x02,
        0x81, 0, // set contrast (still visible, but probably less chance of burn-in)
        0xD9,
        0xF1,
        0xDB,
        0x40,
        0xA4, // display all on resume
        0xA6, // normal display
        0x2E, // disable scrolling
        0xAF  // display on
    };

    display_send_command_list(init, sizeof(init));

    display_flags |= DFLG_AWAKE;
    sleep_counter = SLEEP_COUNTER_BEGIN;
    time_bcd = 0;

    memset(display_data, 0, sizeof(display_data));
    memset(display_exdata, 0, sizeof(display_exdata));

    if (compatibility_mode) {
        static const char str[] PROGMEM = "Compatible mode.";
        strcpy_P(display_exdata, str);
    } else {
        static const char str[] PROGMEM = "Ready.";
        strcpy_P(display_exdata, str);
    }

    display_data[DISPL_OFFS_TIME+2] = ':';

    display_clear();
    display_splash();
}

void display_data_begin(void) {
    static const uint8_t PROGMEM cmds[] = {
        0x22,
        0,                  // Page start address
        DISPLAY_HEIGHT / 8, // Page end address

        0x21,
        0,                 // Column start address
        DISPLAY_WIDTH - 1  // Column end address
    };
    display_send_command_list(cmds, sizeof(cmds));
}

void display_clear(void) {
    display_data_begin();

    i2c_start(DISPLAY_ADDR);
    i2c_write(0x40);

    for (uint16_t count = DISPLAY_BYTE_COUNT; count--;)
        i2c_write(0);

    i2c_stop();
}

void display_splash(void) {
    display_flags &= ~(DFLG_IN_TEXT_MODE | DFLG_TEXT_MODE_REQ);
    splash_counter = SPLASH_TIMEOUT_COUNTER;
    display_data_begin();

    i2c_start(DISPLAY_ADDR);
    i2c_write(0x40);

    const uint8_t* b = splash;
    for (uint16_t count = DISPLAY_BYTE_COUNT; count--;)
        i2c_write(pgm_read_byte(b++));

    i2c_stop();
}

void display_splash_status(uint8_t sc) {
    display_flags &= ~(DFLG_IN_TEXT_MODE | DFLG_TEXT_MODE_REQ);
    splash_counter = SPLASH_TIMEOUT_COUNTER;
    display_data_begin();

    i2c_start(DISPLAY_ADDR);
    i2c_write(0x40);

    uint16_t ch = (uint16_t)num2hex_char(sc >> 4) * 8;
    for (uint8_t i = 0; i < 8; i++)
        i2c_write(pgm_read_byte(&font[ch + i]));

    ch = (uint16_t)num2hex_char(sc) * 8;
    for (uint8_t i = 0; i < 8; i++)
        i2c_write(pgm_read_byte(&font[ch + i]));

    i2c_stop();
}

void display_enter_text_mode(void) {
    display_flags |= DFLG_TEXT_MODE_REQ;
}

void display_update(void) {
    char ch;
    uint8_t mask, data;
    char* dp = display_data;

    if (!(display_flags & DFLG_IN_TEXT_MODE))
        return;

    display_data_begin();

    i2c_start(DISPLAY_ADDR);
    i2c_write(0x40);

    for (uint16_t i = 0; i < sizeof(display_data); i++) {
        if (i == (DISPLAY_WIDTH_CHARS * 2) && (display_flags & (DFLG_EXT_DATA | DFLG_SHOW_EXT_DATA)) == (DFLG_EXT_DATA | DFLG_SHOW_EXT_DATA))
            dp = display_exdata;

        ch = *dp & 0x7F;
        mask = 0;

        if (*dp & 0x80)
            mask = 0xFF;

        dp++;

        const uint8_t* baseptr = &font[ch * 8];

        for (uint8_t u = 0; u < 8; u++) {
            data = pgm_read_byte(baseptr + u) ^ mask;

            // draw a line under the status bar
            if (i >= DISPLAY_WIDTH_CHARS && i < DISPLAY_WIDTH_CHARS * 2)
                data |= 1;

            i2c_write(data);
        }
    }

    i2c_stop();
}

void display_set_char_at(uint8_t x, uint8_t y, char ch) {
    if (x > DISPLAY_WIDTH_CHARS || y > DISPLAY_HEIGHT_CHARS)
        return;

    display_data[x + y * DISPLAY_WIDTH_CHARS] = ch;
}

void display_update_profiles(void) {
    static const char str[] PROGMEM = "Profile:";
    strcpy_P(&display_data[2 * DISPLAY_WIDTH_CHARS], str);

    uint8_t x = DISPLAY_WIDTH_CHARS - 4;

    display_set_char_at(x, 2, 0x00);
    display_set_char_at(x + 1, 2, 0x0d);
    display_set_char_at(x + 2, 2, 0x00);

    for (int i = 0; i < 4; i++) {
        if (profiles[i].enabled) {
            if (i < current_profile)
                display_set_char_at(x, 2, 0x11);
            else if (i > current_profile)
                display_set_char_at(x + 2, 2, 0x10);
        }
    }

    x += 3;

    for (x = 0; x < DISPLAY_WIDTH_CHARS; x++)
        display_set_char_at(x, 3, 0);

    for (x = 0; x < DISPLAY_WIDTH_CHARS; x++) {
        uint8_t b = eeprom_read_one(profiles[current_profile].name_addr + x);
        if (!b) break;
        display_set_char_at(x, 3, b & 127);
    }

    display_update();
}

void display_handle_key(uint8_t key) {
    uint8_t i;
    switch (key) {
        case DISPLAY_KEY_RIGHT:
            for (i = current_profile + 1; i < 4; i++)
                if (profiles[i].enabled)
                    break;
            break;
        case DISPLAY_KEY_LEFT:
            for (i = current_profile - 1; i >= 0; i--)
                if (profiles[i].enabled)
                    break;
            break;
        default:
            return;
    }

    set_current_profile(i);
    load_keymappings();
}

void display_update_status(uint8_t item, uint8_t value) {
    bool wake = true;

    switch (item) {
        case DISPL_STATUS_ACTIVITY:
            if (act_status != value) {
                display_set_char_at(DISPL_OFFS_ACTIVITY, 0, value ? 0x1C : 0);
                act_status = value;
            }

            wake = false;
            break;
        case DISPL_STATUS_BATTERY:
            if (value)
                display_set_char_at(DISPL_OFFS_BATTERY, 0, 8 + (value / 51));
            else
                display_set_char_at(DISPL_OFFS_BATTERY, 0, 7);
            wake = false;
            break;
        /*case DISPL_STATUS_SECURITY:
            display_set_char_at(DISPL_OFFS_SECURITY, 0, value ? 5 : 0);
            break;
        case DISPL_STATUS_BT:
            if (value == 0)
                display_set_char_at(DISPL_OFFS_BT, 0, 0);
            else if (value == 1)
                display_set_char_at(DISPL_OFFS_BT, 0, 4);
            else
                display_set_char_at(DISPL_OFFS_BT, 0, 0x84);
            break;*/
        case DISPL_STATUS_LEDS:
            display_set_char_at(DISPL_OFFS_CLED, 0, 1 | ((value & LED_FLAG_CAPSLOCK) ? 0x80 : 0));
            display_set_char_at(DISPL_OFFS_SLED, 0, 2 | ((value & LED_FLAG_SCROLLLOCK) ? 0x80 : 0));
            display_set_char_at(DISPL_OFFS_NLED, 0, 3 | ((value & LED_FLAG_NUMLOCK) ? 0x80 : 0));
            break;
        case DISPL_STATUS_EXTPWR:
            display_set_char_at(DISPL_OFFS_EXTPWR, 0, value ? 6 : 0);
            break;
        case DISPL_STATUS_BUTTON:
            display_set_char_at(DISPL_OFFS_BUTTON, 0, value ? 19 : 0);
            break;
        default:
            return;
    }

    display_data[DISPL_OFFS_TIME] = num2hex_char(time_bcd >> 12);
    display_data[DISPL_OFFS_TIME+1] = num2hex_char(time_bcd >> 8);
    display_data[DISPL_OFFS_TIME+3] = num2hex_char(time_bcd >> 4);
    display_data[DISPL_OFFS_TIME+4] = num2hex_char(time_bcd);

    if (wake) display_wake();
    display_update();
}

void display_tick(void) {
    if ((display_flags & (DFLG_TEXT_MODE_REQ | DFLG_IN_TEXT_MODE)) == DFLG_TEXT_MODE_REQ) {
        splash_counter--;
        if (splash_counter == 0) {
            display_clear();
            display_flags |= DFLG_IN_TEXT_MODE;
            display_update();
        }
    }

    if (!(display_flags & DFLG_AWAKE))
        return;

    sleep_counter--;

    if (sleep_counter == 0) {
        display_flags &= ~DFLG_AWAKE;
        display_send_command(0xAE);
    }
}

void display_wake(void) {
    if (!(display_flags & DFLG_AWAKE)) {
        display_flags |= DFLG_AWAKE;
        display_update(); // make sure that we are up to date
        display_send_command(0xAF);
    }

    sleep_counter = SLEEP_COUNTER_BEGIN;
}

void display_set_text_visibility(bool local) {
    if (local)
        display_flags &= ~DFLG_SHOW_EXT_DATA;
    else
        display_flags |= DFLG_SHOW_EXT_DATA;
}

void display_set_time(uint8_t min_bcd, uint8_t hr_bcd) {
    time_bcd = (((uint16_t)hr_bcd) << 8) | min_bcd;
}

char num2hex_char(uint8_t n) {
    n &= 0xf;
    if (n < 10)
        return '0' + n;

    return 'a' + (n - 10);
}
