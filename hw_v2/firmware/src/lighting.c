
#include "lighting.h"
#include "storage.h"
#include "main.h"

#include <string.h>

#include <avr/io.h>
#include <avr/pgmspace.h>

#include <util/delay.h>

#include <light_ws2812.h>

struct pixel p = {255, 0, 0};
struct pixel pixels[15];

bool rgb_enabled = false;

rgb_config_t rgb_cfg;

int16_t anim_speed = 0;
uint8_t anim_pos = 0;

void set_rgb_enabled(bool e) {
    rgb_enabled = e;
}

bool get_rgb_enabled(void) {
    return rgb_enabled;
}

void set_status_led_bits(uint8_t flags) {
    DDRC |= 0b00110000;

    for (uint8_t i = 0; i < 8; i++) {
        PORTC &= ~0b10000; // Set PIN_LED_SERIAL_CLK to LOW
        PORTC = (PORTC & ~0b100000) | ((flags & 1) << 5); // Set PIN_LED_SERIAL_DATA
        PORTC |= 0b10000; // Set PIN_LED_SERIAL_CLK to HIGH
        flags >>= 1;
    }

    DDRC &= 0b11001111;
}

void rgb_fill(uint16_t brightness) {
    brightness &= 0xFF;
    uint16_t brightness_mod = (uint16_t)brightness + 1;

    for (uint8_t i = 0; i < 15; i++) {
        pixels[i].g = ((uint16_t)p.g * brightness_mod) >> 8;
        pixels[i].r = ((uint16_t)p.r * brightness_mod) >> 8;
        pixels[i].b = ((uint16_t)p.b * brightness_mod) >> 8;
    }
}

void rgb_clear(void) {
    for (uint8_t i = 0; i < 15; i++) {
        pixels[i].g = 0;
        pixels[i].r = 0;
        pixels[i].b = 0;
    }
}

void wheel(uint8_t pos, pixel_t* out) {
    if (pos < 85) {
        out->r = pos * 3;
        out->g = 255 - pos * 3;
        out->b = 0;
    } else if (pos < 170) {
        pos -= 85;
        out->r = 255 - pos * 3;
        out->g = 0;
        out->b = pos * 3;
    } else {
        pos -= 170;
        out->r = 0;
        out->g = pos * 3;
        out->b = 255 - pos * 3;
    }
}

void rgb_tick_animation(void) {
    if (rgb_enabled && rgb_cfg.anim_type > 0) {
        if (anim_speed <= 0) {
            anim_speed = 255 - rgb_cfg.speed;

            p.r = rgb_cfg.color.r;
            p.g = rgb_cfg.color.g;
            p.b = rgb_cfg.color.b;

            switch (rgb_cfg.anim_type) {
                case RGB_ANIMATION_TYPE_STATIC:          rgb_anim_tick_static(); break;
                case RGB_ANIMATION_TYPE_BREATHING:       rgb_anim_tick_breathing(); break;
                case RGB_ANIMATION_TYPE_FLASHING:        rgb_anim_tick_flashing(); break;
                case RGB_ANIMATION_TYPE_WHEEL_FLASHING:  rgb_anim_tick_wheel_flashing(); break;
                case RGB_ANIMATION_TYPE_WHEEL_BREATHING: rgb_anim_tick_wheel_breathing(); break;
                case RGB_ANIMATION_TYPE_WHEEL:           rgb_anim_tick_wheel(); break;
            }

            anim_pos++;
        } else {
            anim_speed -= 10;
        }
    } else {
        anim_speed = 0;
        rgb_clear();
    }

    ws2812_setleds((struct cRGB *)pixels, 15);
}

uint8_t tmp[4];

bool lighting_reload_config(uint16_t* addr) {
    eeprom_read(*addr, tmp, 4);
    if (memcmp(tmp, "!RGB", 4)) return false;
    (*addr) += 4;

    eeprom_read((*addr), (uint8_t*)&rgb_cfg, sizeof(rgb_config_t));
    *addr += sizeof(rgb_config_t);

    uint16_t hash;
    eeprom_read(*addr, (uint8_t*)&hash, sizeof(uint16_t));
    (*addr) += sizeof(uint16_t);
    return check_hash(&rgb_cfg, sizeof(rgb_config_t), hash);
}

void lighting_copy_config(rgb_config_t* cfg) {
    memcpy(&rgb_cfg, cfg, sizeof(rgb_config_t));
}

void rgb_anim_tick_static(void) {
    rgb_fill(rgb_cfg.brightness);
}

void rgb_anim_tick_breathing(void) {
    uint8_t pos = anim_pos & 0xFF;
    if (pos < 128)
        rgb_fill(pos * 2);
    else
        rgb_fill(255 - (pos - 128) * 2);
}

void rgb_anim_tick_flashing(void) {
    if (anim_pos % 5 < 2) {
        rgb_clear();
        return;
    }

    rgb_fill(rgb_cfg.brightness);
}

void rgb_anim_tick_wheel_flashing(void) {
    if (anim_pos % 5 < 2) {
        rgb_clear();
        return;
    }

    wheel(anim_pos, &p);
    rgb_fill(rgb_cfg.brightness);
}

void rgb_anim_tick_wheel_breathing(void) {
    wheel(anim_pos, &p);
    rgb_fill(rgb_cfg.brightness);
}

void rgb_anim_tick_wheel(void) {
    for (uint8_t i = 0; i < 15; i++)
        wheel(anim_pos + i, &pixels[i]);
}
