
#include "main.h"
#include "storage.h"
#include "display.h"

#include <string.h>

uint8_t last_pressed_muxid = 0xff;
uint8_t last_pressed_dmuxid = 0xff;

uint16_t keys_down[8] = { 0 };
uint8_t keymap[8][16] = { 0 };

bool any_key_down = false;

#define _BS(n, k) (!!((n) & (1 << k)))

inline __attribute__((always_inline)) uint8_t reverse_bit_order_3(uint8_t n) {
    return _BS(n, 2) | (_BS(n, 1) << 1) | (_BS(n, 0) << 2);
}

uint16_t process_row(const uint8_t i) {
    register uint8_t lo_bits = 0;
    register uint8_t hi_bits = 0;

    for (uint8_t j = 0; j < 8; j++) {
        uint8_t u = reverse_bit_order_3(j);
        PORTD = i | (u << 5);
        _delay_us(5);
        uint8_t pins = PIND;

        if (pins & (1 << PD3)) {
            lo_bits |= (uint16_t)1 << j;
            last_pressed_dmuxid = i;
            last_pressed_muxid = j;
        }

        if (pins & (1 << PD4)) {
            hi_bits |= (uint16_t)1 << u;
            last_pressed_dmuxid = i;
            last_pressed_muxid = u + 8;
        }
    }

    return lo_bits | (hi_bits << 8);
}

void process_keys(void) {
    uint16_t res;

    any_key_down = false;

    // first scan
    for (uint8_t i = 0; i < 8; i++) {
        PORTD = i;
        _delay_us(20);

        keys_down[i] = process_row(i);
    }

    _delay_ms(1);

    // debounce
    for (uint8_t i = 0; i < 8; i++) {
        PORTD = i;
        _delay_us(20);

        res = process_row(i);
        keys_down[i] &= res;
        any_key_down |= !!res;
    }
}

bool load_keymappings() {
    uint16_t address = profiles[current_profile].map_addr;

    memset(keymap, 0, sizeof(keymap));

    uint16_t hash = 0;
    uint8_t r, c, k, kc;

    HASHREAD(c)
    while (c != 0xFF) {
        HASHREAD(r)
        HASHREAD(k)

        k += c;
        for (; c < k; c++) {
            HASHREAD(kc)

            if (r >= 8 || c >= 16)
                return false;

            if (kc != 0xFF)
                keymap[r][c] = kc;
        }

        HASHREAD(c)
    }

    uint16_t read_hash;
    eeprom_read(address, (uint8_t*)&read_hash, sizeof(uint16_t));
    if (read_hash != hash)
        return false;

    display_update_profiles();

    return true;
}
