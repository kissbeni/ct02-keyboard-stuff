
#include "usb.h"
#include "storage.h"
#include "display.h"
#include "lighting.h"
#include "main.h"

#define CFG_DATA_HEADER_MAX_SIZE (sizeof(uint8_t) + sizeof(uint8_t) + sizeof(uint8_t))
#define CFG_DATA_SIZE (CFG_EPSIZE - CFG_DATA_HEADER_MAX_SIZE)

uint8_t buffer[sizeof(rgb_config_t)];

extern uint8_t last_pressed_muxid;
extern uint8_t last_pressed_dmuxid;
extern uint16_t keys_down[8];

#define KEYBD_CMD_WRITE_EEPROM              'w'
#define KEYBD_CMD_RELOAD_CFG                'R'
#define KEYBD_CMD_PREVIEW_RGB               'p'
#define KEYBD_CMD_HARD_RESET                'b'
#define KEYBD_CMD_WRITE_DISPL               'd'
#define KEYBD_CMD_UPDATE_DISPL              'u'
#define KEYBD_CMD_SEL_PROFILE               's'
#define KEYBD_CMD_SET_TIME                  't'

#define KEYBD_CMD_GET_LAST_PRESSED_KEY      'l'
#define KEYBD_CMD_GET_KEYMX_PRESSED_KEYS    'm'
#define KEYBD_CMD_READ_EEPROM               'r'
#define KEYBD_CMD_GET_MAX_CONFIG_VERSION    'V'
#define KEYBD_CMD_READ_DISPL                'D'
#define KEYBD_CMD_GET_PROFILE               'P'

#define KEYBD_RES_INVALID_CMD '?'
#define KEYBD_RES_OK          '.'
#define KEYBD_RES_DATA        '+'
#define KEYBD_RES_BAD_RANGE   '$'
#define KEYBD_RES_BAD_DATA    '/'
#define KEYBD_RES_GENERIC_ERR '#'

static uint8_t usb_fetch_byte(void) {
    Endpoint_SelectEndpoint(CFG_RX_EPADDR);

    uint16_t counter = 2500;

    while (!Endpoint_IsReadWriteAllowed()) {
        Endpoint_ClearOUT();

        while (!Endpoint_IsOUTReceived()) {
            if (USB_DeviceState == DEVICE_STATE_Unattached || counter-- == 0)
                return 0;
        }

        if (counter-- == 0)
            return 0;
    }

    return Endpoint_Read_8();
}

static bool usb_wait_in_ready(void) {
    uint16_t counter = 400;

    while (!Endpoint_IsINReady()) {
        _delay_us(100);
        if ((USB_DeviceState == DEVICE_STATE_Unattached) || (counter-- == 0))
            return false;
    }

    return true;
}

static void usb_write_byte(const uint8_t data) {
    Endpoint_SelectEndpoint(CFG_TX_EPADDR);

    uint16_t counter = 2500;

    if (!Endpoint_IsReadWriteAllowed()) {
        Endpoint_ClearIN();

        if (!usb_wait_in_ready() || counter-- == 0)
            return;
    }

    Endpoint_Write_8(data);
}

void usb_handle_cmd_write_eeprom(void) {
    uint8_t hi = usb_fetch_byte();

    uint8_t length = hi >> 3;

    if (length >= CFG_DATA_SIZE) {
        usb_write_byte(KEYBD_RES_BAD_RANGE);
        return;
    }

    uint16_t addr = ((uint16_t)(hi & 7) << 8) | usb_fetch_byte();

    eeprom_set_write_enable(true);

    for (uint16_t i = addr; i < addr + length; i++)
        eeprom_write_one(i, usb_fetch_byte());

    eeprom_set_write_enable(false);

    usb_write_byte(KEYBD_RES_OK);
}

void usb_handle_cmd_preview_rgb(void) {
    uint32_t hash = FNV_32_INIT;
    uint8_t b;

    for (uint8_t i = 0; i < sizeof(rgb_config_t); i++) {
        b = usb_fetch_byte();
        hash = (hash ^ b) * FNV_32_PRIME;
        buffer[i] = b;
    }

    for (uint8_t i = 0; i < 4; i++)
        if (((uint8_t*)&hash)[i] != usb_fetch_byte()) {
            usb_write_byte(KEYBD_RES_BAD_DATA);
            return;
        }

    lighting_copy_config((rgb_config_t*)buffer);
    usb_write_byte(KEYBD_RES_OK);
}

void usb_handle_cmd_write_displ(void) {
    uint8_t row = usb_fetch_byte() - '0';

    if (row >= DISPLAY_HEIGHT_CHARS - 2) {
        usb_write_byte(KEYBD_RES_BAD_RANGE);
        return;
    }

    for (uint8_t i = 0; i < DISPLAY_WIDTH_CHARS; i++)
        display_exdata[i + (DISPLAY_WIDTH_CHARS * row)] = usb_fetch_byte();

    usb_write_byte(KEYBD_RES_OK);
}

void usb_handle_cmd_sel_profile(void) {
    uint8_t prof = usb_fetch_byte() - '0';

    if (prof >= 4) {
        usb_write_byte(KEYBD_RES_BAD_RANGE);
        return;
    }

    if (!profiles[prof].enabled) {
        usb_write_byte(KEYBD_RES_BAD_DATA);
        return;
    }

    set_current_profile(prof);

    if (load_keymappings())
        usb_write_byte(KEYBD_RES_OK);
    else
        usb_write_byte(KEYBD_RES_GENERIC_ERR);
}

void usb_handle_cmd_get_keymx_pressed_keys(void) {
    usb_write_byte(KEYBD_RES_DATA);
    usb_write_byte(sizeof(uint16_t) * 8);

    for (uint8_t i = 0; i < (sizeof(uint16_t) * 8); i++)
        usb_write_byte(((uint8_t*)keys_down)[i]);
}

void usb_handle_cmd_read_eeprom(void) {
    uint8_t hi = usb_fetch_byte();

    uint8_t length = hi >> 3;

    if (length >= CFG_DATA_SIZE) {
        usb_write_byte(KEYBD_RES_BAD_RANGE);
        return;
    }

    uint16_t addr = ((uint16_t)(hi & 7) << 8) | usb_fetch_byte();

    usb_write_byte(KEYBD_RES_DATA);
    usb_write_byte(length);

    for (uint8_t i = 0; i < length; i++)
        usb_write_byte(eeprom_read_one(addr + i));
}

void usb_handle_cmd_read_displ(void) {
    uint8_t row = usb_fetch_byte() - '0';

    if (row >= DISPLAY_HEIGHT_CHARS - 2) {
        usb_write_byte(KEYBD_RES_BAD_RANGE);
        return;
    }

    usb_write_byte(KEYBD_RES_DATA);
    usb_write_byte(DISPLAY_WIDTH_CHARS);

    for (uint8_t i = 0; i < DISPLAY_WIDTH_CHARS; i++)
        usb_write_byte(display_exdata[i + (DISPLAY_WIDTH_CHARS * row)]);
}

void usb_write_endcmd(void) {
    usb_write_byte('\r');
    usb_write_byte('\n');
}

void usb_cfg_task(void) {
    Endpoint_SelectEndpoint(CFG_RX_EPADDR);

    if (!Endpoint_IsOUTReceived())
        return;

    uint8_t start = usb_fetch_byte();

    if (start == '\r' || start == '\n')
        return;

    display_update_status(DISPL_STATUS_ACTIVITY, true);

    if (start != '!') {
        usb_write_byte(KEYBD_RES_INVALID_CMD);
        goto skip_cmd;
    }

    uint8_t req = usb_fetch_byte();

    switch (req) {
        case KEYBD_CMD_WRITE_EEPROM:
            usb_handle_cmd_write_eeprom();
            break;
        case KEYBD_CMD_RELOAD_CFG:
            reload_configuration();
            usb_write_byte(KEYBD_RES_OK);
            break;
        case KEYBD_CMD_PREVIEW_RGB:
            usb_handle_cmd_preview_rgb();
            break;
        case KEYBD_CMD_HARD_RESET:
            usb_write_byte(KEYBD_RES_OK);
            wdt_enable(WDTO_500MS);
            break;
        case KEYBD_CMD_WRITE_DISPL:
            usb_handle_cmd_write_displ();
            break;
        case KEYBD_CMD_UPDATE_DISPL:
            display_update();
            usb_write_byte(KEYBD_RES_OK);
            break;
        case KEYBD_CMD_SEL_PROFILE:
            usb_handle_cmd_sel_profile();
            break;
        case KEYBD_CMD_SET_TIME:
            display_set_time(usb_fetch_byte(), usb_fetch_byte());
            usb_write_byte(KEYBD_RES_OK);
            break;
        case KEYBD_CMD_GET_LAST_PRESSED_KEY:
            usb_write_byte(KEYBD_RES_DATA);
            usb_write_byte(2); // size
            usb_write_byte(last_pressed_dmuxid);
            usb_write_byte(last_pressed_muxid);
            break;
        case KEYBD_CMD_GET_KEYMX_PRESSED_KEYS:
            usb_handle_cmd_get_keymx_pressed_keys();
            break;
        case KEYBD_CMD_READ_EEPROM:
            usb_handle_cmd_read_eeprom();
            break;
        case KEYBD_CMD_GET_MAX_CONFIG_VERSION:
            usb_write_byte(KEYBD_RES_DATA);
            usb_write_byte(1); // size
            usb_write_byte(MAX_CONFIG_VERSION);
            break;
        case KEYBD_CMD_READ_DISPL:
            usb_handle_cmd_read_displ();
            break;
        case KEYBD_CMD_GET_PROFILE:
            usb_write_byte(KEYBD_RES_DATA);
            usb_write_byte(1); // size
            usb_write_byte('0' + current_profile);
            break;
        case '!':
            usb_write_byte(KEYBD_RES_OK);
            break;
        default:
            usb_write_byte(KEYBD_RES_INVALID_CMD);
            break;
    }

    skip_cmd:

    usb_write_endcmd();
    Endpoint_SelectEndpoint(CFG_TX_EPADDR);

    bool IsEndpointFull = !Endpoint_IsReadWriteAllowed();

    Endpoint_ClearIN();

    if (IsEndpointFull) {
        if (!usb_wait_in_ready())
            goto activity_off;

        Endpoint_ClearIN();
    }

    if (!usb_wait_in_ready())
        goto activity_off;

    Endpoint_SelectEndpoint(CFG_RX_EPADDR);
    Endpoint_ClearOUT();

    activity_off:
    display_update_status(DISPL_STATUS_ACTIVITY, false);
}
