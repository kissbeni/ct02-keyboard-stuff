
#include "usb.h"
#include "main.h"

#include <avr/pgmspace.h>

const USB_Descriptor_String_t PROGMEM LanguageString = USB_STRING_DESCRIPTOR_ARRAY(LANGUAGE_ID_ENG);
const USB_Descriptor_String_t PROGMEM ManufacturerString = USB_STRING_DESCRIPTOR(L"Kiss Benedek");
const USB_Descriptor_String_t PROGMEM ProductString = USB_STRING_DESCRIPTOR(L"Siemens Nixdorf CT-02");
const USB_Descriptor_String_t PROGMEM ProductString_C = USB_STRING_DESCRIPTOR(L"Siemens Nixdorf CT-02 (compatibility mode)");

const USB_Descriptor_HIDReport_Datatype_t PROGMEM KeyboardReport[] = {
    /* Use the HID class driver's standard Keyboard report.
     *   Max simultaneous keys: 6
     */
    HID_DESCRIPTOR_KEYBOARD(6)
};

// Device descriptor structure
const USB_Descriptor_Device_t PROGMEM DeviceDescriptor = {
    .Header                 = {
        .Size = sizeof(USB_Descriptor_Device_t),
        .Type = DTYPE_Device
    },

    .USBSpecification       = VERSION_BCD(1,1,0),
    .Class                  = CDC_CSCP_CDCClass,
    .SubClass               = CDC_CSCP_NoSpecificSubclass,
    .Protocol               = CDC_CSCP_NoSpecificProtocol,

    .Endpoint0Size          = FIXED_CONTROL_ENDPOINT_SIZE,

    .VendorID               = 0x03EB,
    .ProductID              = 0x2042,
    .ReleaseNumber          = VERSION_BCD(0,1,0),

    .ManufacturerStrIndex   = STRING_ID_MF,
    .ProductStrIndex        = STRING_ID_PROD,
    .SerialNumStrIndex      = USE_INTERNAL_SERIAL,

    .NumberOfConfigurations = FIXED_NUM_CONFIGURATIONS
};

const USB_Descriptor_Device_t PROGMEM DeviceDescriptor_C = {
    .Header                 = {
        .Size = sizeof(USB_Descriptor_Device_t),
        .Type = DTYPE_Device
    },

    .USBSpecification       = VERSION_BCD(1,1,0),
    .Class                  = USB_CSCP_NoDeviceClass,
    .SubClass               = USB_CSCP_NoDeviceSubclass,
    .Protocol               = USB_CSCP_NoDeviceProtocol,

    .Endpoint0Size          = FIXED_CONTROL_ENDPOINT_SIZE,

    .VendorID               = 0x03EB,
    .ProductID              = 0x2042,
    .ReleaseNumber          = VERSION_BCD(0,1,0),

    .ManufacturerStrIndex   = STRING_ID_MF,
    .ProductStrIndex        = STRING_ID_PROD,
    .SerialNumStrIndex      = USE_INTERNAL_SERIAL,

    .NumberOfConfigurations = FIXED_NUM_CONFIGURATIONS
};

// Configuration descriptor structure.
const USB_Descriptor_Configuration_t PROGMEM ConfigurationDescriptor = {
    .Config = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Configuration_Header_t),
            .Type = DTYPE_Configuration
        },

        .TotalConfigurationSize = sizeof(USB_Descriptor_Configuration_t),
        .TotalInterfaces        = 3,

        .ConfigurationNumber    = 1,
        .ConfigurationStrIndex  = NO_DESCRIPTOR,

        .ConfigAttributes       = USB_CONFIG_ATTR_RESERVED,

        .MaxPowerConsumption    = USB_CONFIG_POWER_MA(350)
    },

    // HID keyboard interface
    .HID_Interface = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Interface_t),
            .Type = DTYPE_Interface
        },

        .InterfaceNumber        = INTERFACE_ID_KEYBOARD,
        .AlternateSetting       = 0,

        .TotalEndpoints         = 1,

        .Class                  = HID_CSCP_HIDClass,
        .SubClass               = HID_CSCP_BootSubclass,
        .Protocol               = HID_CSCP_KeyboardBootProtocol,

        .InterfaceStrIndex      = NO_DESCRIPTOR
    },
    .HID_KeyboardHID = {
        .Header                 = {
            .Size = sizeof(USB_HID_Descriptor_HID_t),
            .Type = HID_DTYPE_HID
        },

        .HIDSpec                = VERSION_BCD(1,1,1),
        .CountryCode            = 9, // German
        .TotalReportDescriptors = 1,
        .HIDReportType          = HID_DTYPE_Report,
        .HIDReportLength        = sizeof(KeyboardReport)
    },
    .HID_ReportINEndpoint = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Endpoint_t),
            .Type = DTYPE_Endpoint
        },

        .EndpointAddress        = KEYBOARD_EPADDR,
        .Attributes             = (EP_TYPE_INTERRUPT | ENDPOINT_ATTR_NO_SYNC | ENDPOINT_USAGE_DATA),
        .EndpointSize           = KEYBOARD_EPSIZE,
        .PollingIntervalMS      = 5
    },

    // Emulated serial interface for configuration
    .CDC_CCI_Interface = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Interface_t),
            .Type = DTYPE_Interface
        },

        .InterfaceNumber        = INTERFACE_ID_CDC_CCI,
        .AlternateSetting       = 0,

        .TotalEndpoints         = 1,

        .Class                  = CDC_CSCP_CDCClass,
        .SubClass               = CDC_CSCP_ACMSubclass,
        .Protocol               = CDC_CSCP_ATCommandProtocol,

        .InterfaceStrIndex      = NO_DESCRIPTOR
    },
    .CDC_Functional_Header = {
        .Header                 = {
            .Size = sizeof(USB_CDC_Descriptor_FunctionalHeader_t),
            .Type = CDC_DTYPE_CSInterface
        },
        .Subtype                = CDC_DSUBTYPE_CSInterface_Header,

        .CDCSpecification       = VERSION_BCD(2,0,0),
    },
    .CDC_Functional_ACM = {
        .Header                 = {
            .Size = sizeof(USB_CDC_Descriptor_FunctionalACM_t),
            .Type = CDC_DTYPE_CSInterface
        },

        .Subtype                = CDC_DSUBTYPE_CSInterface_ACM,
        .Capabilities           = 0x00,
    },
    .CDC_Functional_Union = {
        .Header                 = {
            .Size = sizeof(USB_CDC_Descriptor_FunctionalUnion_t),
            .Type = CDC_DTYPE_CSInterface
        },

        .Subtype                = CDC_DSUBTYPE_CSInterface_Union,
        .MasterInterfaceNumber  = INTERFACE_ID_CDC_CCI,
        .SlaveInterfaceNumber   = INTERFACE_ID_CDC_DCI,
    },
    .CDC_NotificationEndpoint = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Endpoint_t),
            .Type = DTYPE_Endpoint
        },

        .EndpointAddress        = CFG_NOTIFY_EPADDR,
        .Attributes             = (EP_TYPE_INTERRUPT | ENDPOINT_ATTR_NO_SYNC | ENDPOINT_USAGE_DATA),
        .EndpointSize           = CFG_NOTIFY_EPSIZE,
        .PollingIntervalMS      = 0xFF
    },
    .CDC_DCI_Interface = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Interface_t),
            .Type = DTYPE_Interface
        },

        .InterfaceNumber        = INTERFACE_ID_CDC_DCI,
        .AlternateSetting       = 0,

        .TotalEndpoints         = 2,

        .Class                  = CDC_CSCP_CDCDataClass,
        .SubClass               = CDC_CSCP_NoDataSubclass,
        .Protocol               = CDC_CSCP_NoDataProtocol,

        .InterfaceStrIndex      = NO_DESCRIPTOR
    },
    .CDC_DataOutEndpoint = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Endpoint_t),
            .Type = DTYPE_Endpoint
        },

        .EndpointAddress        = CFG_RX_EPADDR,
        .Attributes             = (EP_TYPE_BULK | ENDPOINT_ATTR_NO_SYNC | ENDPOINT_USAGE_DATA),
        .EndpointSize           = CFG_EPSIZE,
        .PollingIntervalMS      = 5
    },
    .CDC_DataInEndpoint = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Endpoint_t),
            .Type = DTYPE_Endpoint
        },

        .EndpointAddress        = CFG_TX_EPADDR,
        .Attributes             = (EP_TYPE_BULK | ENDPOINT_ATTR_NO_SYNC | ENDPOINT_USAGE_DATA),
        .EndpointSize           = CFG_EPSIZE,
        .PollingIntervalMS      = 5
    }
};

const USB_Descriptor_Configuration_C_t PROGMEM ConfigurationDescriptor_C = {
    .Config = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Configuration_Header_t),
            .Type = DTYPE_Configuration
        },

        .TotalConfigurationSize = sizeof(USB_Descriptor_Configuration_C_t),
        .TotalInterfaces        = 1,

        .ConfigurationNumber    = 1,
        .ConfigurationStrIndex  = NO_DESCRIPTOR,

        .ConfigAttributes       = USB_CONFIG_ATTR_RESERVED,

        .MaxPowerConsumption    = USB_CONFIG_POWER_MA(350)
    },

    // HID keyboard interface
    .HID_Interface = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Interface_t),
            .Type = DTYPE_Interface
        },

        .InterfaceNumber        = 0,
        .AlternateSetting       = 0,

        .TotalEndpoints         = 1,

        .Class                  = HID_CSCP_HIDClass,
        .SubClass               = HID_CSCP_BootSubclass,
        .Protocol               = HID_CSCP_KeyboardBootProtocol,

        .InterfaceStrIndex      = NO_DESCRIPTOR
    },
    .HID_KeyboardHID = {
        .Header                 = {
            .Size = sizeof(USB_HID_Descriptor_HID_t),
            .Type = HID_DTYPE_HID
        },

        .HIDSpec                = VERSION_BCD(1,1,1),
        .CountryCode            = 9, // German
        .TotalReportDescriptors = 1,
        .HIDReportType          = HID_DTYPE_Report,
        .HIDReportLength        = sizeof(KeyboardReport)
    },
    .HID_ReportINEndpoint = {
        .Header                 = {
            .Size = sizeof(USB_Descriptor_Endpoint_t),
            .Type = DTYPE_Endpoint
        },

        .EndpointAddress        = KEYBOARD_EPADDR,
        .Attributes             = (EP_TYPE_INTERRUPT | ENDPOINT_ATTR_NO_SYNC | ENDPOINT_USAGE_DATA),
        .EndpointSize           = KEYBOARD_EPSIZE,
        .PollingIntervalMS      = 5
    }
};

#if defined(HAS_MULTIPLE_DESCRIPTOR_ADDRESS_SPACES)
uint16_t CALLBACK_USB_GetDescriptor(const uint16_t wValue, const uint16_t wIndex,
    const void** const DescriptorAddress, uint8_t* const DescriptorMemorySpace)
#else
uint16_t CALLBACK_USB_GetDescriptor(const uint16_t wValue, const uint16_t wIndex,
    const void** const DescriptorAddress)
#endif
{
    const uint8_t  DescriptorType   = (wValue >> 8);
    const uint8_t  DescriptorNumber = (wValue & 0xFF);

    const void* Address = NULL;
    uint16_t    Size    = NO_DESCRIPTOR;

    switch (DescriptorType) {
        case DTYPE_Device:
            Address = compatibility_mode ? &DeviceDescriptor_C : &DeviceDescriptor;
            Size    = sizeof(USB_Descriptor_Device_t);
            break;
        case DTYPE_Configuration:
            Address = compatibility_mode ? (const void*)&ConfigurationDescriptor_C : (const void*)&ConfigurationDescriptor;
            Size    = compatibility_mode ? sizeof(USB_Descriptor_Configuration_C_t) : sizeof(USB_Descriptor_Configuration_t);
            break;
        case DTYPE_String:
            switch (DescriptorNumber) {
                case 0x00:
                    Address = &LanguageString;
                    Size    = pgm_read_byte(&LanguageString.Header.Size);
                    break;
                case 0x01:
                    Address = &ManufacturerString;
                    Size    = pgm_read_byte(&ManufacturerString.Header.Size);
                    break;
                case 0x02:
                    Address = compatibility_mode ? &ProductString_C : &ProductString;
                    Size    = pgm_read_byte((compatibility_mode ? ProductString_C : ProductString).Header.Size);
                    break;
            }

            break;
        case HID_DTYPE_HID:
            Address = (compatibility_mode ? &ConfigurationDescriptor_C.HID_KeyboardHID : &ConfigurationDescriptor.HID_KeyboardHID);
            Size    = sizeof(USB_HID_Descriptor_HID_t);
            break;
        case HID_DTYPE_Report:
            Address = &KeyboardReport;
            Size    = sizeof(KeyboardReport);
            break;
    }

    *DescriptorAddress = Address;
    return Size;
}
