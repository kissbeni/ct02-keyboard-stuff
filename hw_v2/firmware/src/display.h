
#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdint.h>
#include <stdbool.h>

#define DISPLAY_ADDR 0x78

#define DISPLAY_HEIGHT 32
#define DISPLAY_WIDTH  128

#define DISPLAY_HEIGHT_CHARS (DISPLAY_HEIGHT / 8)
#define DISPLAY_WIDTH_CHARS (DISPLAY_WIDTH / 8)

#define DISPLAY_BYTE_COUNT ((DISPLAY_WIDTH * DISPLAY_HEIGHT) / 8)

// keys handled by the display module
#define DISPLAY_KEY_RIGHT 0
#define DISPLAY_KEY_LEFT  1

// status items
#define DISPL_STATUS_ACTIVITY 0
#define DISPL_STATUS_BATTERY  1
#define DISPL_STATUS_SECURITY 2
#define DISPL_STATUS_BT       3
#define DISPL_STATUS_LEDS     4
#define DISPL_STATUS_EXTPWR   5
#define DISPL_STATUS_BUTTON   6

// status bar display offsets
#define DISPL_OFFS_ACTIVITY   3
#define DISPL_OFFS_BATTERY    9
#define DISPL_OFFS_SECURITY   5
#define DISPL_OFFS_BT         4
#define DISPL_OFFS_CLED       0
#define DISPL_OFFS_SLED       1
#define DISPL_OFFS_NLED       2
#define DISPL_OFFS_EXTPWR     8
#define DISPL_OFFS_BUTTON     6
#define DISPL_OFFS_TIME       11 // 00:00 format

#define font ((uint8_t*)0x2C00)

extern char display_data[DISPLAY_HEIGHT_CHARS * DISPLAY_WIDTH_CHARS];
extern char display_exdata[(DISPLAY_HEIGHT_CHARS - 2) * DISPLAY_WIDTH_CHARS];

void display_init(void);
void display_clear(void);
void display_set_char_at(uint8_t x, uint8_t y, char ch);
void display_splash(void);
void display_splash_status(uint8_t sc);
void display_enter_text_mode(void);
void display_update(void);
void display_update_profiles(void);
void display_handle_key(uint8_t key);
void display_update_status(uint8_t item, uint8_t value);
void display_tick(void);
void display_wake(void);
void display_set_text_visibility(bool local);
void display_set_time(uint8_t min_bcd, uint8_t hr_bcd);
char num2hex_char(uint8_t n);

#endif
