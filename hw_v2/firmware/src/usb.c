
#include "usb.h"
#include "lighting.h"
#include "storage.h"
#include "display.h"
#include "macro.h"
#include "main.h"

extern uint16_t keys_down[8];
extern bool any_key_down;

extern uint8_t keymap[8][16];

extern uint8_t macro_keys_down[5];
extern uint8_t macro_program_counter;

static uint8_t PrevKeyboardHIDReportBuffer[sizeof(USB_KeyboardReport_Data_t)];

USB_ClassInfo_HID_Device_t Keyboard_HID_Interface = {
    .Config = {
        .InterfaceNumber           = INTERFACE_ID_KEYBOARD,
        .ReportINEndpoint          = {
            .Address           = KEYBOARD_EPADDR,
            .Size              = KEYBOARD_EPSIZE,
            .Banks             = 1,
        },
        .PrevReportINBuffer        = PrevKeyboardHIDReportBuffer,
        .PrevReportINBufferSize    = sizeof(PrevKeyboardHIDReportBuffer)
    }
};

static CDC_LineEncoding_t line_encoding = {
    .BaudRateBPS = 0,
    .CharFormat  = CDC_LINEENCODING_OneStopBit,
    .ParityType  = CDC_PARITY_None,
    .DataBits    = 8
};

void usb_init(void) {
    Keyboard_HID_Interface.Config.InterfaceNumber = compatibility_mode ? 0 : INTERFACE_ID_KEYBOARD;
    USB_Init();
}

void usb_task(void) {
    HID_Device_USBTask(&Keyboard_HID_Interface);
    usb_cfg_task();
    USB_USBTask();
}

void EVENT_USB_Device_Connect(void) {
    set_status_led_bits(LED_FLAGS_TEST);
    reload_configuration();
    set_rgb_enabled(true);
}

void EVENT_USB_Device_Disconnect(void) {
    set_status_led_bits(LED_FLAGS_EMPTY);
    set_rgb_enabled(false);
}

void EVENT_USB_Device_ConfigurationChanged(void) {
    bool ConfigSuccess = true;

    // Setup configuration endpoints
    ConfigSuccess &= Endpoint_ConfigureEndpoint(CFG_NOTIFY_EPADDR, EP_TYPE_INTERRUPT, CFG_NOTIFY_EPSIZE, 1);
    ConfigSuccess &= Endpoint_ConfigureEndpoint(CFG_TX_EPADDR, EP_TYPE_BULK, CFG_EPSIZE, 1);
    ConfigSuccess &= Endpoint_ConfigureEndpoint(CFG_RX_EPADDR, EP_TYPE_BULK, CFG_EPSIZE, 1);

    // Setup keyboard endpoints
    ConfigSuccess &= HID_Device_ConfigureEndpoints(&Keyboard_HID_Interface);

    USB_Device_EnableSOFEvents();

    if (!ConfigSuccess)
        display_splash_status(0x40);

    set_status_led_bits(ConfigSuccess ? LED_FLAGS_EMPTY : LED_FLAGS_CONFIG_FAILED);
}

void EVENT_USB_Device_ControlRequest(void) {
    HID_Device_ProcessControlRequest(&Keyboard_HID_Interface);

    if ((USB_ControlRequest.bmRequestType & (CONTROL_REQTYPE_TYPE | CONTROL_REQTYPE_RECIPIENT)) != (REQTYPE_CLASS | REQREC_INTERFACE))
        return;

    // Handle CDC configuration parameters
    switch (USB_ControlRequest.bRequest) {
        case CDC_REQ_GetLineEncoding:
            if (USB_ControlRequest.bmRequestType == (REQDIR_DEVICETOHOST | REQTYPE_CLASS | REQREC_INTERFACE)) {
                Endpoint_ClearSETUP();
                Endpoint_Write_Control_Stream_LE(&line_encoding, sizeof(CDC_LineEncoding_t));
                Endpoint_ClearOUT();
            }

            break;
        case CDC_REQ_SetLineEncoding:
            if (USB_ControlRequest.bmRequestType == (REQDIR_HOSTTODEVICE | REQTYPE_CLASS | REQREC_INTERFACE)) {
                Endpoint_ClearSETUP();
                Endpoint_Read_Control_Stream_LE(&line_encoding, sizeof(CDC_LineEncoding_t));
                Endpoint_ClearIN();
            }

            break;
    }
}

void EVENT_USB_Device_StartOfFrame(void) {
    HID_Device_MillisecondElapsed(&Keyboard_HID_Interface);
}

void add_report_keycode(uint8_t kc, USB_KeyboardReport_Data_t* KeyboardReport, uint8_t* x) {
    if (!kc || (*x) >= 6)
        return;

    if (kc >= 0xE0 && kc < 0xE8) {
        KeyboardReport->Modifier |= (1 << (kc & 7));
        return;
    }

    KeyboardReport->KeyCode[(*x)++] = kc;

    /*switch (kc) {
        case HID_KEYBOARD_SC_LEFT_CONTROL:
            KeyboardReport->Modifier |= HID_KEYBOARD_MODIFIER_LEFTCTRL;
            break;
        case HID_KEYBOARD_SC_LEFT_SHIFT:
            KeyboardReport->Modifier |= HID_KEYBOARD_MODIFIER_LEFTSHIFT;
            break;
        case HID_KEYBOARD_SC_LEFT_ALT:
            KeyboardReport->Modifier |= HID_KEYBOARD_MODIFIER_LEFTALT;
            break;
        case HID_KEYBOARD_SC_LEFT_GUI:
            KeyboardReport->Modifier |= HID_KEYBOARD_MODIFIER_LEFTGUI;
            break;
        case HID_KEYBOARD_SC_RIGHT_CONTROL:
            KeyboardReport->Modifier |= HID_KEYBOARD_MODIFIER_RIGHTCTRL;
            break;
        case HID_KEYBOARD_SC_RIGHT_SHIFT:
            KeyboardReport->Modifier |= HID_KEYBOARD_MODIFIER_RIGHTSHIFT;
            break;
        case HID_KEYBOARD_SC_RIGHT_ALT:
            KeyboardReport->Modifier |= HID_KEYBOARD_MODIFIER_RIGHTALT;
            break;
        case HID_KEYBOARD_SC_RIGHT_GUI:
            KeyboardReport->Modifier |= HID_KEYBOARD_MODIFIER_RIGHTGUI;
            break;
        default:
            KeyboardReport->KeyCode[(*x)++] = kc;
            break;
    }*/
}

bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
    USB_KeyboardReport_Data_t* KeyboardReport = (USB_KeyboardReport_Data_t*)ReportData;
    uint8_t x = 0;

    process_keys();

    #if ENABLE_MACROES
    if (macro_program_counter < 50) {
        step_macro();
        for (uint8_t i = 0; i < 5; i++)
            add_report_keycode(macro_keys_down[i], KeyboardReport, &x);
    }
    #endif

    if (any_key_down) {
        display_wake();

        for (uint8_t d = 0; d < 8 && x < 6; d++) {
            uint16_t k = keys_down[d];

            if (k == 0)
                continue;

            for (uint8_t m = 0; m < 16; m++) {
                uint8_t kc = keymap[d][m];

                if ((k & ((uint16_t)1 << m))) {
                    if (kc >= 0xF0) {
                        #if ENABLE_MACROES
                        run_macro(kc & 0xF);
                        #endif

                        goto report_full;
                    }

                    add_report_keycode(kc, KeyboardReport, &x);
                }
            }
        }
    }

    report_full:

    *ReportSize = sizeof(USB_KeyboardReport_Data_t);
    return false;
}

void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
    uint8_t  leds   = 0;
    uint8_t* LEDReport = (uint8_t*)ReportData;

    if (*LEDReport & HID_KEYBOARD_LED_NUMLOCK)
        leds |= LED_FLAG_NUMLOCK;

    if (*LEDReport & HID_KEYBOARD_LED_CAPSLOCK)
        leds |= LED_FLAG_CAPSLOCK;

    if (*LEDReport & HID_KEYBOARD_LED_SCROLLLOCK)
        leds |= LED_FLAG_SCROLLLOCK;

    display_update_status(DISPL_STATUS_LEDS, leds);

    set_status_led_bits(leds);
}
