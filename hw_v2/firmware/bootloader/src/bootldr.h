
#ifndef BOOTLDR_H
#define BOOTLDR_H

#define BOOTLOADER_VERSION_MAJOR     0x01
#define BOOTLOADER_VERSION_MINOR     0x02
#define BOOTLOADER_HWVERSION_MAJOR   0x02
#define BOOTLOADER_HWVERSION_MINOR   0x00
#define SOFTWARE_IDENTIFIER          "LUFACDC"
#define MAGIC_BOOT_KEY               0xC702

#define PROTECTED_PAGE_BEGIN ((0x2C00/SPM_PAGESIZE)*SPM_PAGESIZE)

enum AVR109_Memories {
    MEMORY_TYPE_FLASH  = 'F',
    MEMORY_TYPE_EEPROM = 'E',
};

enum AVR109_Commands {
    AVR109_COMMAND_Sync                     = '\x1b',
    AVR109_COMMAND_ReadEEPROM               = 'd',
    AVR109_COMMAND_WriteEEPROM              = 'D',
    AVR109_COMMAND_ReadFLASHWord            = 'R',
    AVR109_COMMAND_WriteFlashPage           = 'm',
    AVR109_COMMAND_FillFlashPageWordLow     = 'c',
    AVR109_COMMAND_FillFlashPageWordHigh    = 'C',
    AVR109_COMMAND_GetBlockWriteSupport     = 'b',
    AVR109_COMMAND_BlockWrite               = 'B',
    AVR109_COMMAND_BlockRead                = 'g',
    AVR109_COMMAND_ReadExtendedFuses        = 'Q',
    AVR109_COMMAND_ReadHighFuses            = 'N',
    AVR109_COMMAND_ReadLowFuses             = 'F',
    AVR109_COMMAND_ReadLockbits             = 'r',
    AVR109_COMMAND_WriteLockbits            = 'l',
    AVR109_COMMAND_EraseFLASH               = 'e',
    AVR109_COMMAND_ReadSignature            = 's',
    AVR109_COMMAND_ReadBootloaderSWVersion  = 'V',
    AVR109_COMMAND_ReadBootloaderHWVersion  = 'v',
    AVR109_COMMAND_ReadBootloaderIdentifier = 'S',
    AVR109_COMMAND_ReadBootloaderInterface  = 'p',
    AVR109_COMMAND_SetCurrentAddress        = 'A',
    AVR109_COMMAND_ReadAutoAddressIncrement = 'a',
    AVR109_COMMAND_ReadPartCode             = 't',
    AVR109_COMMAND_EnterProgrammingMode     = 'P',
    AVR109_COMMAND_LeaveProgrammingMode     = 'L',
    AVR109_COMMAND_SelectDeviceType         = 'T',
    AVR109_COMMAND_SetLED                   = 'x',
    AVR109_COMMAND_ClearLED                 = 'y',
    AVR109_COMMAND_ExitBootloader           = 'E',
};

#endif
