
#include "Descriptors.h"
#include "bootldr.h"
#include "font.h"
#include "softi2c.h"

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/boot.h>
#include <avr/eeprom.h>
#include <avr/power.h>
#include <avr/interrupt.h>

#include <stdbool.h>

#include <LUFA/Drivers/USB/USB.h>

#define DISPLAY_ADDR 0x78

#define DISPLAY_HEIGHT 32
#define DISPLAY_WIDTH  128

#define DISPLAY_HEIGHT_CHARS (DISPLAY_HEIGHT / 8)
#define DISPLAY_WIDTH_CHARS (DISPLAY_WIDTH / 8)

#define DISPLAY_BYTE_COUNT ((DISPLAY_WIDTH * DISPLAY_HEIGHT) / 8)

uint32_t current_address;
bool bootloader_run = true;
uint16_t MagicBootKey ATTR_NO_INIT;

static uint8_t state = 0;
static const PROGMEM char bootldr_str[] = "CT-02 BootloaderVer 1.2 / HW 2.0";
static const PROGMEM char read_str[]    = "Reading flash...                ";
static const PROGMEM char erase_str[]   = "Erasing flash...                ";
static const PROGMEM char write_str[]   = "Writing flash...                ";
static const PROGMEM char exit_str[]    = "Good bye :)     Have a nice day!";

static CDC_LineEncoding_t line_encoding = {
    .BaudRateBPS = 0,
    .CharFormat  = CDC_LINEENCODING_OneStopBit,
    .ParityType  = CDC_PARITY_None,
    .DataBits    = 8
};

void Application_Jump_Check(void) ATTR_INIT_SECTION(3);

static void cdc_task(void);

void display_send_command_list(const uint8_t *c, uint8_t n) {
    i2c_start(DISPLAY_ADDR);
    i2c_write(0x00);
    while (n--) i2c_write(pgm_read_byte(c++));
    i2c_stop();
}

void display_send_command(uint8_t c) {
    i2c_start(DISPLAY_ADDR);
    i2c_write(0x00);
    i2c_write(c);
    i2c_stop();
}

void display_data_begin(void) {
    static const uint8_t PROGMEM cmds[] = {
        0x22,
        0,                  // Page start address
        DISPLAY_HEIGHT / 8, // Page end address

        0x21,
        0,                 // Column start address
        DISPLAY_WIDTH - 1  // Column end address
    };
    display_send_command_list(cmds, sizeof(cmds));

    i2c_start(DISPLAY_ADDR);
    i2c_write(0x40);
}


void display_clear(void) {
    display_data_begin();

    for (uint16_t count = DISPLAY_BYTE_COUNT; count--;)
        i2c_write(0);

    i2c_stop();
}

void display_init(void) {
    i2c_init();

    // initialize display
    static const uint8_t PROGMEM init[] = {
        0xAE, // display off
        0xD5,
        0x80,
        0xA8,
        DISPLAY_HEIGHT - 1,   // height
        0xD3,
        0x00, // set display offset
        0x40, // set startline to 0
        0x8D,
        0x14,
        0x20, 0x00, // addressing mode
        0xA1,
        0xC8,
        0xDA,
        0x02,
        0x81, 0, // set contrast (still visible, but probably less chance of burn-in)
        0xD9,
        0xF1,
        0xDB,
        0x40,
        0xA4, // display all on resume
        0xA6, // normal display
        0x2E, // disable scrolling
        0xAF  // display on
    };
    display_send_command_list(init, sizeof(init));

    display_clear();
}

// void display_write(const char* str) {
//     char ch;
//
//     display_data_begin();
//
//     for (uint16_t i = 0; i < str[i]; i++) {
//         ch = str[i] & 0x7F;
//
//         const uint8_t* baseptr = &font[ch * 8];
//
//         for (uint8_t u = 0; u < 8; u++)
//             i2c_write(pgm_read_byte(baseptr + u));
//     }
//
//     i2c_stop();
// }

void display_write_pgmem(const char* str) {
    char ch;

    display_data_begin();

    for (uint16_t i = 0;;) {
        ch = pgm_read_byte(&str[i]);

        if (!ch)
            break;

        ch &= 0x7F;

        const uint8_t* baseptr = &font[ch * 8];

        for (uint8_t u = 0; u < 8; u++)
            i2c_write(pgm_read_byte(baseptr + u));

        i++;
    }

    i2c_stop();
}

void bootldr_init_io(void) {
    // Every not-connected port is set as an input,
    // to prevent damaging the MCU.
    DDRB = //--+--------+-------------------------------------------
    (0 << PB0) | //  IN | Not-conected
    (0 << PB1) | // OUT | SPI clock (Unused in bootloader mode)
    (0 << PB2) | // OUT | SPI MOSI (Unused in bootloader mode)
    (0 << PB3) | //  IN | SPI MISO (Unused in bootloader mode)
    (0 << PB4) | // OUT | EEPROM CS (Unused in bootloader mode)
    (0 << PB5) | //  IN | I2C SDA
    (0 << PB6) | //  IN | I2C SCL
    (0 << PB7) ; // OUT | RGB strip data (Unused in bootloader mode)
    DDRC = //--+--------+-------------------------------------------
    (0 << PC2) | //  IN | Power button
    (1 << PC4) | // OUT | Shift-register (for leds) clock
    (0 << PC5) | // OUT | Shift-register (for leds) data
                 //     | (Only set as an output when used, because
                 //     | this is indirectly connected to a part of
                 //     | the keyboard mx)
    (0 << PC6) | //  IN | Reserved
    (0 << PC7) ; //  IN | Not-conected
    // --------+--------+-------------------------------------------

    PORTC |= (1 << PC2); // Pullup for the power button
}

void bootloader_main(void) {
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    clock_prescale_set(clock_div_1);

    MCUCR = (1 << IVCE);
    MCUCR = (1 << IVSEL);

    display_init();
    display_write_pgmem(bootldr_str);

    USB_Init();
    GlobalInterruptEnable();

    while (bootloader_run) {
        cdc_task();
        USB_USBTask();
    }

    display_write_pgmem(exit_str);

    USB_Detach();
    MagicBootKey = MAGIC_BOOT_KEY;

    // enable watchdog for reseting
    wdt_enable(WDTO_250MS);

    for (;;);
}

static void jump_to_main(void) {
    // disable watchdog
    MCUSR &= ~(1<<WDRF);
    wdt_disable();

    MagicBootKey = 0;

    ((void (*)(void))0x0000)();
}

void Application_Jump_Check(void) {
    if ((MCUSR & (1 << WDRF)) && (MagicBootKey == MAGIC_BOOT_KEY))
        jump_to_main();
}

int main(int argc, char const *argv[]) {
    bootldr_init_io();

    // jump to bootloader if the power button is held down during powerup
    // and skip when we're reseting from the bootloader
    if (!(PINC & (1 << PC2)) && (!(MCUSR & (1 << WDRF)) || MagicBootKey != MAGIC_BOOT_KEY))
        bootloader_main();

    jump_to_main();
    return 0;
}

void EVENT_USB_Device_ConfigurationChanged(void) {
    // Setup CDC endpoints

    Endpoint_ConfigureEndpoint(CDC_NOTIFICATION_EPADDR, EP_TYPE_INTERRUPT,
                               CDC_NOTIFICATION_EPSIZE, 1);

    Endpoint_ConfigureEndpoint(CDC_TX_EPADDR, EP_TYPE_BULK, CDC_TXRX_EPSIZE, 1);
    Endpoint_ConfigureEndpoint(CDC_RX_EPADDR, EP_TYPE_BULK, CDC_TXRX_EPSIZE, 1);
}

void EVENT_USB_Device_ControlRequest(void) {
    // ignore non-CDC requests
    if ((USB_ControlRequest.bmRequestType & (CONTROL_REQTYPE_TYPE | CONTROL_REQTYPE_RECIPIENT)) != (REQTYPE_CLASS | REQREC_INTERFACE))
        return;

    Endpoint_ClearSETUP();

    switch (USB_ControlRequest.bRequest) {
        case CDC_REQ_GetLineEncoding:
            if (USB_ControlRequest.bmRequestType & REQDIR_DEVICETOHOST) {
                Endpoint_Write_Control_Stream_LE(&line_encoding, sizeof(CDC_LineEncoding_t));
                Endpoint_ClearOUT();
            }

            break;
        case CDC_REQ_SetLineEncoding:
            if (USB_ControlRequest.bmRequestType & REQDIR_HOSTTODEVICE) {
                Endpoint_Read_Control_Stream_LE(&line_encoding, sizeof(CDC_LineEncoding_t));
                Endpoint_ClearIN();
            }

            break;
    }
}

static uint8_t read_next_cmd_byte(void) {
    Endpoint_SelectEndpoint(CDC_RX_EPADDR);

    while (!Endpoint_IsReadWriteAllowed()) {
        Endpoint_ClearOUT();

        while (!Endpoint_IsOUTReceived())
            if (USB_DeviceState == DEVICE_STATE_Unattached)
                return 0;
    }

    return Endpoint_Read_8();
}

static void write_response(const uint8_t Response) {
    Endpoint_SelectEndpoint(CDC_TX_EPADDR);

    if (!Endpoint_IsReadWriteAllowed()) {
        Endpoint_ClearIN();

        while (!Endpoint_IsINReady())
            if (USB_DeviceState == DEVICE_STATE_Unattached)
                return;
    }

    Endpoint_Write_8(Response);
}

static void wait_for_page_io(void) {
    boot_spm_busy_wait();
    boot_rww_enable();
}

static void handle_block_rw(const uint8_t cmd) {
    uint16_t bs;
    uint8_t hi = false, lo = 0;

    bs  = (read_next_cmd_byte() << 8);
    bs |=  read_next_cmd_byte();

    if (read_next_cmd_byte() != MEMORY_TYPE_FLASH) {
        write_response('?');

        return;
    }

    if (cmd == AVR109_COMMAND_BlockRead) {
        if (state != 1) {
            display_write_pgmem(read_str);
            state = 1;
        }
        _delay_ms(5);
        boot_rww_enable();

        while (bs--) {
            write_response(pgm_read_byte(current_address | hi));

            if (hi) current_address += 2;

            hi = !hi;
        }

    } else {
        if (state != 2) {
            display_write_pgmem(write_str);
            state = 2;
        }
        uint32_t page_start = current_address;

        if (page_start >= PROTECTED_PAGE_BEGIN) {
            write_response('\r');
            return;
        }

        boot_page_erase_safe(page_start);
        wait_for_page_io();

        while (bs--) {
            if (hi) {
                boot_page_fill(current_address, (read_next_cmd_byte() << 8) | lo);

                current_address += 2;
            } else {
                lo = read_next_cmd_byte();
            }

            hi = !hi;
        }

        boot_page_write_safe(page_start);
        wait_for_page_io();

        write_response('\r');
    }
}

static void cdc_task(void) {
    Endpoint_SelectEndpoint(CDC_RX_EPADDR);

    if (!Endpoint_IsOUTReceived())
        return;

    uint8_t cmd = read_next_cmd_byte();

    switch (cmd) {
        case AVR109_COMMAND_ExitBootloader:
            bootloader_run = false;
            goto withcr;
        case AVR109_COMMAND_SetLED:
        case AVR109_COMMAND_ClearLED:
        case AVR109_COMMAND_SelectDeviceType:
            read_next_cmd_byte();
            goto withcr;
        case AVR109_COMMAND_EnterProgrammingMode:
        case AVR109_COMMAND_LeaveProgrammingMode:
            goto withcr;
        case AVR109_COMMAND_ReadPartCode:
            write_response(0x44);
            write_response(0x00);
            break;
        case AVR109_COMMAND_ReadAutoAddressIncrement:
            write_response('Y');
            break;
        case AVR109_COMMAND_SetCurrentAddress:
            current_address   = (read_next_cmd_byte() << 9);
            current_address  |= (read_next_cmd_byte() << 1);

            goto withcr;
        case AVR109_COMMAND_ReadBootloaderInterface:
            write_response('S');
            break;
        case AVR109_COMMAND_ReadBootloaderIdentifier:
            for (uint8_t CurrByte = 0; CurrByte < 7; CurrByte++)
                write_response(SOFTWARE_IDENTIFIER[CurrByte]);
            break;
        case AVR109_COMMAND_ReadBootloaderSWVersion:
            write_response('0' + BOOTLOADER_VERSION_MAJOR);
            write_response('0' + BOOTLOADER_VERSION_MINOR);
            break;
        case AVR109_COMMAND_ReadSignature:
            write_response(AVR_SIGNATURE_3);
            write_response(AVR_SIGNATURE_2);
            write_response(AVR_SIGNATURE_1);
            break;
        case AVR109_COMMAND_EraseFLASH:
            display_write_pgmem(erase_str);
            boot_rww_enable();

            for (uint32_t CurrFlashAddress = 0; CurrFlashAddress < (uint32_t)PROTECTED_PAGE_BEGIN; CurrFlashAddress += SPM_PAGESIZE) {
                boot_page_erase_safe(CurrFlashAddress);
                wait_for_page_io();
            }

            display_write_pgmem(bootldr_str);
            goto withcr;
        case AVR109_COMMAND_ReadLockbits:
            write_response(boot_lock_fuse_bits_get(GET_LOCK_BITS));
            break;
        case AVR109_COMMAND_ReadLowFuses:
            write_response(boot_lock_fuse_bits_get(GET_LOW_FUSE_BITS));
            break;
        case AVR109_COMMAND_ReadHighFuses:
            write_response(boot_lock_fuse_bits_get(GET_HIGH_FUSE_BITS));
            break;
        case AVR109_COMMAND_ReadExtendedFuses:
            write_response(boot_lock_fuse_bits_get(GET_EXTENDED_FUSE_BITS));
            break;
        case AVR109_COMMAND_GetBlockWriteSupport:
            write_response('Y');
            write_response(SPM_PAGESIZE >> 8);
            write_response(SPM_PAGESIZE & 0xFF);
            break;
        case AVR109_COMMAND_BlockWrite:
        case AVR109_COMMAND_BlockRead:
            handle_block_rw(cmd);
            break;
        /*case AVR109_COMMAND_FillFlashPageWordHigh:
            if (current_address < PROTECTED_PAGE_BEGIN)
                boot_page_fill(current_address, read_next_cmd_byte());
            goto withcr;
        case AVR109_COMMAND_FillFlashPageWordLow:
            if (current_address < PROTECTED_PAGE_BEGIN)
                boot_page_fill(current_address | 0x01, read_next_cmd_byte());
            current_address += 2;
            goto withcr;
        case AVR109_COMMAND_WriteFlashPage:
            if (current_address < PROTECTED_PAGE_BEGIN) {
                boot_page_write(current_address);
                boot_spm_busy_wait();
            }
            goto withcr;
        case AVR109_COMMAND_ReadFLASHWord: {
            uint16_t ProgramWord = pgm_read_word(current_address);
            write_response(ProgramWord >> 8);
            write_response(ProgramWord & 0xFF);
            break;
        }*/
        case AVR109_COMMAND_Sync:
            break;
        default:
            write_response('?');
            break;
    }

    goto nocr;
    withcr:
    write_response('\r');
    nocr:

    Endpoint_SelectEndpoint(CDC_TX_EPADDR);

    bool IsEndpointFull = !Endpoint_IsReadWriteAllowed();

    Endpoint_ClearIN();

    if (IsEndpointFull) {
        while (!Endpoint_IsINReady())
            if (USB_DeviceState == DEVICE_STATE_Unattached)
                return;

        Endpoint_ClearIN();
    }

    while (!Endpoint_IsINReady())
        if (USB_DeviceState == DEVICE_STATE_Unattached)
            return;

    Endpoint_SelectEndpoint(CDC_RX_EPADDR);
    Endpoint_ClearOUT();
}
