
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include "ctapi.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#define VID 0x03EB
#define PID 0x2042

#define FNV_32_INIT 0x811c9dc5
#define FNV_32_PRIME 0x01000193

#define KEYBD_CMD_WRITE_EEPROM              'w'
#define KEYBD_CMD_RELOAD_CFG                'R'
#define KEYBD_CMD_PREVIEW_RGB               'p'
#define KEYBD_CMD_HARD_RESET                'b'
#define KEYBD_CMD_WRITE_DISPL               'd'
#define KEYBD_CMD_UPDATE_DISPL              'u'
#define KEYBD_CMD_SEL_PROFILE               's'
#define KEYBD_CMD_SET_TIME                  't'

#define KEYBD_CMD_GET_LAST_PRESSED_KEY      'l'
#define KEYBD_CMD_GET_KEYMX_PRESSED_KEYS    'm'
#define KEYBD_CMD_READ_EEPROM               'r'
#define KEYBD_CMD_GET_MAX_CONFIG_VERSION    'V'
#define KEYBD_CMD_READ_DISPL                'D'
#define KEYBD_CMD_GET_PROFILE               'P'

#define KEYBD_CMD_BEGIN       '!'

#define KEYBD_RES_INVALID_CMD '?'
#define KEYBD_RES_OK          '.'
#define KEYBD_RES_DATA        '+'
#define KEYBD_RES_BAD_RANGE   '$'
#define KEYBD_RES_BAD_DATA    '/'
#define KEYBD_RES_GENERIC_ERR '#'

#define PACKET_SIZE 16
#define CFG_EPSIZE 32
#define INTERFACE_ID 2

#define CFG_RX_EPADDR (LIBUSB_ENDPOINT_OUT | 4)
#define CFG_TX_EPADDR (LIBUSB_ENDPOINT_IN  | 3)

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

static struct libusb_context* context = NULL;
static bool show_progress_bars = false;

static int ctapi_do_transfer(ctapi_device_t* dev, uint8_t buff[CFG_EPSIZE], uint16_t tx_size) {
    int res;

    res = libusb_bulk_transfer(
        dev->usb_handle,    // handle
        CFG_RX_EPADDR,      // endpoint
        buff,               // transfer buffer
        tx_size,            // size
        NULL,               // bytes transfered (out)
        5000                // timeout
    );

    usleep(2000);
    memset(buff, 0, CFG_EPSIZE);

    res = libusb_bulk_transfer(
        dev->usb_handle,    // handle
        CFG_TX_EPADDR,      // endpoint
        buff,               // transfer buffer
        CFG_EPSIZE,         // size
        NULL,               // bytes transfered (out)
        5000                // timeout
    );

    return res;
}

uint32_t fnv(const uint8_t* data, int len) {
    int rv = FNV_32_INIT;

    for (int i = 0; i < len; i++) {
        rv ^= data[i];
        rv *= FNV_32_PRIME;
    }

    return rv;
}

uint64_t libusb_device_unique_id(libusb_device* dev) {
    uint8_t ports[16];

    int res = libusb_get_port_numbers(dev, ports, 16);

    uint32_t hash = 0;

    if (res >= 0)
        hash = fnv(ports, res);

    return
        (libusb_get_bus_number(dev)) |
        (libusb_get_bus_number(dev) << 8) |
        (libusb_get_device_address(dev) << 16) |
        (hash << 24);
}

void show_progress(int current, int max) {
    if (!show_progress_bars) return;

    int percent = (current/(float)max)*100;
    int n = (current/(float)max)*70;
    printf("[");
    for (int i = 0; i < 70; i++) {
        if (i <= n) printf("#");
        else        printf(".");
    }
    printf("] %3d%%\r", percent);
    fflush(stdout);
}

void finish_progress() {
    if (show_progress_bars) puts("");
}

void ctapi_logging_option(int opt, int val) {
    switch (opt) {
        case 1:
            show_progress_bars = !!val;
            break;
        default:
            break;
    }
}

bool ctapi_any_device_present(void) {
    bool res = false;
    libusb_device** list = NULL;
    ssize_t count = 0;

    count = libusb_get_device_list(context, &list);
    assert(count > 0);

    for (size_t idx = 0; idx < count; ++idx) {
        libusb_device* device = list[idx];
        struct libusb_device_descriptor desc = {0};

        if (libusb_get_device_descriptor(device, &desc) == 0) {
            if (desc.idVendor == VID && desc.idProduct == PID) {
                res = true;
                break;
            }
        }
    }

    libusb_free_device_list(list, count);

    return res;
}

ctapi_device_t* ctapi_first_device(void) {
    struct libusb_device_handle* handle;

    if ((handle = libusb_open_device_with_vid_pid(context, VID, PID)) == NULL)
        return NULL;

    libusb_set_auto_detach_kernel_driver(handle, true);

    ctapi_device_t* res = (ctapi_device_t*)malloc(sizeof(ctapi_device_t));

    res->usb_handle = handle;
    res->uid = libusb_device_unique_id(libusb_get_device(handle));
    res->interface_claimed = false;

    return res;
}

void ctapi_destroy_device(ctapi_device_t* dev) {
    if (!dev) return;

    if (dev->interface_claimed)
        libusb_release_interface(dev->usb_handle, INTERFACE_ID);

    if (dev->usb_handle)
        libusb_close(dev->usb_handle);

    free(dev);
}

bool ctapi_ping(ctapi_device_t* dev) {
    if (!dev) return false;

    if (!dev->interface_claimed)
        return false;

    if (!dev->usb_handle)
        return false;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_BEGIN;

    ctapi_do_transfer(dev, buff, 2);

    return buff[0] == KEYBD_RES_OK;
}

bool ctapi_open(ctapi_device_t* dev) {
    if (!dev || !dev->usb_handle)
        return false;

    if (dev->interface_claimed)
        return true;

    return dev->interface_claimed = (libusb_claim_interface(dev->usb_handle, INTERFACE_ID) == LIBUSB_SUCCESS);
}

bool ctapi_close(ctapi_device_t* dev) {
    if (!dev || !dev->usb_handle)
        return false;

    if (!dev->interface_claimed)
        return true;

    dev->interface_claimed = false;
    return (libusb_release_interface(dev->usb_handle, INTERFACE_ID) == LIBUSB_SUCCESS);
}

int ctapi_read_eeprom_raw(ctapi_device_t* dev, void* buffer, uint8_t size, uint16_t addr) {
    // printf("ctapi_read_eeprom_raw(%p, %p, %d, %d)\n", dev, buffer, size, addr);
    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    if (size > PACKET_SIZE)
        return -2;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_READ_EEPROM;
    buff[2] = (size << 3) | (addr & 0x700) >> 8;
    buff[3] = addr & 0xff;

    int res = ctapi_do_transfer(dev, buff, 4);

    if (res < 0)
        return res;

    if (buff[0] != KEYBD_RES_DATA)
        return -buff[0];

    memcpy(buffer, buff + 2, buff[1]);
    return buff[1];
}

int ctapi_write_eeprom_raw(ctapi_device_t* dev, const void* buffer, uint8_t size, uint16_t addr) {
    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    if (size > PACKET_SIZE)
        return -2;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_WRITE_EEPROM;
    buff[2] = (size << 3) | (addr & 0x700) >> 8;
    buff[3] = addr & 0xff;

    memcpy(buff + 4, buffer, size);

    int res = ctapi_do_transfer(dev, buff, 4 + size);

    if (res < 0)
        return res;

    if (buff[0] != KEYBD_RES_OK)
        return -buff[0];

    return size;
}

int ctapi_write_eeprom(ctapi_device_t* dev, const void* buffer, int size, uint16_t addr) {
    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    int packet_length, res, offset = 0;
    uint8_t verify_buffer[PACKET_SIZE];

    bool retry = true;

    size = MIN(size, 2048 - addr);
    int initial_size = size;

    while (size > 0) {
        show_progress(offset, initial_size);
        packet_length = MIN(PACKET_SIZE, size);

        do_retry:

        if (ctapi_write_eeprom_raw(dev, buffer + offset, packet_length, addr) != packet_length) {
            finish_progress();
            return -2;
        }

        if (ctapi_read_eeprom_raw(dev, verify_buffer, packet_length, addr) != packet_length) {
            finish_progress();
            return -3;
        }

        if ((res = memcmp(buffer + offset, verify_buffer, packet_length)) != 0) {
            if (retry) {
                retry = false;
                goto do_retry;
            }

            finish_progress();
            return -4;
        }

        usleep(50000);

        retry = true;
        size -= packet_length;
        addr += packet_length;
        offset += packet_length;
    }

    finish_progress();
    return offset;
}

int ctapi_read_eeprom(ctapi_device_t* dev, void* buffer, int size, uint16_t addr) {
    //printf("ctapi_read_eeprom(%p, %p, %d, %d)\n", dev, buffer, size, addr);
    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    int packet_length, res, offset = 0;

    size = MIN(size, 2048 - addr);
    int initial_size = size;

    while (size > 0) {
        show_progress(offset, initial_size);
        packet_length = MIN(PACKET_SIZE, size);

        if (ctapi_read_eeprom_raw(dev, buffer + offset, packet_length, addr) != packet_length) {
            finish_progress();
            return -2;
        }

        size -= packet_length;
        addr += packet_length;
        offset += packet_length;
    }

    finish_progress();
    return offset;
}

int ctapi_reload_config(ctapi_device_t* dev) {
    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_RELOAD_CFG;

    int res = ctapi_do_transfer(dev, buff, 2);

    if (res == 0)
        return buff[0] == KEYBD_RES_OK ? 0 : -buff[0];

    return res;
}

int ctapi_get_lastkey(ctapi_device_t* dev) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];

    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_GET_LAST_PRESSED_KEY;

    res = ctapi_do_transfer(dev, buff, 2);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_DATA && buff[1] >= sizeof(uint16_t))
            return *((uint16_t*)&buff[2]);
        else
            return -buff[0];
    }

    return res;
}

int ctapi_get_matrix(ctapi_device_t* dev, uint8_t* buffer, int size) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    if (size <= 0)
        return -2;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_GET_KEYMX_PRESSED_KEYS;

    res = ctapi_do_transfer(dev, buff, 2);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_DATA && buff[1] >= sizeof(uint8_t) * 16) {
            memcpy(buffer, &buff[2], MIN(16, size));
            return MIN(16, size);
        }

        return -buff[0];
    }

    return res;
}

int ctapi_get_version(ctapi_device_t* dev) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_GET_MAX_CONFIG_VERSION;

    res = ctapi_do_transfer(dev, buff, 2);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_DATA && buff[1] >= 1)
            return buff[2];

        return -buff[0];
    }

    return res;
}

int ctapi_preview_rgb(ctapi_device_t* dev, rgb_config_t* config) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_PREVIEW_RGB;

    uint32_t hash = FNV_32_INIT;
    for (uint8_t i = 0; i < sizeof(rgb_config_t); i++) {
        uint8_t b = ((uint8_t*)config)[i];
        buff[2 + i] = b;
        hash = (hash ^ b) * FNV_32_PRIME;
    }

    memcpy(buff + 2 + sizeof(rgb_config_t), &hash, 4);

    printf("SEND: ");
    for (int i = 0; i < 2 + sizeof(rgb_config_t) + 4; i++)
        printf("%02x ", buff[i]);
    puts("");
    fflush(stdout);

    res = ctapi_do_transfer(dev, buff, 2 + sizeof(rgb_config_t) + 4);

    printf("RECV: ");
    for (int i = 0; i < 2 + sizeof(rgb_config_t) + 4; i++)
        printf("%02x ", buff[i]);
    puts("");
    fflush(stdout);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_OK)
            return 0;

        return -buff[0];
    }

    return res;
}

int ctapi_preview_rgb_construct(ctapi_device_t* dev,
    uint8_t anim_type,
    uint8_t brightness,
    uint8_t speed,
    uint8_t r,
    uint8_t g,
    uint8_t b) {

    rgb_config_t cfg = {
        .anim_type = anim_type,
        .brightness = brightness,
        .speed = speed,
        .color = {
            .r = r,
            .g = g,
            .b = b
        }
    };

    return ctapi_preview_rgb(dev, &cfg);
}


int ctapi_hard_reset(ctapi_device_t* dev) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_HARD_RESET;

    res = ctapi_do_transfer(dev, buff, 2);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_OK)
            return 0;

        return -buff[0];
    }

    return res;
}

int ctapi_write_display(ctapi_device_t* dev, int row, const uint8_t* data) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_WRITE_DISPL;
    buff[2] = row + '0';

    memcpy(buff + 3, data, 16);

    res = ctapi_do_transfer(dev, buff, 3 + 16);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_OK)
            return 0;

        return -buff[0];
    }

    return res;
}

int ctapi_update_display(ctapi_device_t* dev) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_UPDATE_DISPL;

    res = ctapi_do_transfer(dev, buff, 2);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_OK)
            return 0;

        return -buff[0];
    }

    return res;
}

int ctapi_select_profile(ctapi_device_t* dev, int id) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_SEL_PROFILE;
    buff[2] = id + '0';

    res = ctapi_do_transfer(dev, buff, 3);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_OK)
            return 0;

        return -buff[0];
    }

    return res;
}

int ctapi_set_time(ctapi_device_t* dev, int h, int m) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_SET_TIME;
    buff[2] = (h / 10) << 4 | (h % 10);
    buff[3] = (m / 10) << 4 | (m % 10);

    res = ctapi_do_transfer(dev, buff, 4);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_OK)
            return 0;

        return -buff[0];
    }

    return res;
}

int ctapi_read_display(ctapi_device_t* dev, int row, uint8_t* buffer) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_READ_DISPL;
    buff[2] = row + '0';

    res = ctapi_do_transfer(dev, buff, 3);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_DATA && buff[1] >= 16) {
            memcpy(buffer, &buff[2], 16);
            return buff[1];
        }

        return -buff[0];
    }

    return res;
}

int ctapi_get_profile(ctapi_device_t* dev) {
    int res;

    if (!dev || !dev->usb_handle || !dev->interface_claimed)
        return -1;

    uint8_t buff[CFG_EPSIZE];
    buff[0] = KEYBD_CMD_BEGIN;
    buff[1] = KEYBD_CMD_GET_PROFILE;

    res = ctapi_do_transfer(dev, buff, 2);

    if (res == 0) {
        if (buff[0] == KEYBD_RES_DATA && buff[1] >= 1)
            return buff[2] - '0';

        return -buff[0];
    }

    return res;
}



__attribute__((constructor)) static void init_libusb_context() {
    assert(libusb_init(&context) == 0);
}

__attribute__((destructor)) static void deinit_libusb_context() {
    libusb_exit(context);
}
