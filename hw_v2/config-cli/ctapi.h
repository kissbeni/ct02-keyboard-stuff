
#ifndef CTAPI_H
#define CTAPI_H

#include <stdint.h>
#include <stdbool.h>

#include <usb.h>
#include <libusb.h>

#ifdef __GNUC__
    #define EXPORT __attribute__((visibility("default")))
#else
    #define EXPORT
    #pragma error Unknown compiler, please use GCC
#endif

#define CTAPI_LOGOPT_PROGRESS_BARS    1
#define CTAPI_LOGOPT_COMMAND_HEXDUMPS 2

typedef struct ctapi_device {
    struct libusb_device_handle* usb_handle;
    uint64_t uid;
    bool interface_claimed;
} ctapi_device_t;

typedef struct rgb_config {
    uint8_t anim_type;
    uint8_t brightness;
    uint8_t speed;
    struct {
        uint8_t r, g, b;
    } color;
} rgb_config_t;

EXPORT void ctapi_logging_option(int opt, int val);

EXPORT bool ctapi_any_device_present(void);

EXPORT ctapi_device_t* ctapi_first_device(void);
EXPORT void ctapi_destroy_device(ctapi_device_t* dev);

EXPORT bool ctapi_ping(ctapi_device_t* dev);

EXPORT bool ctapi_open(ctapi_device_t* dev);
EXPORT bool ctapi_close(ctapi_device_t* dev);

EXPORT int ctapi_write_eeprom(ctapi_device_t* dev, const void* buffer, int size, uint16_t addr);
EXPORT int ctapi_read_eeprom(ctapi_device_t* dev, void* buffer, int size, uint16_t addr);

EXPORT int ctapi_reload_config(ctapi_device_t* dev);
EXPORT int ctapi_get_lastkey(ctapi_device_t* dev);
EXPORT int ctapi_get_matrix(ctapi_device_t* dev, uint8_t* buffer, int size);
EXPORT int ctapi_get_version(ctapi_device_t* dev);

EXPORT int ctapi_preview_rgb(ctapi_device_t* dev, rgb_config_t* config);

EXPORT int ctapi_preview_rgb_construct(ctapi_device_t* dev,
    uint8_t anim_type,
    uint8_t brightness,
    uint8_t speed,
    uint8_t r,
    uint8_t g,
    uint8_t b
);

EXPORT int ctapi_hard_reset(ctapi_device_t* dev);
EXPORT int ctapi_write_display(ctapi_device_t* dev, int row, const uint8_t* data);
EXPORT int ctapi_update_display(ctapi_device_t* dev);
EXPORT int ctapi_select_profile(ctapi_device_t* dev, int id);
EXPORT int ctapi_set_time(ctapi_device_t* dev, int h, int m);
EXPORT int ctapi_read_display(ctapi_device_t* dev, int row, uint8_t* buffer);
EXPORT int ctapi_get_profile(ctapi_device_t* dev);

#endif
