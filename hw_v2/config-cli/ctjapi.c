
#include "ctapi.h"
#include "keyboard_device_usb_CTAPIWrapper.h"
#include "keyboard_device_usb_CTAPIDeviceWrapper.h"

#include <stddef.h>
#include <string.h>

// -------------------------------------------------------------------------- //

/*
 * Class:     keyboard_device_usb_CTAPIWrapper
 * Method:    ctapi_any_device_present
 * Signature: ()Z
 */
jboolean Java_keyboard_device_usb_CTAPIWrapper_ctapi_1any_1device_1present(JNIEnv* env, jclass cls) {
    return ctapi_any_device_present();
}

/*
 * Class:     keyboard_device_usb_CTAPIWrapper
 * Method:    ctapi_first_device
 * Signature: ()Lkeyboard/device/usb/CTAPIDeviceWrapper;
 */
jobject Java_keyboard_device_usb_CTAPIWrapper_ctapi_1first_1device(JNIEnv* env, jclass cls) {
    ctapi_device_t* dev = ctapi_first_device();

    if (!dev)
        return NULL;

    jclass device = (*env)->FindClass(env, "keyboard/device/usb/CTAPIDeviceWrapper");
    jmethodID constructor = (*env)->GetMethodID(env, device, "<init>", "(J)V");
    return (*env)->NewObject(env, device, constructor, *((ptrdiff_t*)&dev));
}

// -------------------------------------------------------------------------- //

ctapi_device_t* jobj2ptr(JNIEnv* env, jobject self) {
    jclass cls = (*env)->GetObjectClass(env, self);
    jfieldID field = (*env)->GetFieldID(env, cls, "obj_ptr", "J");
    ptrdiff_t ptr = (*env)->GetLongField(env, self, field);

    if (!ptr)
        return NULL;

    return (ctapi_device_t*)ptr;
}

// -------------------------------------------------------------------------- //

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_destroy_device
 * Signature: ()V
 */
void Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1destroy_1device(JNIEnv* env, jobject self) {
    ctapi_destroy_device(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_ping
 * Signature: ()Z
 */
jboolean Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1ping(JNIEnv* env, jobject self) {
    return ctapi_ping(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_open
 * Signature: ()Z
 */
jboolean Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1open(JNIEnv* env, jobject self) {
    return ctapi_open(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_close
 * Signature: ()Z
 */
jboolean Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1close(JNIEnv* env, jobject self) {
    return ctapi_close(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_write_eeprom
 * Signature: ([BS)I
 */
jint Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1write_1eeprom(JNIEnv* env, jobject self, jbyteArray data, jshort addr) {
    jboolean isCopy;

    ctapi_device_t* ptr = jobj2ptr(env, self);
    jsize s = (*env)->GetArrayLength(env, data);
    jbyte* b = (*env)->GetByteArrayElements(env, data, &isCopy);

    int res = ctapi_write_eeprom(ptr, b, s, addr);

    if (isCopy)
        (*env)->ReleaseByteArrayElements(env, data, b, 0);

    return res;
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_read_eeprom
 * Signature: ([BS)I
 */
jint Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1read_1eeprom(JNIEnv* env, jobject self, jbyteArray data, jshort addr) {
    jboolean isCopy;

    ctapi_device_t* ptr = jobj2ptr(env, self);
    jsize s = (*env)->GetArrayLength(env, data);
    jbyte* read_data = malloc(s);

    if (!read_data)
        return -5;

    int res = ctapi_read_eeprom(ptr, read_data, s, addr);

    if (res > 0)
        (*env)->SetByteArrayRegion(env, data, 0, res, read_data);

    free(read_data);
    return res;
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_reload_config
 * Signature: ()I
 */
jint Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1reload_1config(JNIEnv* env, jobject self) {
    return ctapi_reload_config(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_get_lastkey
 * Signature: ()I
 */
jint Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1get_1lastkey(JNIEnv* env, jobject self) {
    return ctapi_get_lastkey(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_get_matrix
 * Signature: ([B)I
 */
jint Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1get_1matrix(JNIEnv* env, jobject self, jbyteArray data) {
    jboolean isCopy;

    ctapi_device_t* ptr = jobj2ptr(env, self);
    jsize s = (*env)->GetArrayLength(env, data);
    jbyte* read_data = malloc(16);

    if (!read_data)
        return -5;

    int res = ctapi_get_matrix(ptr, (uint8_t*)read_data, s);

    if (res > 0)
        (*env)->SetByteArrayRegion(env, data, 0, res, read_data);
    else free(read_data);

    return res;
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_get_version
 * Signature: ()I
 */
jint Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1get_1version(JNIEnv* env, jobject self) {
    return ctapi_get_version(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_preview_rgb
 * Signature: (Lkeyboard/device/usb/CTAPIRGBConfig;)I
 */
jint Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1preview_1rgb(JNIEnv* env, jobject self, jobject config) {
    jfieldID field;
    uint8_t anim_type;
    uint8_t brightness;
    uint8_t speed;
    uint8_t r;
    uint8_t g;
    uint8_t b;

    jclass cls = (*env)->GetObjectClass(env, config);

    field = (*env)->GetFieldID(env, cls, "anim_type", "B");
    anim_type = (*env)->GetByteField(env, config, field);

    field = (*env)->GetFieldID(env, cls, "brightness", "B");
    brightness = (*env)->GetByteField(env, config, field);

    field = (*env)->GetFieldID(env, cls, "speed", "B");
    speed = (*env)->GetByteField(env, config, field);

    field = (*env)->GetFieldID(env, cls, "r", "B");
    r = (*env)->GetByteField(env, config, field);

    field = (*env)->GetFieldID(env, cls, "g", "B");
    g = (*env)->GetByteField(env, config, field);

    field = (*env)->GetFieldID(env, cls, "b", "B");
    b = (*env)->GetByteField(env, config, field);

    return ctapi_preview_rgb_construct(jobj2ptr(env, self), anim_type, brightness, speed, r, g, b);
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_hard_reset
 * Signature: ()I
 */
jint JNICALL Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1hard_1reset(JNIEnv* env, jobject self) {
    return ctapi_hard_reset(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_write_display
 * Signature: (I[B)I
 */
jint JNICALL Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1write_1display(JNIEnv* env, jobject self, jint row, jbyteArray data) {
    jboolean isCopy;
    int res;

    ctapi_device_t* ptr = jobj2ptr(env, self);
    jsize s = (*env)->GetArrayLength(env, data);
    jbyte* b = (*env)->GetByteArrayElements(env, data, &isCopy);

    // ctapi_write_display requires a buffer which is at least 16 bytes large
    if (s >= 16)
        res = ctapi_write_display(ptr, row, (uint8_t*)b);
    else {
        uint8_t tmp[16];
        memset(tmp, 0, 16);
        memcpy(tmp, b, s);
        res = ctapi_write_display(ptr, row, tmp);
    }

    if (isCopy)
        (*env)->ReleaseByteArrayElements(env, data, b, 0);

    return res;
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_update_display
 * Signature: ()I
 */
jint JNICALL Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1update_1display(JNIEnv* env, jobject self) {
    return ctapi_update_display(jobj2ptr(env, self));
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_select_profile
 * Signature: (I)I
 */
jint JNICALL Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1select_1profile(JNIEnv* env, jobject self, jint id) {
    return ctapi_select_profile(jobj2ptr(env, self), id);
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_set_time
 * Signature: (II)I
 */
jint JNICALL Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1set_1time(JNIEnv* env, jobject self, jint h, jint m) {
    return ctapi_set_time(jobj2ptr(env, self), h, m);
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_read_display
 * Signature: (I[B)I
 */
jint JNICALL Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1read_1display(JNIEnv* env, jobject self, jint row, jbyteArray buffer) {
    jboolean isCopy;

    ctapi_device_t* ptr = jobj2ptr(env, self);
    jsize s = (*env)->GetArrayLength(env, buffer);
    jbyte* read_data = malloc(16);

    if (!read_data)
        return -5;

    int res = ctapi_read_display(ptr, row, (uint8_t*)read_data);

    if (res > 0)
        (*env)->SetByteArrayRegion(env, buffer, 0, res, read_data);
    else free(read_data);

    return res;
}

/*
 * Class:     keyboard_device_usb_CTAPIDeviceWrapper
 * Method:    ctapi_get_profile
 * Signature: ()I
 */
jint JNICALL Java_keyboard_device_usb_CTAPIDeviceWrapper_ctapi_1get_1profile(JNIEnv* env, jobject self) {
    return ctapi_get_profile(jobj2ptr(env, self));
}
