#!/bin/bash

gcc \
    main.c \
    ctapi.c \
    -g -ggdb \
    -I/usr/include/libusb-1.0/ \
    -lusb-1.0 \
    -lusb \
    --std=gnu99 \
    -o ctcfg

gcc \
    ctapi.c \
    ctjapi.c \
    -shared \
    -fpic \
    -z noexecstack \
    -g -ggdb \
    -I/usr/include/libusb-1.0/ \
    -I/usr/lib/jvm/java-12-openjdk/include/linux/ \
    -I/usr/lib/jvm/java-12-openjdk/include/ \
    -I/usr/lib/jvm/java-12-openjdk-amd64/include/linux/ \
    -I/usr/lib/jvm/java-12-openjdk-amd64/include/ \
    -I/usr/lib/jvm/java-13-openjdk/include/linux/ \
    -I/usr/lib/jvm/java-13-openjdk/include/ \
    -I/usr/lib/jvm/java-13-openjdk-amd64/include/linux/ \
    -I/usr/lib/jvm/java-13-openjdk-amd64/include/ \
    -lusb-1.0 \
    -lusb \
    --std=gnu99 \
    -o libctcfg.so
