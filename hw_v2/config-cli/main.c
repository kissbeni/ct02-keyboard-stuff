
#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <usb.h>
#include <libusb.h>
#include <stdbool.h>
#include <signal.h>
#include <stdint.h>
#include <unistd.h>
#include <argp.h>

#include "ctapi.h"

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#define SPLASH_STATUS_STARTUP               0x00
#define SPLASH_STATUS_HEADER_BAD_MAGIC      0x20
#define SPLASH_STATUS_HEADER_BAD_VERSION    0x21
#define SPLASH_STATUS_NO_PROFILES           0x22
#define SPLASH_STATUS_BAD_RGB_CONFIG        0x23
#define SPLASH_STATUS_BAD_MACRO_HEADER      0x24
#define SPLASH_STATUS_KEY_MAP_FAILED        0x25
#define SPLASH_STATUS_OK                    0xFF

static char doc[] = "CT-02 USB command tool\n\n"
                    "Commands:\n"
                    " cfgver  - Reads the config version\n"
                    " lastkey - Reads the last pressed key\n"
                    " keymx   - Reads the status of all keys\n"
                    " write   - Write data to the EEPROM\n"
                    " read    - Read data from the EEPROM\n"
                    " reload  - Reload configuration\n"
                    " verify  - Verify configuration\n"
                    " settime - Set time\n";

static char args_doc[] = "COMMAND";

static struct argp_option options[] = {
    { "file",    'f', "FILE",   0, "The file used for EEPROM read/write commands" },
    { "address", 'a', "ADDR",   0, "The start address for EEPROM operations" },
    { "size",    's', "BYTES",  0, "How many bytes to read from the EEPROM" },
    { "repeat",  'r', "COUNT",  0, "Repeat the command (-1 for no limit)" },
    { "delay",   'd', "MILLS",  0, "Delay between commands" },
    { "verbose", 'v', 0,        0, "Be verbose" },
    { 0 }
};

typedef struct arguments {
    char* command;
    char* file;
    int address, size, repeat_count, delay;
    int verbose_level;
} arguments_t;

#define HANDLER_ARGS arguments_t* args, ctapi_device_t* dev

typedef struct command_def {
    const char* name;
    void(*handler)(HANDLER_ARGS);
} command_def_t;

void cmd_cfgver(HANDLER_ARGS);
void cmd_lastkey(HANDLER_ARGS);
void cmd_keymx(HANDLER_ARGS);
void cmd_read(HANDLER_ARGS);
void cmd_write(HANDLER_ARGS);
void cmd_reload(HANDLER_ARGS);
void cmd_settime(HANDLER_ARGS);
void cmd_verify(HANDLER_ARGS);

static command_def_t commands[] = {
    { "cfgver",     cmd_cfgver  },
    { "lastkey",    cmd_lastkey },
    { "keymx",      cmd_keymx   },
    { "write",      cmd_write   },
    { "read",       cmd_read    },
    { "reload",     cmd_reload  },
    { "verify",     cmd_verify  },
    { "settime",    cmd_settime },
    { 0, 0 }
};

static error_t parse_opt(int key, char* arg, struct argp_state* state) {
    arguments_t* arguments = state->input;

    switch (key) {
        case 'f':
            arguments->file = arg;
            break;
        case 'a':
            arguments->address = atoi(arg);
            break;
        case 's':
            arguments->size = atoi(arg);
            break;
        case 'r':
            arguments->repeat_count = atoi(arg);
            break;
        case 'd':
            arguments->delay = atoi(arg);
            break;
        case 'v':
            arguments->verbose_level++;
            break;
        case ARGP_KEY_ARG:
            if (state->arg_num >= 1)
                argp_usage(state);

            arguments->command = arg;
            break;

        case ARGP_KEY_END:
            if (state->arg_num < 1)
                argp_usage (state);
            break;

        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

static struct libusb_device_handle* handle;

void validate_address(int address) {
    if (address < 0 || address >= 2048) {
        fprintf(stderr, "Address is out of range: %d\n", address);
        exit(1);
    }
}

void handle_interrupt(int dummy) {
    exit(0);
}

int main (int argc, char** argv) {
    struct arguments arguments;

    /* Default values. */
    arguments.command = 0;
    arguments.file = "-";
    arguments.address = 0;
    arguments.repeat_count = 1;
    arguments.delay = 100;
    arguments.verbose_level = 0;

    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    for (int i = 0; arguments.command[i]; i++)
        arguments.command[i] = tolower(arguments.command[i]);

    void(*handler)(HANDLER_ARGS) = 0;

    for (int i = 0; commands[i].name; i++)
        if (!strcmp(commands[i].name, arguments.command)) {
            handler = commands[i].handler;
            break;
        }

    if (!handler) {
        fprintf(stderr, "Unrecognised command: %s\n", arguments.command);
        return 1;
    }

    ctapi_logging_option(CTAPI_LOGOPT_PROGRESS_BARS, true);

    if (arguments.verbose_level >= 2)
        ctapi_logging_option(CTAPI_LOGOPT_COMMAND_HEXDUMPS, true);

    if (arguments.verbose_level >= 3)
        libusb_set_option(NULL, LIBUSB_OPTION_LOG_LEVEL, 4);

    ctapi_device_t* dev = 0;
    if (strcmp("verify", arguments.command)) {
        printf("Waiting for device");
        do {
            printf(".");
            fflush(stdout);
            usleep(100 * 1000);
        } while (!ctapi_any_device_present());

        puts("");

        if (arguments.verbose_level >= 1)
            puts("Detected a device, trying to get a handle for it...");

        dev = ctapi_first_device();

        if (!ctapi_open(dev)) {
            uid_t uid = getuid();
            if (!(uid < 0 || uid != geteuid())) {
                fprintf(stderr, "Failed to open device, maybe try again as root\n");
                return 2;
            }

            fprintf(stderr, "Failed to open device, try reconnecting the device\n");
            return 2;
        }

        if (arguments.verbose_level >= 1)
            puts("Got CTAPI device");

        if (arguments.verbose_level >= 2)
            printf("invoking command handler for %s (handler ptr: %p, device: %p)\n", arguments.command, handler, dev);
    }

    handler(&arguments, dev);

    if (arguments.verbose_level >= 1)
        puts("Handler finished, destroying device");

    ctapi_destroy_device(dev);

    if (arguments.verbose_level >= 1)
        puts("Bye!");

    return 0;
}

void print_binary(uint8_t v) {
    int i;
    for (i = 0; i < 8; i++) {
        if (v & 128) printf("|");
        else printf(".");

        v <<= 1;
    }
}

void cmd_cfgver(HANDLER_ARGS) {
    int res = ctapi_get_version(dev);
    if (res < 0) {
        fprintf(stderr, "Failed to get version info, error=%d\n", res);
        return;
    }

    printf("%d\n", res);
}

void cmd_lastkey(HANDLER_ARGS) {
    int repeat = args->repeat_count;

    while (repeat < 0 || repeat-- > 0) {
        int res = ctapi_get_lastkey(dev);
        if (res < 0) {
            fprintf(stderr, "Failed to get last key, error=%d\n", res);
        } else {
            printf("%04x\n", res);
        }

        usleep(args->delay * 1000);
    }
}

void cmd_keymx(HANDLER_ARGS) {
    uint8_t buff[24];

    int repeat = args->repeat_count;
    while (repeat < 0 || repeat-- > 0) {
        int res = ctapi_get_matrix(dev, buff, 24);
        if (res < 0) {
            fprintf(stderr, "Failed to get keyboard matrix, error=%d\n", res);
        } else {
            for (int i = 0; i < 16; i++) {
                print_binary(buff[2 + i]);
                printf(" ");
            }
        }

        usleep(args->delay * 1000);
    }
}

void cmd_read(HANDLER_ARGS) {
    FILE* fp = fopen(args->file, "wb");
    if (!fp) {
        fprintf(stderr, "Failed to open file %s\n", args->file);
        return;
    }

    int size = MIN(args->size, 2048 - args->address);

    void* buffer = malloc(size);
    if (!buffer) {
        fprintf(stderr, "failed to allocate memory\n");
        return;
    }

    memset(buffer, 0, size);

    int res = ctapi_read_eeprom(dev, buffer, size, args->address);

    if (res < 0) {
        printf("Failed to read EEPROM: error=%d\n", res);
    } else {
        printf("Read %d bytes.\n", res);
    }

    fwrite(buffer, 1, size, fp);
    free(buffer);
    fclose(fp);
}

void cmd_write(HANDLER_ARGS) {
    FILE* fp = fopen(args->file, "rb");
    if (!fp) {
        fprintf(stderr, "Failed to open file %s\n", args->file);
        return;
    }

    int size = MIN(args->size, 2048 - args->address);

    printf("Writing %d bytes to EEPROM\n", size);

    void* buffer = malloc(size);
    if (!buffer) {
        fprintf(stderr, "failed to allocate memory\n");
        return;
    }

    int res = fread(buffer, 1, size, fp);
    if (res != size) {
        fprintf(stderr, "Failed to read input file, fread resulted %d while expecting %d\n", res, size);
        return;
    }

    res = ctapi_write_eeprom(dev, buffer, size, args->address);

    if (res < 0) {
        printf("Failed to write EEPROM: error=%d\n", res);
    } else {
        printf("Wrote %d bytes.\n", res);
    }

    free(buffer);
    fclose(fp);
}

void cmd_reload(HANDLER_ARGS) {
    int res = ctapi_reload_config(dev);
    if (res < 0) {
        fprintf(stderr, "Failed to reload config, error=%d\n", res);
        return;
    }
}

void cmd_settime(HANDLER_ARGS) {
    int res = ctapi_set_time(dev, 12, 34);
    if (res < 0) {
        fprintf(stderr, "Failed to get version info, error=%d\n", res);
        return;
    }
}

uint8_t fread_byte(FILE* fp) {
    uint8_t tmp;
    fread(&tmp, 1, 1, fp);
    return tmp;
}

#define FNV_32_INIT 0x811c9dc5
#define FNV_32_PRIME 0x01000193

#define HASHREAD(b) (b) = fread_byte(fp); hash = (hash >> 1) + ((hash & 1) << 15) + b;

#define MONOP   0
#define MOHOLDD 1
#define MOHOLDU 2
#define MOPRESS 3
#define MODELAY 4
#define MOREPN  5
#define MOEXIT  15

uint8_t macro_program_counter = 0xFF;
uint8_t current_byte, exec_offset, press_key, delay_counter;
uint16_t start_address, repn_info, macro_address_list_address;
uint8_t macro_keys_down[5];

void run_macro(FILE* fp, uint8_t slot) {
    if (slot > 15)
        return;

    fseek(fp, macro_address_list_address + sizeof(uint16_t)*slot, SEEK_SET);
    fread(&start_address, 2, 1, fp);
    if (start_address == 0 || start_address >= 2048)
        return;

    for (uint8_t i = 0; i < 5; i++)
        macro_keys_down[i] = 0;

    fseek(fp, start_address, SEEK_SET);
    current_byte = fread_byte(fp);
    exec_offset = 0;
    macro_program_counter = 0;
    press_key = 0xFF;
}

uint8_t get_byte(FILE* fp) {
    exec_offset++;
    fseek(fp, start_address + exec_offset, SEEK_SET);
    uint8_t b = fread_byte(fp);
    return b;
}

void set_hold(uint8_t arg, uint8_t set) {
    for (uint8_t i = 0; i < 5; i++) {
        uint8_t kd = macro_keys_down[i];

        if (kd == arg) {
            if (!(set & 1))
                macro_keys_down[i] = 0;

        } else if (!kd && (set & 1)) {
            macro_keys_down[i] = arg;

            if (set & 2)
                press_key = i;

            break;
        }
    }
}

void step_macro(FILE* fp) {
    if (press_key < 5) {
        macro_keys_down[press_key] = 0;
        press_key = 0xFF;
    }

    if (delay_counter > 0) {
        delay_counter--;
        return;
    }

    if (macro_program_counter > 50)
        return;

    uint8_t opcode, arg_byte = 0;

    if ((macro_program_counter % 2) == 0)
        opcode = current_byte >> 4;
    else
        opcode = current_byte & 0xF;

    if (opcode > 0 && opcode < 10)
        arg_byte = get_byte(fp);

    switch (opcode) {
        case MONOP:
            printf("  <0x%02x> NOP\n", macro_program_counter);
            break;
        case MOHOLDD:
            printf("  <0x%02x> HOLDD 0x%02x\n", macro_program_counter, arg_byte);
            set_hold(arg_byte, 1);
            break;
        case MOHOLDU:
            printf("  <0x%02x> HOLDU 0x%02x\n", macro_program_counter, arg_byte);
            set_hold(arg_byte, 0);
            break;
        case MOPRESS:
            printf("  <0x%02x> PRESS 0x%02x\n", macro_program_counter, arg_byte);
            set_hold(arg_byte, 3);
            break;
        case MODELAY:
            delay_counter = arg_byte;
            printf("  <0x%02x> DELAY %d\n", macro_program_counter, arg_byte);
            break;
        /*case MOREPN:
            do_repn(arg_byte);
            return;*/
        case MOEXIT:
            printf("  <0x%02x> EXIT\n", macro_program_counter);
            macro_program_counter = 0xFF;
            return;
        default:
            printf("  <0x%02x> UNKNOWN INSTRUCTION (%d)\n", macro_program_counter, opcode);
            break;
    }

    if ((macro_program_counter++ % 2) == 1)
        current_byte = get_byte(fp);
}

bool check_hash(void* data, uint16_t data_len, uint16_t sum) {
    uint16_t checksum = 0;

    for (uint16_t i = 0; i < data_len; i++) {
        checksum = (checksum >> 1) + ((checksum & 1) << 15);
        checksum += ((uint8_t*)data)[i];
    }

    if (checksum == sum) {
        printf("  <check hash OK: 0x%04x == 0x%04x>\n", checksum, sum);
        return true;
    }

    printf("  <check hash FAIL: 0x%04x != 0x%04x>\n", checksum, sum);
    return false;
}

void cmd_verify(HANDLER_ARGS) {
    uint8_t tmp;
    FILE* fp = fopen(args->file, "rb");

    printf("Verifying configuration file %s\n", args->file);

    uint8_t e = 0x20;
    const char* msg;

    if (!fp) {
        printf("Error 0x%02x: could not begin read\n", e);
        return;
    }

    e = SPLASH_STATUS_HEADER_BAD_MAGIC;
    msg = "invalid header magic";

    if (fread_byte(fp) != 0xC7) {
        printf("\x1b[1m\x1b[36mHeader check........................................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }

    if (fread_byte(fp) != 0x02) {
        printf("\x1b[1m\x1b[36mHeader check........................................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }

    printf("header magic:           OK (0x02C7)\n");

    e = SPLASH_STATUS_HEADER_BAD_VERSION;
    msg = "invalid header version";
    if ((tmp = fread_byte(fp)) != 0x03) {
        printf("\x1b[1m\x1b[36mHeader check........................................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }

    printf("header version:         %d\n", tmp);
    printf("\x1b[1m\x1b[36mHeader check........................................[\x1b[32mPASSED\x1b[36m\x1b[1m]\x1b[0m\n");

    tmp = fread_byte(fp);
    uint8_t num_profiles = tmp & 0xf;

    printf("number of profiles:     %d (raw: 0x%02x)\n", num_profiles, tmp);

    e = SPLASH_STATUS_NO_PROFILES;
    msg = "no active profiles found";
    if (num_profiles == 0 || !(tmp & 0xF)) {
        printf("\x1b[1m\x1b[36mProfile header list check...........................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }

    uint8_t first_valid_profile = 0xFF;

    uint16_t keymapAddresses[4][2];
    memset(keymapAddresses, 0, sizeof(keymapAddresses));

    for (uint8_t i = 0; i < num_profiles; i++) {
        e = 0x30 | i;
        uint16_t addr1, addr2;
        fread(&addr1, 1, 2, fp);
        fread(&addr2, 1, 2, fp);
        printf("profile #%d:             b:%04xh n:%04xh e:%d\n",
            i,
            addr1,
            addr2,
            !!(tmp & (1 << (i + 4))));

        if (!!(tmp & (1 << (i + 4)))) {
            keymapAddresses[i][0] = addr1;
            keymapAddresses[i][1] = addr2;

            if (first_valid_profile == 0xFF)
                first_valid_profile = i;
        }
    }

    printf("default profile:        %d\n", first_valid_profile);

    printf("\x1b[1m\x1b[36mProfile header list check...........................[\x1b[32mPASSED\x1b[36m\x1b[1m]\x1b[0m\n");

    printf("rgb config begins:      0x%04lx\n", ftell(fp));

    e = SPLASH_STATUS_BAD_RGB_CONFIG;
    msg = "invalid RGB header";
    if (fread_byte(fp) != '!') {
        printf("\x1b[1m\x1b[36mRGB configuration check.............................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }
    if (fread_byte(fp) != 'R') {
        printf("\x1b[1m\x1b[36mRGB configuration check.............................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }
    if (fread_byte(fp) != 'G') {
        printf("\x1b[1m\x1b[36mRGB configuration check.............................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }
    if (fread_byte(fp) != 'B') {
        printf("\x1b[1m\x1b[36mRGB configuration check.............................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }

    rgb_config_t cfg;
    fread(&cfg, 1, sizeof(cfg), fp);

    printf("rgb animation type:     %d\n", cfg.anim_type);
    printf("rgb brightness:         %d\n", cfg.brightness);
    printf("rgb animation speed:    %d\n", cfg.speed);
    printf("rgb color r:            %d\n", cfg.color.r);
    printf("rgb color g:            %d\n", cfg.color.g);
    printf("rgb color b:            %d\n", cfg.color.b);

    uint16_t hash;
    fread(&hash, 2, 1, fp);

    msg = "invalid RGB checksum";
    printf("rgb configuration hash: 0x%04x\n", hash);
    if (!check_hash(&cfg, sizeof(cfg), hash)) {
        printf("\x1b[1m\x1b[36mRGB configuration check.............................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }

    printf("rgb config end address: 0x%04lx\n", ftell(fp));

    printf("\x1b[1m\x1b[36mRGB configuration check.............................[\x1b[32mPASSED\x1b[36m\x1b[1m]\x1b[0m\n");

    e = SPLASH_STATUS_BAD_MACRO_HEADER;
    msg = "invalid macro header";
    if (fread_byte(fp) != 'M') {
        printf("\x1b[1m\x1b[36mMacro header check..................................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
        goto error;
    }

    uint64_t macro_list_start = ftell(fp);
    macro_address_list_address = macro_list_start;
    printf("macro list start:       0x%04lx\n", macro_list_start);

    for (int i = 0; i < 16; i++) {
        fseek(fp, macro_list_start + i * 2, SEEK_SET);
        uint16_t offs = 0xFFFF;
        fread(&offs, sizeof(offs), 1, fp);

        if (offs == 0xFFFF)
            printf("macro #%02d start:        ???\n", i);
        else if (offs == 0)
            printf("macro #%02d start:        <empty>\n", i);
        else
            printf("macro #%02d start:        0x%04x\n", i, offs);
    }

    printf("\x1b[1m\x1b[36mMacro header check..................................[\x1b[32mPASSED\x1b[36m\x1b[1m]\x1b[0m\n");
    fseek(fp, macro_list_start + 16 * 2, SEEK_SET);

    e = SPLASH_STATUS_KEY_MAP_FAILED;

    for (int i = 0; i < 4; i++) {
        if (!keymapAddresses[i][0]) {
            printf("keymap profile %d is disabled\n", i);
            continue;
        }

        printf("checking keymap profile %d\n", i);
        uint16_t address = keymapAddresses[i][0];

        printf("keymap name address :   0x%04x\n", keymapAddresses[i][1]);
        printf("keymap begin address:   0x%04x\n", address);
        char name[256];
        fseek(fp, 0, SEEK_END);
        uint64_t size = ftell(fp);
        fseek(fp, keymapAddresses[i][1], SEEK_SET);
        fread(name, 1, MIN(size - keymapAddresses[i][1], 256), fp);
        printf("keymap profile name:    %s\n", name);

        fseek(fp, address, SEEK_SET);

        hash = 0;
        uint8_t r, c, k, kc;

        HASHREAD(c)
        while (c != 0xFF) {
            HASHREAD(r)
            HASHREAD(k)

            k += c;
            for (; c < k; c++) {
                HASHREAD(kc)

                if (r >= 8 || c >= 16) {
                    msg = "invalid keymap";
                    printf("\x1b[1m\x1b[36mProfile keymap checks...............................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
                    goto error;
                }
            }

            HASHREAD(c)
        }

        msg = "hash missmatch";
        uint16_t read_hash;
        fread(&read_hash, 1, 2, fp);

        if (read_hash == hash) {
            printf("hash check ok!\n");
        } else {
            printf("hash missmatch: 0x%04x != 0x%04x\n", hash, read_hash);
            printf("\x1b[1m\x1b[36mProfile keymap checks...............................[\x1b[31mFAILED\x1b[36m\x1b[1m]\x1b[0m\n");
            goto error;
        }
    }

    printf("\x1b[1m\x1b[36mProfile keymap checks...............................[\x1b[32mPASSED\x1b[36m\x1b[1m]\x1b[0m\n");

    puts("Running macros:");

    for (uint8_t i = 0; i < 16; i++) {
        run_macro(fp, i);

        printf("macro slot #%02d:\n", i);
        while (macro_program_counter < 50)
            step_macro(fp);
    }

    printf("\x1b[1m\x1b[36mMacro execution tests...............................[\x1b[32mPASSED\x1b[36m\x1b[1m]\x1b[0m\n");

    goto end;

    error:
    printf("Error 0x%02x: %s (file offset: 0x%04lx)\n", e, msg, ftell(fp));
    end:
    fclose(fp);
}
